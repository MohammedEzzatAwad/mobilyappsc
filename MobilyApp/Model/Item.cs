﻿using System.Threading;
using MobilyApp.Common;

namespace MobilyApp.Model
{
    public class Item : ViewModelBase
    {
        protected string m_refName = "";
        protected string m_refTitle = "";
        protected string m_refDescription = "";
        protected string m_refLink = "";
        protected string m_refLogo = "";
        protected string m_refCategory = "";
        private PropertyChangedEventHandler<string> nameChanged;
        private PropertyChangedEventHandler<string> categoryChanged; 
        private PropertyChangedEventHandler<string> titleChanged;
        private PropertyChangedEventHandler<string> descriptionChanged;
        private PropertyChangedEventHandler<string> linkChanged;
        private PropertyChangedEventHandler<string> logoChanged;


        public virtual string Category
        {
            get
            {
                return this.m_refCategory;
            }
            set
            {
                string refOldValue = this.m_refCategory;
                if (!this.ValidateCategoryChange(refOldValue, value))
                    return;
                this.m_refCategory = value;
                this.HandleCategoryChanged(refOldValue, this.m_refCategory);
                this.NotifyPropertyChanged("Category");
                this.RaiseNameChanged(refOldValue, this.m_refCategory);
            }
        }

        public virtual string Name
        {
            get
            {
                return this.m_refName;
            }
            set
            {
                string refOldValue = this.m_refName;
                if (!this.ValidateNameChange(refOldValue, value))
                    return;
                this.m_refName = value;
                this.HandleNameChanged(refOldValue, this.m_refName);
                this.NotifyPropertyChanged("Name");
                this.RaiseNameChanged(refOldValue, this.m_refName);
            }
        }
        public virtual string Title
        {
            get
            {
                return this.m_refTitle;
            }
            set
            {
                string refOldValue = this.m_refName;
                if (!this.ValidateTitleChange(refOldValue, value))
                    return;
                this.m_refTitle = value;
                this.HandleTitleChanged(refOldValue, this.m_refTitle);
                this.NotifyPropertyChanged("Title");
                this.RaiseTitleChanged(refOldValue, this.m_refTitle);
            }
        }

        public virtual string Description
        {
            get
            {
                return this.m_refDescription;
            }
            set
            {
                string refOldValue = this.m_refDescription;
                if (!this.ValidateDescriptionChange(refOldValue, value))
                    return;
                this.m_refDescription = value;
                this.HandleDescriptionChanged(refOldValue, this.m_refDescription);
                this.NotifyPropertyChanged("Description");
                this.RaiseDescriptionChanged(refOldValue, this.m_refDescription);
            }
        }
        public virtual string Link
        {
            get
            {
                return this.m_refLink;
            }
            set
            {
                string refOldValue = this.m_refLink;
                if (!this.ValidateLinkChange(refOldValue, value))
                    return;
                this.m_refLink = value;
                this.HandleLinkChanged(refOldValue, this.m_refLink);
                this.NotifyPropertyChanged("Link");
                this.RaiseLinkChanged(refOldValue, this.m_refLink);
            }
        }

        public virtual string Logo
        {
            get
            {
                return this.m_refLogo;
            }
            set
            {
                string refOldValue = this.m_refLogo;
                if (!this.ValidateLogoChange(refOldValue, value))
                    return;
                this.m_refLogo = value;
                this.HandleLogoChanged(refOldValue, this.m_refLogo);
                this.NotifyPropertyChanged("Logo");
                this.RaiseLogoChanged(refOldValue, this.m_refLogo);
            }
        }


        public event PropertyChangedEventHandler<string> NameChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.nameChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.nameChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.nameChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.nameChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }
        public event PropertyChangedEventHandler<string> CategoryChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.categoryChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.categoryChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.categoryChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.categoryChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }
        public event PropertyChangedEventHandler<string> TitleChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.titleChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.titleChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.titleChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.titleChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }
        public event PropertyChangedEventHandler<string> DescriptionChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.descriptionChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.descriptionChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.descriptionChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.descriptionChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }
        public event PropertyChangedEventHandler<string> LinkChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.linkChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.linkChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.linkChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.linkChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }
        public event PropertyChangedEventHandler<string> LogoChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.logoChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.logoChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.logoChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.logoChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        protected void RaiseNameChanged(string refOldValue, string refNewValue)
        {
            if (this.nameChanged == null)
                return;
            this.nameChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }
        protected void RaiseCategoryChanged(string refOldValue, string refNewValue)
        {
            if (this.categoryChanged == null)
                return;
            this.categoryChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }
        protected void RaiseTitleChanged(string refOldValue, string refNewValue)
        {
            if (this.titleChanged == null)
                return;
            this.titleChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }
        protected void RaiseDescriptionChanged(string refOldValue, string refNewValue)
        {
            if (this.descriptionChanged == null)
                return;
            this.descriptionChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }
        protected void RaiseLinkChanged(string refOldValue, string refNewValue)
        {
            if (this.linkChanged == null)
                return;
            this.linkChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }
        protected void RaiseLogoChanged(string refOldValue, string refNewValue)
        {
            if (this.logoChanged == null)
                return;
            this.logoChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected virtual void HandleNameChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateNameChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleCategoryChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateCategoryChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleTitleChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateTitleChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleDescriptionChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateDescriptionChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleLinkChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateLinkChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }
        protected virtual void HandleLogoChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateLogoChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }
    }
}