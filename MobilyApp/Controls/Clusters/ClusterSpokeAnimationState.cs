﻿namespace MobilyApp.Controls.Clusters
{
    public enum ClusterSpokeAnimationState
    {
        Expanding,
        Collapsing,
        Neutral,
        Flicking,
        Destroying
    }
}