﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace MobilyApp.Converters
{
    public class CaseConverter : IValueConverter
    {
        // Methods
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string str = value as string;
            if (str == null)
            {
                return null;
            }
            if (parameter != null)
            {
                return str.ToUpper(CultureInfo.CurrentCulture);
            }
            return str.ToLower(CultureInfo.CurrentCulture);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }

}