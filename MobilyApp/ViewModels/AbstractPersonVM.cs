﻿using System;
using System.Threading;
using MobilyApp.Common;

namespace MobilyApp.ViewModels
{
    public abstract class AbstractPersonVM : ViewModelBase
    {
        protected BillVM m_refBill = (BillVM)null;
        protected BalanceVM m_refBalance = (BalanceVM)null;
        protected NeqatyVM m_refNeqaty = (NeqatyVM) null;
        protected PrePaidVM m_refPrePaid = (PrePaidVM) null;
        protected PostPaidVM m_refPostPaid = (PostPaidVM) null;
        protected ServicesVM m_refServices = (ServicesVM) null;
        protected string m_refHashCode = "";
      
        private PropertyChangedEventHandler<BillVM> billChanged;
        private PropertyChangedEventHandler<BalanceVM> balanceChanged;
        private PropertyChangedEventHandler<NeqatyVM> neqatyChanged;
        private PropertyChangedEventHandler<PrePaidVM> prePaidChanged;
        private PropertyChangedEventHandler<PostPaidVM> postpaidChanged;
        private PropertyChangedEventHandler<ServicesVM> servicesChanged;

        private PropertyChangedEventHandler<string> hashCodeChanged;
        private EventHandler<GenericEventArgs<string>> errorFound;
        private EventHandler<GenericEventArgs<string>> progress;


        public virtual BillVM Bill
        {
            get
            {
                return this.m_refBill;
            }
            set
            {
                BillVM refOldValue = this.m_refBill;
                if (!this.ValidateBillChange(refOldValue, value))
                    return;
                this.m_refBill = value;
                this.HandleBillChanged(refOldValue, this.m_refBill);
                this.NotifyPropertyChanged("Bill");
                this.RaiseBillChanged(refOldValue, this.m_refBill);
            }
        }

        public virtual BalanceVM Balance
        {
            get
            {
                return this.m_refBalance;
            }
            set
            {
                BalanceVM refOldValue = this.m_refBalance;
                if (!this.ValidateBalanceChange(refOldValue, value))
                    return;
                this.m_refBalance = value;
                this.HandleBalanceChanged(refOldValue, this.m_refBalance);
                this.NotifyPropertyChanged("Balance");
                this.RaiseBalanceChanged(refOldValue, this.m_refBalance);
            }
        }

        public virtual NeqatyVM Neqaty
        {
            get
            {
                return this.m_refNeqaty;
            }
            set
            {
                NeqatyVM refOldValue = this.m_refNeqaty;
                if (!this.ValidateNeqatyChange(refOldValue, value))
                    return;
                this.m_refNeqaty = value;
                this.HandleNeqatyChanged(refOldValue, this.m_refNeqaty);
                this.NotifyPropertyChanged("Neqaty");
                this.RaiseNeqatyChanged(refOldValue, this.m_refNeqaty);
            }
        }

        public virtual PrePaidVM PrePaid
        {
            get
            {
                return this.m_refPrePaid;
            }
            set
            {
                PrePaidVM refOldValue = this.m_refPrePaid;
                if (!this.ValidatePrePaidChange(refOldValue, value))
                    return;
                this.m_refPrePaid = value;
                this.HandlePrePaidChanged(refOldValue, this.m_refPrePaid);
                this.NotifyPropertyChanged("PrePaid");
                this.RaisePrePaidChanged(refOldValue, this.m_refPrePaid);
            }
        }

        public virtual PostPaidVM PostPaid
        {
            get
            {
                return this.m_refPostPaid;
            }
            set
            {
                PostPaidVM refOldValue = this.m_refPostPaid;
                if (!this.ValidatePostPaidChange(refOldValue, value))
                    return;
                this.m_refPostPaid = value;
                this.HandlePostPaidChanged(refOldValue, this.m_refPostPaid);
                this.NotifyPropertyChanged("PostPaid");
                this.RaisePostPaidChanged(refOldValue, this.m_refPostPaid);
            }
        }

        public virtual ServicesVM Services
        {
            get
            {
                return this.m_refServices;
            }
            set
            {
                ServicesVM refOldValue = this.m_refServices;
                if (!this.ValidateServicesChange(refOldValue, value))
                    return;
                this.m_refServices = value;
                this.HandleServicesChanged(refOldValue, this.m_refServices);
                this.NotifyPropertyChanged("Services");
                this.RaiseServicesChanged(refOldValue, this.m_refServices);
            }
        }

        public virtual string HashCode
        {
            get
            {
                return this.m_refHashCode;
            }
            set
            {
                string refOldValue = this.m_refHashCode;
                if (!this.ValidateHashCodeChange(refOldValue, value))
                    return;
                this.m_refHashCode = value;
                this.HandleHashCodeChanged(refOldValue, this.m_refHashCode);
                this.NotifyPropertyChanged("HashCode");
                this.RaiseHashCodeChanged(refOldValue, this.m_refHashCode);
            }
        }




        public event PropertyChangedEventHandler<BillVM> BillChanged
        {
            add
            {
                PropertyChangedEventHandler<BillVM> changedEventHandler = this.billChanged;
                PropertyChangedEventHandler<BillVM> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<BillVM>>(ref this.billChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<BillVM> changedEventHandler = this.billChanged;
                PropertyChangedEventHandler<BillVM> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<BillVM>>(ref this.billChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<BalanceVM> BalanceChanged
        {
            add
            {
                PropertyChangedEventHandler<BalanceVM> changedEventHandler = this.balanceChanged;
                PropertyChangedEventHandler<BalanceVM> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<BalanceVM>>(ref this.balanceChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<BalanceVM> changedEventHandler = this.balanceChanged;
                PropertyChangedEventHandler<BalanceVM> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<BalanceVM>>(ref this.balanceChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<NeqatyVM> NeqatyChanged
        {
            add
            {
                PropertyChangedEventHandler<NeqatyVM> changedEventHandler = this.neqatyChanged;
                PropertyChangedEventHandler<NeqatyVM> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<NeqatyVM>>(ref this.neqatyChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<NeqatyVM> changedEventHandler = this.neqatyChanged;
                PropertyChangedEventHandler<NeqatyVM> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<NeqatyVM>>(ref this.neqatyChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }
        public event PropertyChangedEventHandler<PrePaidVM> PrePaidChanged
        {
            add
            {
                PropertyChangedEventHandler<PrePaidVM> changedEventHandler = this.prePaidChanged;
                PropertyChangedEventHandler<PrePaidVM> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<PrePaidVM>>(ref this.prePaidChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<PrePaidVM> changedEventHandler = this.prePaidChanged;
                PropertyChangedEventHandler<PrePaidVM> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<PrePaidVM>>(ref this.prePaidChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<PostPaidVM> PostPaidChanged
        {
            add
            {
                PropertyChangedEventHandler<PostPaidVM> changedEventHandler = this.postpaidChanged;
                PropertyChangedEventHandler<PostPaidVM> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<PostPaidVM>>(ref this.postpaidChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<PostPaidVM> changedEventHandler = this.postpaidChanged;
                PropertyChangedEventHandler<PostPaidVM> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<PostPaidVM>>(ref this.postpaidChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }
        public event PropertyChangedEventHandler<ServicesVM> ServicesChanged
        {
            add
            {
                PropertyChangedEventHandler<ServicesVM> changedEventHandler = this.servicesChanged;
                PropertyChangedEventHandler<ServicesVM> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<ServicesVM>>(ref this.servicesChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<ServicesVM> changedEventHandler = this.servicesChanged;
                PropertyChangedEventHandler<ServicesVM> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<ServicesVM>>(ref this.servicesChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> HashCodeChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.hashCodeChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.hashCodeChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.hashCodeChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.hashCodeChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event EventHandler<GenericEventArgs<string>> ErrorFound
        {
            add
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.errorFound;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.errorFound, comparand + value, comparand);
                }
                while (eventHandler != comparand);
            }
            remove
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.errorFound;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.errorFound, comparand - value, comparand);
                }
                while (eventHandler != comparand);
            }
        }

        public event EventHandler<GenericEventArgs<string>> Progress
        {
            add
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.progress;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.progress, comparand + value, comparand);
                }
                while (eventHandler != comparand);
            }
            remove
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.progress;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.progress, comparand - value, comparand);
                }
                while (eventHandler != comparand);
            }
        }

        protected void RaiseBillChanged(BillVM refOldValue, BillVM refNewValue)
        {
            if (this.billChanged == null)
                return;
            this.billChanged((object)this, new PropertyChangedEventArgs<BillVM>(refOldValue, refNewValue));
        }

        protected void RaiseBalanceChanged(BalanceVM refOldValue, BalanceVM refNewValue)
        {
            if (this.balanceChanged == null)
                return;
            this.balanceChanged((object)this, new PropertyChangedEventArgs<BalanceVM>(refOldValue, refNewValue));
        }
        protected void RaiseNeqatyChanged(NeqatyVM refOldValue, NeqatyVM refNewValue)
        {
            if (this.neqatyChanged == null)
                return;
            this.neqatyChanged((object)this, new PropertyChangedEventArgs<NeqatyVM>(refOldValue, refNewValue));
        }
        protected void RaisePrePaidChanged(PrePaidVM refOldValue, PrePaidVM refNewValue)
        {
            if (this.prePaidChanged == null)
                return;
            this.prePaidChanged((object)this, new PropertyChangedEventArgs<PrePaidVM>(refOldValue, refNewValue));
        }
        protected void RaisePostPaidChanged(PostPaidVM refOldValue, PostPaidVM refNewValue)
        {
            if (this.postpaidChanged == null)
                return;
            this.postpaidChanged((object)this, new PropertyChangedEventArgs<PostPaidVM>(refOldValue, refNewValue));
        }
        protected void RaiseServicesChanged(ServicesVM refOldValue, ServicesVM refNewValue)
        {
            if (this.servicesChanged == null)
                return;
            this.servicesChanged((object)this, new PropertyChangedEventArgs<ServicesVM>(refOldValue, refNewValue));
        }


        protected void RaiseHashCodeChanged(string refOldValue, string refNewValue)
        {
            if (this.hashCodeChanged == null)
                return;
            this.hashCodeChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseErrorFound(string ErrorName)
        {
            if (this.errorFound == null)
                return;
            this.errorFound((object)this, new GenericEventArgs<string>(ErrorName));
        }

        protected void RaiseProgress(string Where)
        {
            if (this.progress == null)
                return;
            this.progress((object)this, new GenericEventArgs<string>(Where));
        }

        protected virtual void HandleBillChanged(BillVM refOldValue, BillVM refNewValue)
        {
        }

        protected virtual bool ValidateBillChange(BillVM refOldValue, BillVM refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleBalanceChanged(BalanceVM refOldValue, BalanceVM refNewValue)
        {
        }

        protected virtual bool ValidateBalanceChange(BalanceVM refOldValue, BalanceVM refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }
        protected virtual void HandleNeqatyChanged(NeqatyVM refOldValue, NeqatyVM refNewValue)
        {
        }

        protected virtual bool ValidateNeqatyChange(NeqatyVM refOldValue, NeqatyVM refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandlePrePaidChanged(PrePaidVM refOldValue, PrePaidVM refNewValue)
        {
        }

        protected virtual bool ValidatePrePaidChange(PrePaidVM refOldValue, PrePaidVM refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }


        protected virtual void HandlePostPaidChanged(PostPaidVM refOldValue, PostPaidVM refNewValue)
        {
        }

        protected virtual bool ValidatePostPaidChange(PostPaidVM refOldValue, PostPaidVM refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }
        protected virtual void HandleServicesChanged(ServicesVM refOldValue, ServicesVM refNewValue)
        {
        }

        protected virtual bool ValidateServicesChange(ServicesVM refOldValue, ServicesVM refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleHashCodeChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateHashCodeChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }
    }
}