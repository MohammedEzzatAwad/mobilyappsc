﻿using System;
using System.Threading;
using MobilyApp.Common;

namespace MobilyApp.ViewModels
{
    public class AbstractNeqatyVM : ViewModelBase
    {
        protected string m_refCurrentBalance = "--";
        protected string m_refEarnedPoints = "--";
        protected string m_refRedeemedPoints = "--";
        protected string m_refExpirySchedulePoints = "--";
        protected string m_refLostPoints = "--";

        private PropertyChangedEventHandler<string> currentBalanceChanged;
        private PropertyChangedEventHandler<string> earnedPointsChanged;
        private PropertyChangedEventHandler<string> redeemedPointsChanged;
        private PropertyChangedEventHandler<string> expirySchedulePointsChanged;
        private PropertyChangedEventHandler<string> lostPointsChanged;

        private EventHandler<GenericEventArgs<string>> errorFound;
        private EventHandler<GenericEventArgs<string>> progress;

        public virtual string CurrentBalance
        {
            get
            {
                return this.m_refCurrentBalance;
            }
            set
            {
                string refOldValue = this.m_refCurrentBalance;
                if (!this.ValidateCurrentBalanceChange(refOldValue, value))
                    return;
                this.m_refCurrentBalance = value;
                this.HandleCurrentBalanceChanged(refOldValue, this.m_refCurrentBalance);
                this.NotifyPropertyChanged("CurrentBalance");
                this.RaiseCurrentBalanceChanged(refOldValue, this.m_refCurrentBalance);
            }
        }
        public virtual string EarnedPoints
        {
            get
            {
                return this.m_refEarnedPoints;
            }
            set
            {
                string refOldValue = this.m_refEarnedPoints;
                if (!this.ValidateEarnedPointsChange(refOldValue, value))
                    return;
                this.m_refEarnedPoints = value;
                this.HandleEarnedPointsChanged(refOldValue, this.m_refEarnedPoints);
                this.NotifyPropertyChanged("EarnedPoints");
                this.RaiseEarnedPointsChanged(refOldValue, this.m_refEarnedPoints);
            }
        }
        public virtual string RedeemedPoints
        {
            get
            {
                return this.m_refRedeemedPoints;
            }
            set
            {
                string refOldValue = this.m_refRedeemedPoints;
                if (!this.ValidateRedeemedPointsChange(refOldValue, value))
                    return;
                this.m_refRedeemedPoints = value;
                this.HandleRedeemedPointsChanged(refOldValue, this.m_refRedeemedPoints);
                this.NotifyPropertyChanged("RedeemedPoints");
                this.RaiseRedeemedPointsChanged(refOldValue, this.m_refRedeemedPoints);
            }
        }
        public virtual string ExpirySchedulePoints
        {
            get
            {
                return this.m_refExpirySchedulePoints;
            }
            set
            {
                string refOldValue = this.m_refExpirySchedulePoints;
                if (!this.ValidateExpirySchedulePointsChange(refOldValue, value))
                    return;
                this.m_refExpirySchedulePoints = value;
                this.HandleExpirySchedulePointsChanged(refOldValue, this.m_refExpirySchedulePoints);
                this.NotifyPropertyChanged("ExpirySchedulePoints");
                this.RaiseExpirySchedulePointsChanged(refOldValue, this.m_refExpirySchedulePoints);
            }
        }
        public virtual string LostPoints
        {
            get
            {
                return this.m_refLostPoints;
            }
            set
            {
                string refOldValue = this.m_refLostPoints;
                if (!this.ValidateLostPointsChange(refOldValue, value))
                    return;
                this.m_refLostPoints = value;
                this.HandleLostPointsChanged(refOldValue, this.m_refLostPoints);
                this.NotifyPropertyChanged("LostPoints");
                this.RaiseLostPointsChanged(refOldValue, this.m_refLostPoints);
            }
        }

        public event PropertyChangedEventHandler<string> CurrentBalanceChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.currentBalanceChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.currentBalanceChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.currentBalanceChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.currentBalanceChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }
        public event PropertyChangedEventHandler<string> EarnedPointsChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.earnedPointsChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.earnedPointsChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.earnedPointsChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.earnedPointsChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }
        public event PropertyChangedEventHandler<string> RedeemedPointsChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.redeemedPointsChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.redeemedPointsChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.redeemedPointsChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.redeemedPointsChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }
        public event PropertyChangedEventHandler<string> ExpirySchedulePointsChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.expirySchedulePointsChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.expirySchedulePointsChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.expirySchedulePointsChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.expirySchedulePointsChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }
        public event PropertyChangedEventHandler<string> LostPointsChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.lostPointsChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.lostPointsChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.lostPointsChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.lostPointsChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }
        public event EventHandler<GenericEventArgs<string>> ErrorFound
        {
            add
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.errorFound;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.errorFound, comparand + value, comparand);
                }
                while (eventHandler != comparand);
            }
            remove
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.errorFound;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.errorFound, comparand - value, comparand);
                }
                while (eventHandler != comparand);
            }
        }
        public event EventHandler<GenericEventArgs<string>> Progress
        {
            add
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.progress;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.progress, comparand + value, comparand);
                }
                while (eventHandler != comparand);
            }
            remove
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.progress;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.progress, comparand - value, comparand);
                }
                while (eventHandler != comparand);
            }
        }

        protected void RaiseCurrentBalanceChanged(string refOldValue, string refNewValue)
        {
            if (this.currentBalanceChanged == null)
                return;
            this.currentBalanceChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }
        protected void RaiseEarnedPointsChanged(string refOldValue, string refNewValue)
        {
            if (this.earnedPointsChanged == null)
                return;
            this.earnedPointsChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }
        protected void RaiseRedeemedPointsChanged(string refOldValue, string refNewValue)
        {
            if (this.redeemedPointsChanged == null)
                return;
            this.redeemedPointsChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }
        protected void RaiseExpirySchedulePointsChanged(string refOldValue, string refNewValue)
        {
            if (this.expirySchedulePointsChanged == null)
                return;
            this.expirySchedulePointsChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }
        protected void RaiseLostPointsChanged(string refOldValue, string refNewValue)
        {
            if (this.lostPointsChanged == null)
                return;
            this.lostPointsChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }
        protected void RaiseErrorFound(string ErrorName)
        {
            if (this.errorFound == null)
                return;
            this.errorFound((object)this, new GenericEventArgs<string>(ErrorName));
        }

        protected virtual void HandleCurrentBalanceChanged(string refOldValue, string refNewValue)
        {
        }
        protected virtual bool ValidateCurrentBalanceChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleEarnedPointsChanged(string refOldValue, string refNewValue)
        {
        }
        protected virtual bool ValidateEarnedPointsChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleRedeemedPointsChanged(string refOldValue, string refNewValue)
        {
        }
        protected virtual bool ValidateRedeemedPointsChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleExpirySchedulePointsChanged(string refOldValue, string refNewValue)
        {
        }
        protected virtual bool ValidateExpirySchedulePointsChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleLostPointsChanged(string refOldValue, string refNewValue)
        {
        }
        protected virtual bool ValidateLostPointsChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

    }
}