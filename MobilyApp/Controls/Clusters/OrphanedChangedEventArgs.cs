﻿using System.Windows;

namespace MobilyApp.Controls.Clusters
{
    public class OrphanedChangedEventArgs : RoutedEventArgs
    {
        // Methods
        public OrphanedChangedEventArgs(object hubData, object spokeData, bool isOrphaned)
            : base(ClustersControl.OrphanedChangedEvent)
        {
            this.HubData = hubData;
            this.SpokeData = spokeData;
            this.IsOrphaned = isOrphaned;
        }

        // Properties
        public object HubData { get; private set; }

        public bool IsOrphaned { get; private set; }

        public object SpokeData { get; private set; }

         
    }
}