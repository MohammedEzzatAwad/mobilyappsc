﻿using System;

namespace MobilyApp.Common
{
    public class GenericEventArgs<T> : EventArgs
    {
        public T Param { get; protected set; }

        public GenericEventArgs(T refParam)
        {
            this.Param = refParam;
        }
    }
}