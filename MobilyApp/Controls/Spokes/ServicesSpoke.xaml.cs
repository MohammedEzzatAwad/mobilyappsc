﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MobilyApp.Controls.Spokes
{
    /// <summary>
    /// Interaction logic for ServicesSpoke.xaml
    /// </summary>
    public partial class ServicesSpoke : UserControl
    {
        public ServicesSpoke()
        {
            InitializeComponent();
        }
    }
}
