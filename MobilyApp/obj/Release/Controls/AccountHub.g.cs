﻿#pragma checksum "..\..\..\Controls\AccountHub.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "61412520235E5141047EB260F0717287"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17626
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation.Controls.Primitives;
using Microsoft.Surface.Presentation.Controls.TouchVisualizations;
using Microsoft.Surface.Presentation.Input;
using Microsoft.Surface.Presentation.Palettes;
using MobilyApp.Controls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace MobilyApp.Controls {
    
    
    /// <summary>
    /// AccountHub
    /// </summary>
    public partial class AccountHub : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 18 "..\..\..\Controls\AccountHub.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.VisualStateGroup LoadingStates;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\Controls\AccountHub.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.VisualState LoadingState;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\Controls\AccountHub.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.VisualState LoadedState;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\Controls\AccountHub.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.VisualState NoResultsErrorState;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\Controls\AccountHub.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.VisualState ServiceErrorState;
        
        #line default
        #line hidden
        
        
        #line 87 "..\..\..\Controls\AccountHub.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock _title;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\..\Controls\AccountHub.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock _lineNumber;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\..\Controls\AccountHub.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel _Controls;
        
        #line default
        #line hidden
        
        
        #line 91 "..\..\..\Controls\AccountHub.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Surface.Presentation.Controls.Primitives.SurfaceToggleButton _ModeCheck;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\..\Controls\AccountHub.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MobilyApp.Controls.CloseButton _Close;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/MobilyApp;component/controls/accounthub.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Controls\AccountHub.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.LoadingStates = ((System.Windows.VisualStateGroup)(target));
            return;
            case 2:
            this.LoadingState = ((System.Windows.VisualState)(target));
            return;
            case 3:
            this.LoadedState = ((System.Windows.VisualState)(target));
            return;
            case 4:
            this.NoResultsErrorState = ((System.Windows.VisualState)(target));
            return;
            case 5:
            this.ServiceErrorState = ((System.Windows.VisualState)(target));
            return;
            case 6:
            this._title = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this._lineNumber = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this._Controls = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 9:
            this._ModeCheck = ((Microsoft.Surface.Presentation.Controls.Primitives.SurfaceToggleButton)(target));
            
            #line 91 "..\..\..\Controls\AccountHub.xaml"
            this._ModeCheck.Checked += new System.Windows.RoutedEventHandler(this.ModeCheck_Checked);
            
            #line default
            #line hidden
            
            #line 91 "..\..\..\Controls\AccountHub.xaml"
            this._ModeCheck.Unchecked += new System.Windows.RoutedEventHandler(this.ModeCheck_Unchecked);
            
            #line default
            #line hidden
            return;
            case 10:
            this._Close = ((MobilyApp.Controls.CloseButton)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

