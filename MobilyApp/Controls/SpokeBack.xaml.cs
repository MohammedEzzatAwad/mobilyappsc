﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface.Presentation.Controls;
using MobilyApp.Core.Utils;
using System.Windows.Media.Animation;
using MobilyApp.Helpers;
using MobilyApp.ViewModels;

namespace MobilyApp.Controls
{
    /// <summary>
    /// Interaction logic for SpokeBack.xaml
    /// </summary>
    public partial class SpokeBack : UserControl
    {
        public static readonly RoutedEvent CloseRequestedEvent;
        public static readonly RoutedEvent ReturnRequestedEvent;
        public static readonly DependencyProperty SpokeErrorToggleProperty;
        public static readonly DependencyProperty SpokeHighlightToggleProperty;
        public static readonly DependencyProperty TagLinkProperty;
        private Image _selectedState;
        private DropShadowEffect ShadowEffect;

        // Methods
        static SpokeBack()
        {
            TagLinkProperty = DependencyProperty.Register("TagLink", typeof(Uri), typeof(SpokeBack), new PropertyMetadata(null, delegate(DependencyObject sender, DependencyPropertyChangedEventArgs e)
            {
                (sender as SpokeBack).UpdateTagLink();
            }));
            SpokeErrorToggleProperty = DependencyProperty.Register("SpokeErrorToggle", typeof(bool?), typeof(SpokeBack), new PropertyMetadata(null, delegate(DependencyObject sender, DependencyPropertyChangedEventArgs e)
            {
                (sender as SpokeBack).UpdateSpokeErrorToggle(e.OldValue as bool?);
            }));
            SpokeHighlightToggleProperty = DependencyProperty.Register("SpokeHighlightToggle", typeof(bool?), typeof(SpokeBack), new PropertyMetadata(null, delegate(DependencyObject sender, DependencyPropertyChangedEventArgs e)
            {
                (sender as SpokeBack).UpdateSpokeHighlightToggle(e.OldValue as bool?);
            }));
            CloseRequestedEvent = EventManager.RegisterRoutedEvent("CloseRequested", RoutingStrategy.Bubble, typeof(EventHandler<RoutedEventArgs>), typeof(SpokeBack));
            ReturnRequestedEvent = EventManager.RegisterRoutedEvent("ReturnRequested", RoutingStrategy.Bubble, typeof(EventHandler<RoutedEventArgs>), typeof(SpokeBack));
        }


        public SpokeBack()
        {
            InitializeComponent();
            base.Loaded +=new RoutedEventHandler(this.BackSpokeLoaded);
            ShadowEffect = base.FindResource("DrobShadowEffect") as DropShadowEffect;
        }

        private void BackSpokeLoaded(object sender, RoutedEventArgs e)
        {
            base.Loaded -= new RoutedEventHandler(this.BackSpokeLoaded);
            if (DataContext != null)
            {
                
                if (DataContext is BillVM)
                {
                    _selectedState = _billBack;

                }
                else if (DataContext is BalanceVM)
                {
                    _selectedState = _balanceBack;
                }
                else if (DataContext is NeqatyVM)
                {
                    _selectedState = _neqatyBack;
                }
                else if (DataContext is PrePaidVM)
                {
                    _selectedState = _prepaidBack;
                }
                else if (DataContext is PostPaidVM)
                {
                    _selectedState = _postpaidBack;
                }
                else if (DataContext is ServicesVM)
                {
                    _selectedState = _servicesBack;
                }
                _selectedState.Effect = ShadowEffect;
                GoToState(_selectedState);
               
            }
        }

        private void GoToState(Image state)
        {
            _billBack.Visibility = state == _billBack ? Visibility.Visible : Visibility.Collapsed;
            _balanceBack.Visibility = state == _balanceBack ? Visibility.Visible : Visibility.Collapsed;
            _neqatyBack.Visibility = state == _neqatyBack ? Visibility.Visible : Visibility.Collapsed;
            _prepaidBack.Visibility = state == _prepaidBack ? Visibility.Visible : Visibility.Collapsed;
            _postpaidBack.Visibility = state == _postpaidBack ? Visibility.Visible : Visibility.Collapsed;
            _servicesBack.Visibility = state == _servicesBack ? Visibility.Visible : Visibility.Collapsed;
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            //Audio.Instance.PlayCue("bes_closeButtonPress_1ch");
            var dataContext = base.DataContext as PersonVM;
            //if (dataContext.IsOrphaned)
            //{
            //    base.RaiseEvent(new RoutedEventArgs(CloseRequestedEvent));
            //}
            //else
            //{
                base.RaiseEvent(new RoutedEventArgs(ReturnRequestedEvent));
            //}
        }

        private void CloseTimer_ManualCompleted(object sender, EventArgs e)
        {
            if (!(base.DataContext as PersonVM).ClosingIn.HasValue)
            {
                base.RaiseEvent(new RoutedEventArgs(CloseRequestedEvent));
            }
        }

        private void Flip_Click(object sender, RoutedEventArgs e)
        {
            //Audio.Instance.PlayCue("bes_cardFlip_1ch");
            ScatterFlip.SetIsFlipped(this.FindVisualParent<ScatterViewItem>(), false);
        }
        private void UpdateSpokeErrorToggle(bool? oldValue)
        {
            if ((base.IsLoaded && oldValue.HasValue) && this.SpokeErrorToggle.HasValue)
            {
               // (this._CloseBtn.Template.Resources["Highlight"] as Storyboard).Clone().Begin(this._CloseBtn, this._CloseBtn.Template);
            }
        }

        private void UpdateSpokeHighlightToggle(bool? oldValue)
        {
            if (base.IsLoaded && oldValue.HasValue)
            {
                ScatterViewItem item = this.FindVisualParent<ScatterViewItem>();
                if (item != null)
                {
                    item.SetRelativeZIndex(RelativeScatterViewZIndex.Topmost);
                    //(base.Resources["Highlight"] as Storyboard).Begin(this);
                }
            }
        }

        private void UpdateTagLink()
        {
            if (base.IsLoaded)
            {
                ScatterViewItem item = this.FindVisualParent<ScatterViewItem>();
                this._SquareLayout.Visibility = Visibility.Collapsed;
                //this._TallLayout.Visibility = Visibility.Collapsed;
                //this._WideLayout.Visibility = Visibility.Collapsed;
                if (Math.Abs((double)(item.ActualWidth - item.ActualHeight)) < 10.0)
                {
                    this._SquareLayout.Visibility = Visibility.Visible;
                    //this._SquareLayout.FindVisualChild<MicrosoftTag>().TagLink = this.TagLink;
                }
                else if (item.ActualWidth > item.ActualHeight)
                {
                    //this._WideLayout.Visibility = Visibility.Visible;
                   // this._WideLayout.FindVisualChild<MicrosoftTag>().TagLink = this.TagLink;
                }
                else if (item.ActualHeight > item.ActualWidth)
                {
                    //this._TallLayout.Visibility = Visibility.Visible;
                   // this._TallLayout.FindVisualChild<MicrosoftTag>().TagLink = this.TagLink;
                }
            }
        }


        // Properties
        public bool? SpokeErrorToggle
        {
            get
            {
                return (bool?)base.GetValue(SpokeErrorToggleProperty);
            }
            set
            {
                base.SetValue(SpokeErrorToggleProperty, value);
            }
        }

        public bool? SpokeHighlightToggle
        {
            get
            {
                return (bool?)base.GetValue(SpokeHighlightToggleProperty);
            }
            set
            {
                base.SetValue(SpokeHighlightToggleProperty, value);
            }
        }

        public Uri TagLink
        {
            get
            {
                return (Uri)base.GetValue(TagLinkProperty);
            }
            set
            {
                base.SetValue(TagLinkProperty, value);
            }
        }

    }
}
