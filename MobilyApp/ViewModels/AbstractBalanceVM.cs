﻿using System;
using System.Threading;
using MobilyApp.Common;

namespace MobilyApp.ViewModels
{
    public class AbstractBalanceVM : ViewModelBase
    {
        protected string m_refExpirationDate = "--";
        protected string m_refBalance = "--";
        protected string m_refFreeMinutes ="--";
        protected string m_refFreeOnNetMinutes = "--";
        protected string m_refFreeSMS = "--";
        protected string m_refFreeOnNetSMS = "--";
        protected string m_refFreeMMS = "--";
        protected string m_refFreeOnNetMMS = "--";
        protected string m_refFreeGPRS = "--";
        protected string m_refNationalFavoriteNumber = "--";
        protected string m_refInternationalFavoriteNumber = "--";
        protected string m_refUnbilledAmount = "--";
        protected string m_refDueAmount = "--";
        protected string m_refLineNumber = "--";
        private PropertyChangedEventHandler<string> expirationDateChanged;
        private PropertyChangedEventHandler<string> balanceChanged;
        private PropertyChangedEventHandler<string> freeMinutesChanged;
        private PropertyChangedEventHandler<string> freeOnNetMinutesChanged;
        private PropertyChangedEventHandler<string> freeSMSChanged;
        private PropertyChangedEventHandler<string> freeOnNetSMSChanged;
        private PropertyChangedEventHandler<string> freeMMSChanged;
        private PropertyChangedEventHandler<string> freeOnNetMMSChanged;
        private PropertyChangedEventHandler<string> freeGPRSChanged;
        private PropertyChangedEventHandler<string> nationalFavoriteNumberChanged;
        private PropertyChangedEventHandler<string> internationalFavoriteNumberChanged;
        private PropertyChangedEventHandler<string> unbilledAmountChanged;
        private PropertyChangedEventHandler<string> dueAmountChanged;
        private PropertyChangedEventHandler<string> lineNumberChanged;
        private EventHandler<GenericEventArgs<string>> errorFound;

        public virtual string ExpirationDate
        {
            get
            {
                return this.m_refExpirationDate;
            }
            set
            {
                string refOldValue = this.m_refExpirationDate;
                if (!this.ValidateExpirationDateChange(refOldValue, value))
                    return;
                this.m_refExpirationDate = value;
                this.HandleExpirationDateChanged(refOldValue, this.m_refExpirationDate);
                this.NotifyPropertyChanged("ExpirationDate");
                this.RaiseExpirationDateChanged(refOldValue, this.m_refExpirationDate);
            }
        }

        public virtual string Balance
        {
            get
            {
                return this.m_refBalance;
            }
            set
            {
                string refOldValue = this.m_refBalance;
                if (!this.ValidateBalanceChange(refOldValue, value))
                    return;
                this.m_refBalance = value;
                this.HandleBalanceChanged(refOldValue, this.m_refBalance);
                this.NotifyPropertyChanged("Balance");
                this.RaiseBalanceChanged(refOldValue, this.m_refBalance);
            }
        }

        public virtual string FreeMinutes
        {
            get
            {
                return this.m_refFreeMinutes;
            }
            set
            {
                string refOldValue = this.m_refFreeMinutes;
                if (!this.ValidateFreeMinutesChange(refOldValue, value))
                    return;
                this.m_refFreeMinutes = value;
                this.HandleFreeMinutesChanged(refOldValue, this.m_refFreeMinutes);
                this.NotifyPropertyChanged("FreeMinutes");
                this.RaiseFreeMinutesChanged(refOldValue, this.m_refFreeMinutes);
            }
        }

        public virtual string FreeOnNetMinutes
        {
            get
            {
                return this.m_refFreeOnNetMinutes;
            }
            set
            {
                string refOldValue = this.m_refFreeOnNetMinutes;
                if (!this.ValidateFreeOnNetMinutesChange(refOldValue, value))
                    return;
                this.m_refFreeOnNetMinutes = value;
                this.HandleFreeOnNetMinutesChanged(refOldValue, this.m_refFreeOnNetMinutes);
                this.NotifyPropertyChanged("FreeOnNetMinutes");
                this.RaiseFreeOnNetMinutesChanged(refOldValue, this.m_refFreeOnNetMinutes);
            }
        } 
        public virtual string FreeSMS
        {
            get
            {
                return this.m_refFreeSMS;
            }
            set
            {
                string refOldValue = this.m_refFreeSMS;
                if (!this.ValidateFreeSMSChange(refOldValue, value))
                    return;
                this.m_refFreeSMS = value;
                this.HandleFreeSMSChanged(refOldValue, this.m_refFreeSMS);
                this.NotifyPropertyChanged("FreeSMS");
                this.RaiseFreeSMSChanged(refOldValue, this.m_refFreeSMS);
            }
        } 
        public virtual string FreeOnNetSMS
        {
            get
            {
                return this.m_refFreeOnNetSMS;
            }
            set
            {
                string refOldValue = this.m_refFreeOnNetSMS;
                if (!this.ValidateFreeOnNetSMSChange(refOldValue, value))
                    return;
                this.m_refFreeOnNetSMS = value;
                this.HandleFreeOnNetSMSChanged(refOldValue, this.m_refFreeOnNetSMS);
                this.NotifyPropertyChanged("FreeOnNetSMS");
                this.RaiseFreeOnNetSMSChanged(refOldValue, this.m_refFreeOnNetSMS);
            }
        } 
        public virtual string FreeMMS
        {
            get
            {
                return this.m_refFreeMMS;
            }
            set
            {
                string refOldValue = this.m_refFreeMMS;
                if (!this.ValidateFreeMMSChange(refOldValue, value))
                    return;
                this.m_refFreeMMS = value;
                this.HandleFreeMMSChanged(refOldValue, this.m_refFreeMMS);
                this.NotifyPropertyChanged("FreeMMS");
                this.RaiseFreeMMSChanged(refOldValue, this.m_refFreeMMS);
            }
        } 
        public virtual string FreeOnNetMMS
        {
            get
            {
                return this.m_refFreeOnNetMMS;
            }
            set
            {
                string refOldValue = this.m_refFreeOnNetMMS;
                if (!this.ValidateFreeOnNetMMSChange(refOldValue, value))
                    return;
                this.m_refFreeOnNetMMS = value;
                this.HandleFreeOnNetMMSChanged(refOldValue, this.m_refFreeOnNetMMS);
                this.NotifyPropertyChanged("FreeOnNetMMS");
                this.RaiseFreeOnNetMMSChanged(refOldValue, this.m_refFreeOnNetMMS);
            }
        } 
        public virtual string FreeGPRS
        {
            get
            {
                return this.m_refFreeGPRS;
            }
            set
            {
                string refOldValue = this.m_refFreeGPRS;
                if (!this.ValidateFreeGPRSChange(refOldValue, value))
                    return;
                this.m_refFreeGPRS = value;
                this.HandleFreeGPRSChanged(refOldValue, this.m_refFreeGPRS);
                this.NotifyPropertyChanged("FreeGPRS");
                this.RaiseFreeGPRSChanged(refOldValue, this.m_refFreeGPRS);
            }
        } 
        public virtual string NationalFavoriteNumber
        {
            get
            {
                return this.m_refNationalFavoriteNumber;
            }
            set
            {
                string refOldValue = this.m_refNationalFavoriteNumber;
                if (!this.ValidateNationalFavoriteNumberChange(refOldValue, value))
                    return;
                this.m_refNationalFavoriteNumber = value;
                this.HandleNationalFavoriteNumberChanged(refOldValue, this.m_refNationalFavoriteNumber);
                this.NotifyPropertyChanged("NationalFavoriteNumber");
                this.RaiseNationalFavoriteNumberChanged(refOldValue, this.m_refNationalFavoriteNumber);
            }
        } 
        public virtual string InternationalFavoriteNumber
        {
            get
            {
                return this.m_refInternationalFavoriteNumber;
            }
            set
            {
                string refOldValue = this.m_refInternationalFavoriteNumber;
                if (!this.ValidateInternationalFavoriteNumberChange(refOldValue, value))
                    return;
                this.m_refInternationalFavoriteNumber = value;
                this.HandleInternationalFavoriteNumberChanged(refOldValue, this.m_refInternationalFavoriteNumber);
                this.NotifyPropertyChanged("InternationalFavoriteNumber");
                this.RaiseInternationalFavoriteNumberChanged(refOldValue, this.m_refInternationalFavoriteNumber);
            }
        } 
        public virtual string UnbilledAmount
        {
            get
            {
                return this.m_refUnbilledAmount;
            }
            set
            {
                string refOldValue = this.m_refUnbilledAmount;
                if (!this.ValidateUnbilledAmountChange(refOldValue, value))
                    return;
                this.m_refUnbilledAmount = value;
                this.HandleUnbilledAmountChanged(refOldValue, this.m_refUnbilledAmount);
                this.NotifyPropertyChanged("UnbilledAmount");
                this.RaiseUnbilledAmountChanged(refOldValue, this.m_refUnbilledAmount);
            }
        } 
        public virtual string DueAmount
        {
            get
            {
                return this.m_refDueAmount;
            }
            set
            {
                string refOldValue = this.m_refDueAmount;
                if (!this.ValidateDueAmountChange(refOldValue, value))
                    return;
                this.m_refDueAmount = value;
                this.HandleDueAmountChanged(refOldValue, this.m_refDueAmount);
                this.NotifyPropertyChanged("DueAmount");
                this.RaiseDueAmountChanged(refOldValue, this.m_refDueAmount);
            }
        } 
        public virtual string LineNumber
        {
            get
            {
                return this.m_refLineNumber;
            }
            set
            {
                string refOldValue = this.m_refLineNumber;
                if (!this.ValidateLineNumberChange(refOldValue, value))
                    return;
                this.m_refLineNumber = value;
                this.HandleLineNumberChanged(refOldValue, this.m_refLineNumber);
                this.NotifyPropertyChanged("LineNumber");
                this.RaiseLineNumberChanged(refOldValue, this.m_refLineNumber);
            }
        }

        public event PropertyChangedEventHandler<string> ExpirationDateChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.expirationDateChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.expirationDateChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.expirationDateChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.expirationDateChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> BalanceChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.balanceChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.balanceChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.balanceChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.balanceChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> FreeMinutesChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.freeMinutesChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.freeMinutesChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.freeMinutesChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.freeMinutesChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> FreeOnNetMinutesChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.freeOnNetMinutesChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.freeOnNetMinutesChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.freeOnNetMinutesChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.freeOnNetMinutesChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> FreeSMSChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.freeSMSChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.freeSMSChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.freeSMSChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.freeSMSChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> FreeOnNetSMSChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.freeOnNetSMSChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.freeOnNetSMSChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.freeOnNetSMSChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.freeOnNetSMSChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> FreeMMSChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.freeMMSChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.freeMMSChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.freeMMSChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.freeMMSChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> FreeOnNetMMSChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.freeOnNetMMSChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.freeOnNetMMSChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.freeOnNetMMSChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.freeOnNetMMSChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> FreeGPRSChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.freeGPRSChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.freeGPRSChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.freeGPRSChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.freeGPRSChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> NationalFavoriteNumberChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.nationalFavoriteNumberChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.nationalFavoriteNumberChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.nationalFavoriteNumberChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.nationalFavoriteNumberChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> InternationalFavoriteNumberChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.internationalFavoriteNumberChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.internationalFavoriteNumberChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.internationalFavoriteNumberChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.internationalFavoriteNumberChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> UnbilledAmountChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.unbilledAmountChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.unbilledAmountChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.unbilledAmountChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.unbilledAmountChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> DueAmountChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.dueAmountChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.dueAmountChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.dueAmountChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.dueAmountChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event PropertyChangedEventHandler<string> LineNumberChanged
        {
            add
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.lineNumberChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.lineNumberChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<string> changedEventHandler = this.lineNumberChanged;
                PropertyChangedEventHandler<string> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<string>>(ref this.lineNumberChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        public event EventHandler<GenericEventArgs<string>> ErrorFound
        {
            add
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.errorFound;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.errorFound, comparand + value, comparand);
                }
                while (eventHandler != comparand);
            }
            remove
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.errorFound;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.errorFound, comparand - value, comparand);
                }
                while (eventHandler != comparand);
            }
        }

        protected void RaiseExpirationDateChanged(string refOldValue, string refNewValue)
        {
            if (this.expirationDateChanged == null)
                return;
            this.expirationDateChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseBalanceChanged(string refOldValue, string refNewValue)
        {
            if (this.balanceChanged == null)
                return;
            this.balanceChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseFreeMinutesChanged(string refOldValue, string refNewValue)
        {
            if (this.freeMinutesChanged == null)
                return;
            this.freeMinutesChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseFreeOnNetMinutesChanged(string refOldValue, string refNewValue)
        {
            if (this.freeOnNetMinutesChanged == null)
                return;
            this.freeOnNetMinutesChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseFreeSMSChanged(string refOldValue, string refNewValue)
        {
            if (this.freeSMSChanged == null)
                return;
            this.freeSMSChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseFreeOnNetSMSChanged(string refOldValue, string refNewValue)
        {
            if (this.freeOnNetSMSChanged == null)
                return;
            this.freeOnNetSMSChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseFreeMMSChanged(string refOldValue, string refNewValue)
        {
            if (this.freeMMSChanged == null)
                return;
            this.freeMMSChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseFreeOnNetMMSChanged(string refOldValue, string refNewValue)
        {
            if (this.freeOnNetMMSChanged == null)
                return;
            this.freeOnNetMMSChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseFreeGPRSChanged(string refOldValue, string refNewValue)
        {
            if (this.freeGPRSChanged == null)
                return;
            this.freeGPRSChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseNationalFavoriteNumberChanged(string refOldValue, string refNewValue)
        {
            if (this.nationalFavoriteNumberChanged == null)
                return;
            this.nationalFavoriteNumberChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseInternationalFavoriteNumberChanged(string refOldValue, string refNewValue)
        {
            if (this.internationalFavoriteNumberChanged == null)
                return;
            this.internationalFavoriteNumberChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseUnbilledAmountChanged(string refOldValue, string refNewValue)
        {
            if (this.unbilledAmountChanged == null)
                return;
            this.unbilledAmountChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseDueAmountChanged(string refOldValue, string refNewValue)
        {
            if (this.dueAmountChanged == null)
                return;
            this.dueAmountChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseLineNumberChanged(string refOldValue, string refNewValue)
        {
            if (this.lineNumberChanged == null)
                return;
            this.lineNumberChanged((object)this, new PropertyChangedEventArgs<string>(refOldValue, refNewValue));
        }

        protected void RaiseErrorFound(string ErrorName)
        {
            if (this.errorFound == null)
                return;
            this.errorFound((object)this, new GenericEventArgs<string>(ErrorName));
        }

        protected virtual void HandleExpirationDateChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateExpirationDateChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleBalanceChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateBalanceChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleFreeMinutesChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateFreeMinutesChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleFreeOnNetMinutesChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateFreeOnNetMinutesChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleFreeSMSChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateFreeSMSChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleFreeOnNetSMSChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateFreeOnNetSMSChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleFreeMMSChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateFreeMMSChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleFreeOnNetMMSChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateFreeOnNetMMSChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleFreeGPRSChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateFreeGPRSChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleNationalFavoriteNumberChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateNationalFavoriteNumberChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleInternationalFavoriteNumberChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateInternationalFavoriteNumberChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleUnbilledAmountChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateUnbilledAmountChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleDueAmountChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateDueAmountChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }

        protected virtual void HandleLineNumberChanged(string refOldValue, string refNewValue)
        {
        }

        protected virtual bool ValidateLineNumberChange(string refOldValue, string refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }
    }
}