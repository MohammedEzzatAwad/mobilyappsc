﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace MobilyApp.Helpers
{
    public class StateManager : DependencyObject
    {
        public static double GetVisualStateProperty(DependencyObject obj)
        {
            return (double)obj.GetValue(VisualStatePropertyProperty);
        }

        public static void SetVisualStateProperty(DependencyObject obj, double value)
        {
            obj.SetValue(VisualStatePropertyProperty, value);
        }

        public static readonly DependencyProperty VisualStatePropertyProperty =
            DependencyProperty.RegisterAttached(
            "VisualStateProperty",
            typeof(double),
            typeof(StateManager),
            new PropertyMetadata((s, e) =>
            {
                var propertyName = (double)e.NewValue;
                var ctrl = s as Control;
                if (ctrl == null)
                    throw new InvalidOperationException("This attached property only supports types derived from Control.");
                System.Windows.VisualStateManager.GoToState(ctrl, (string)e.NewValue, true);
            }));
    }
}