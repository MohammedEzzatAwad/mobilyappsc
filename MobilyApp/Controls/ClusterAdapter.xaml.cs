﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface.Presentation.Controls;
using MobilyApp.Commands;
using MobilyApp.Controls.Clusters;
using MobilyApp.Helpers;
using MobilyApp.ViewModels;

namespace MobilyApp.Controls
{
    /// <summary>
    /// Interaction logic for ClusterAdapter.xaml
    /// </summary>
    public partial class ClusterAdapter : UserControl
    {
        // Private Fields  
        public static readonly DependencyProperty LogInBarProperty = DependencyProperty.Register("LogInBar", typeof(ScatterViewItem), typeof(ClusterAdapter), new PropertyMetadata(null));
        public static readonly DependencyProperty QueriesProperty = DependencyProperty.Register("Queries", typeof(ObservableCollection<PersonVM>), typeof(ClusterAdapter), new PropertyMetadata(null, new PropertyChangedCallback(ClusterAdapter.QueriesPropertyChanged)));
        public static readonly DependencyProperty SpawnOrientationProperty = DependencyProperty.Register("SpawnOrientation", typeof(double), typeof(ClusterAdapter), new PropertyMetadata(0.0));
        public static readonly DependencyProperty SpawnPositionProperty = DependencyProperty.Register("SpawnPosition", typeof(Point), typeof(ClusterAdapter), new PropertyMetadata(new Point()));



        public ClusterAdapter()
        {
            InitializeComponent();
            _Clusters.AddHandler(ClustersControl.IntersectionChangedEvent, new EventHandler<IntersectionChangedEventArgs>(this.Clusters_IntersectionChanged));
            _Clusters.AddHandler(ClustersControl.OrphanedChangedEvent, new EventHandler<OrphanedChangedEventArgs>(this.Clusters_OrphanedChanged));
            _Clusters.AddHandler(Spoke.CloseRequestedEvent, new EventHandler<RoutedEventArgs>(this.Clusters_CloseRequested));
            _Clusters.AddHandler(SpokeBack.CloseRequestedEvent, new EventHandler<RoutedEventArgs>(this.Clusters_CloseRequested));
            _Clusters.AddHandler(Spoke.ReturnRequestedEvent, new EventHandler<RoutedEventArgs>(this.Clusters_ReturnRequested));
            _Clusters.AddHandler(SpokeBack.ReturnRequestedEvent, new EventHandler<RoutedEventArgs>(this.Clusters_ReturnRequested));
            //_Clusters.AddHandler(ScatterViewItem.ContainerActivatedEvent, new EventHandler<RoutedEventArgs>(ContainerActivatedEventHandler));
            //_Clusters.AddHandler(ScatterContentControlBase.ContainerManipulationDeltaEvent, new EventHandler<RoutedEventArgs>(this.ContainerManipulationDeltaEventHandler));
            _Clusters.MouseDown += (sender, e) => {  CancelTimeout(e.OriginalSource); };
            _Clusters.MouseUp += (sender, e) => CancelTimeout(e.OriginalSource);
            _Clusters.TouchDown += (sender, e) => CancelTimeout(e.OriginalSource);
            _Clusters.TouchUp += (sender, e) => CancelTimeout(e.OriginalSource);

            

        }
        private void ContainerActivatedEventHandler(object sender, RoutedEventArgs e)
        {
            CancelTimeout(e.OriginalSource);
        }
        private void ContainerManipulationDeltaEventHandler(object sender, RoutedEventArgs e)
        {
            CancelTimeout(e.OriginalSource);
        }
        private static void CancelTimeout(object source)
        {
            FrameworkElement element = source as FrameworkElement;
            Button button = element as Button;
            if (button == null)
            {
                button = element.FindVisualParent<Button>();
            }
            if (((button == null) || (button.Name == null)) || !button.Name.ToUpper(CultureInfo.InvariantCulture).Contains("CLOSE"))
            {
                ScatterViewItem item = source as ScatterViewItem;
                if (item == null)
                {
                    item = element.FindVisualParent<ScatterViewItem>();
                    if (item == null)
                    {
                        return;
                    }
                }
                PersonVM dataContext = item.DataContext as PersonVM;
                if (dataContext != null)
                {
                    SetClosingInCommand.Execute(dataContext, null);
                }
                else
                {
                    BillVM bill = item.DataContext as BillVM;
                    if (bill != null)
                    {
                        SetClosingInCommand.Execute(bill, null);
                    }

                    BalanceVM balance = item.DataContext as BalanceVM;
                    if (balance != null)
                    {
                        SetClosingInCommand.Execute(balance, null);
                    }
                    NeqatyVM neqaty = item.DataContext as NeqatyVM;
                    if (neqaty != null)
                    {
                        SetClosingInCommand.Execute(neqaty, null);
                    }
                    PrePaidVM prepaid = item.DataContext as PrePaidVM;
                    if (prepaid != null)
                    {
                        SetClosingInCommand.Execute(prepaid, null);
                    }
                    PostPaidVM postpaid = item.DataContext as PostPaidVM;
                    if (postpaid != null)
                    {
                        SetClosingInCommand.Execute(postpaid, null);
                    }
                    ServicesVM services = item.DataContext as ServicesVM;
                    if (services != null)
                    {
                        SetClosingInCommand.Execute(services, null);
                    }
                }
            }
        }

        private void Clusters_CloseRequested(object sender, RoutedEventArgs e)
        {
            PersonVM result = null;
            Spoke originalSource = e.OriginalSource as Spoke;
            if (originalSource != null)
            {
                result = originalSource.DataContext as PersonVM;
            }
            else
            {
                SpokeBack back = e.OriginalSource as SpokeBack;
                result = back.DataContext as PersonVM;
            }
            result.IsOrphaned = false;
            result.IsInCluster = true;
            this._Clusters.RemoveOrphanedSpoke(result);
        }

        private void Clusters_IntersectionChanged(object sender, IntersectionChangedEventArgs e)
        {
            if (!SetIsInClusterCommand.Execute(e.SpokeData as ISpokeData, e.IsIntersecting))
            {
                this._Clusters.ReturnRemovedSpoke(e.SpokeData);
            }
        }
         

        private void Clusters_OrphanedChanged(object sender, OrphanedChangedEventArgs e)
        {
            SetIsOrphanedCommand.Execute(e.SpokeData as PersonVM, e.IsOrphaned);
        }

        private void Clusters_ReturnRequested(object sender, RoutedEventArgs e)
        {
            ISpokeData spokeData = null;
            Spoke originalSource = e.OriginalSource as Spoke;
            if (originalSource != null)
            {
                spokeData = originalSource.DataContext as ISpokeData;
            }
            else
            {
                SpokeBack back = e.OriginalSource as SpokeBack;
                spokeData = back.DataContext as ISpokeData;
            }
            this._Clusters.ReturnRemovedSpoke(spokeData);
            SetIsInClusterCommand.Execute(spokeData, true); 
        }
    

 

        private void Queries_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (PersonVM yvo in e.NewItems)
                {
                    yvo.PropertyChanged += new PropertyChangedEventHandler(this.Query_PropertyChanged);
                    //Audio.Instance.PlayCue("bes_barSpawn_2ch");
                    this._Clusters.AddCluster(yvo, GetSpokesData(yvo), new Point?(this.SpawnPosition), new double?(this.SpawnOrientation), new List<Point> { this.LogInBar.ActualCenter });
                }
            }
            if (e.OldItems != null)
            {
                foreach (PersonVM yvo2 in e.OldItems)
                {
                    yvo2.PropertyChanged -= new PropertyChangedEventHandler(this.Query_PropertyChanged);
                    this._Clusters.RemoveCluster(yvo2);
                }
            }
        }

        private static void QueriesPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            (sender as ClusterAdapter).UpdateQueries(e.OldValue as ObservableCollection<PersonVM>);
        }

        private void Query_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            string str;
            PersonVM hubData = sender as PersonVM;
            if (((str = e.PropertyName) != null))
            {
                if (!(str == "GroupingStyle"))
                {
                        if (str == "Results")
                        {
                            var spokesData = GetSpokesData(hubData);
                            this._Clusters.UpdateCluster(hubData, spokesData);
                        }
                }
                else
                {
                    this._Clusters.SetClusterGroupingStyle(hubData, hubData.GroupingStyle);
                }
            }
        }



        //private void UpdateLocations(ObservableCollection<ResultVO> oldValue)
        //{
        //    if (oldValue != null)
        //    {
        //        oldValue.CollectionChanged -= new NotifyCollectionChangedEventHandler(this.Locations_CollectionChanged);
        //        foreach (ResultVO tvo in oldValue)
        //        {
        //            this._Clusters.RemoveOrphanedSpoke(tvo);
        //        }
        //    }
        //    if (this.Locations != null)
        //    {
        //        this.Locations.CollectionChanged += new NotifyCollectionChangedEventHandler(this.Locations_CollectionChanged);
        //        foreach (ResultVO tvo2 in this.Locations)
        //        {
        //            this._Clusters.AddOrphanedSpoke(tvo2.Query, tvo2, new Point?(this.SpawnPosition), new double?(this.SpawnOrientation), new List<Point> { this.BingBar.ActualCenter });
        //        }
        //    }
        //}

        private void UpdateQueries(ObservableCollection<PersonVM> oldValue)
        {
            if (oldValue != null)
            {
                oldValue.CollectionChanged -= new NotifyCollectionChangedEventHandler(this.Queries_CollectionChanged);
                foreach (PersonVM yvo in oldValue)
                {
                    yvo.PropertyChanged -= new PropertyChangedEventHandler(this.Query_PropertyChanged);
                }
                foreach (object obj2 in this._Clusters.Clusters.Keys)
                {
                    this._Clusters.RemoveCluster(obj2);
                }
            }
            if (this.Queries != null)
            {
                this.Queries.CollectionChanged += new NotifyCollectionChangedEventHandler(this.Queries_CollectionChanged);
                foreach (PersonVM yvo2 in this.Queries)
                {
                    yvo2.PropertyChanged += new PropertyChangedEventHandler(this.Query_PropertyChanged);
                    //Audio.Instance.PlayCue("bes_barSpawn_2ch");
                    var spokesData = GetSpokesData(yvo2);
                    this._Clusters.AddCluster(yvo2,
                                              spokesData, 
                                              new Point?(this.SpawnPosition), 
                                              new double?(this.SpawnOrientation), 
                                              new List<Point> { this.LogInBar.ActualCenter });
                }
            }
        }

        private static IList<object> GetSpokesData(PersonVM yvo2)
        {
            IList<object> spokesData = new List<object>();
            spokesData.Add(yvo2.Balance);
            spokesData.Add(yvo2.Bill);
            spokesData.Add(yvo2.Neqaty);

            spokesData.Add(yvo2.PrePaid);
            spokesData.Add(yvo2.PostPaid);
            spokesData.Add(yvo2.Services);
            return spokesData;
        }
      



        #region Properties
        
        public ScatterViewItem LogInBar
        {
            get
            {
                return (ScatterViewItem)base.GetValue(LogInBarProperty);
            }
            set
            {
                base.SetValue(LogInBarProperty, value);
            }
        }
         
        public ObservableCollection<PersonVM> Queries
        {
            get
            {
                return (ObservableCollection<PersonVM>)base.GetValue(QueriesProperty);
            }
            set
            {
                base.SetValue(QueriesProperty, value);
            }
        }

        public double SpawnOrientation
        {
            get
            {
                return (double)base.GetValue(SpawnOrientationProperty);
            }
            set
            {
                base.SetValue(SpawnOrientationProperty, value);
            }
        }

        public Point SpawnPosition
        {
            get
            {
                return (Point)base.GetValue(SpawnPositionProperty);
            }
            set
            {
                base.SetValue(SpawnPositionProperty, value);
            }
        }

        #endregion
    }
}
