﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace MobilyApp.Helpers
{
    public class ReadOnlyDictionary<TKey, TValue> : IDictionary<TKey, TValue>
    {
        // Fields
        private IDictionary<TKey, TValue> a;

        // Methods
        public ReadOnlyDictionary()
        {
            this.a = new Dictionary<TKey, TValue>();
        }

        public ReadOnlyDictionary(IDictionary<TKey, TValue> dictionary)
        {
            this.a = dictionary;
        }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            throw new NotSupportedException("This dictionary is read-only");
        }

        public void Add(TKey key, TValue value)
        {
            throw new NotSupportedException("This dictionary is read-only");
        }

        public void Clear()
        {
            throw new NotSupportedException("This dictionary is read-only");
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return this.a.Contains(item);
        }

        public bool ContainsKey(TKey key)
        {
            return this.a.ContainsKey(key);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            this.a.CopyTo(array, arrayIndex);
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return this.a.GetEnumerator();
        }

        public bool Remove(TKey key)
        {
            throw new NotSupportedException("This dictionary is read-only");
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            throw new NotSupportedException("This dictionary is read-only");
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.a.GetEnumerator();
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return this.a.TryGetValue(key, out value);
        }

        // Properties
        public int Count
        {
            get
            {
                return this.a.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return true;
            }
        }

        public TValue this[TKey key]
        {
            get
            {
                return this.a[key];
            }
            set
            {
                throw new NotSupportedException("This dictionary is read-only");
            }
        }

        public ICollection<TKey> Keys
        {
            get
            {
                return this.a.Keys;
            }
        }

        public ICollection<TValue> Values
        {
            get
            {
                return this.a.Values;
            }
        }
    }


}