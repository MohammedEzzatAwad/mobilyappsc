﻿using System;
using System.Globalization;
using System.Xml.Linq;
using MobilyApp.ViewModels.Requests;

namespace MobilyApp.ViewModels
{
    public class NeqatyVM : AbstractNeqatyVM, ISpokeData
    {
        // Fields
        private TimeSpan? _ClosingIn;
        private int _Id;
        private bool _IsInCluster = true;

        private bool _SpokeErrorToggle;
        private bool _SpokeHighlightToggle;
        private PersonVM _personVm;
        private string _logoImage;
        public PersonVM ParentPersonVM
        {
            get { return this._personVm; }
            set
            {
                if (_personVm != value)
                {
                    this._personVm = value;
                    base.NotifyPropertyChanged("ParentPersonVM");
                }
            }
        }
        public string LogoImage
        {
            get { return this._logoImage; }
            set
            {
                if (_logoImage != value)
                {
                    this._logoImage = value;
                    NotifyPropertyChanged("LogoImage");
                }
            }
        }
        public TimeSpan? ClosingIn
        {
            get
            {
                return this._ClosingIn;
            }
            set
            {
                if (this._ClosingIn != value)
                {
                    this._ClosingIn = value;
                    base.NotifyPropertyChanged("ClosingIn");
                }
            }
        }

        public int Id
        {
            get
            {
                return this._Id;
            }
            set
            {
                if (this._Id != value)
                {
                    this._Id = value;
                    base.NotifyPropertyChanged("Id");
                }
            }
        }

        public bool IsInCluster
        {
            get
            {
                return this._IsInCluster;
            }
            set
            {
                if (this._IsInCluster != value)
                {
                    this._IsInCluster = value;
                    base.NotifyPropertyChanged("IsInCluster");
                }
            }
        }
        public bool SpokeErrorToggle
        {
            get
            {
                return this._SpokeErrorToggle;
            }
            set
            {
                if (this._SpokeErrorToggle != value)
                {
                    this._SpokeErrorToggle = value;
                    base.NotifyPropertyChanged("SpokeErrorToggle");
                }
            }
        }

        public bool SpokeHighlightToggle
        {
            get
            {
                return this._SpokeHighlightToggle;
            }
            set
            {
                if (this._SpokeHighlightToggle != value)
                {
                    this._SpokeHighlightToggle = value;
                    base.NotifyPropertyChanged("SpokeHighlightToggle");
                }
            }
        }


        public void PostNeqaty(string hashcode)
        {
            string msg =
                BalanceBillRequest.LoginToEPortal(BalanceBillRequest.CreateXmlForBalanceBillNeqaty(hashcode,
                                                                                             Constants.neqaty_LOYALTY_VIEW_SUMMARY));
            msg = msg.Replace("---!>", "-->");
            string error = ConstantsRequest.GetError(msg);
            if (error == null || error == string.Empty)
                this.ParseXml(msg);
            else
                this.RaiseErrorFound("Neqaty :" + error);
        }


        private void ParseXml(string msg)
        {
            CultureInfo invariantCulture = CultureInfo.InvariantCulture;
            string str = string.Empty;
            msg = msg.Replace("---!>", "-->");
            foreach (XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants())
            {
                if (xelement1.Name == (XName)Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        //if (xelement2.Name == (XName)Constants.neqaty_LOYALTY_INQUIRY_ITEMS)
                        //{
                             
                                
                                if (xelement2.Name == (XName)Constants.neqaty_CurrentBalance)
                                {
                                    if (xelement2.Value != null && xelement2.Value != "")
                                        this.CurrentBalance =xelement2.Value;
                                                                        
                                } 
                                else if(xelement2.Name == (XName)Constants.neqaty_EarnedPoints)
                                {
                                    if (xelement2.Value != null && xelement2.Value != "")
                                        this.EarnedPoints = xelement2.Value;
                                                                       
                                }
                                else if (xelement2.Name == (XName)Constants.neqaty_ExpirySchedulerPoints)
                                {
                                    if (xelement2.Value != null && xelement2.Value != "")
                                        this.ExpirySchedulePoints = xelement2.Value;
                                }
                                else if (xelement2.Name == (XName)Constants.neqaty_LostPoints)
                                {
                                    if (xelement2.Value != null && xelement2.Value != "")
                                        this.LostPoints = xelement2.Value;
                                }
                                else if (xelement2.Name == (XName)Constants.neqaty_RedeemedPoints)
                                {
                                    if (xelement2.Value != null && xelement2.Value != "")
                                        this.RedeemedPoints = xelement2.Value;
                                }
                            }
                        //}
                    //}
                }
            }
        }
    }
}