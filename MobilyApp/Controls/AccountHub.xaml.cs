﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface.Presentation.Controls;
using MobilyApp.Commands;
using MobilyApp.Controls.Clusters;
using MobilyApp.Helpers;
using MobilyApp.ViewModels;

namespace MobilyApp.Controls
{
    /// <summary>
    /// Interaction logic for AccountHub.xaml
    /// </summary>
    public partial class AccountHub : UserControl
    {
        #region Dependency Properties

        public static DependencyProperty PersonVMProperty = DependencyProperty.Register("PersonVM",
            typeof(PersonVM),
            typeof(AccountHub),
            new PropertyMetadata(null, (sender, e) =>
        {
            (sender as AccountHub).UpdateResults(e.OldValue as PersonVM);
        }));

        public PersonVM PersonVM
        {
            get { return (PersonVM)GetValue(PersonVMProperty); }
            set { SetValue(PersonVMProperty, value); }
        }

        private void UpdateResults(PersonVM OldpersonVM)
        {
            //TODO: check if the results are updated
            this.UpdateVisualState();
        }

        public static DependencyProperty IsQueryInProgressProperty = DependencyProperty.Register("IsQueryInProgress", typeof(bool), typeof(AccountHub), new PropertyMetadata(false, delegate(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            (sender as AccountHub).UpdateVisualState();
        }));

        public bool IsQueryInProgress
        {
            get { return (bool)base.GetValue(IsQueryInProgressProperty); }
            set { SetValue(IsQueryInProgressProperty, value); }
        }


        private void UpdateVisualState()
        {
            //TODO: write code here
        }



        #endregion

        static AccountHub()
        {
        }

        public AccountHub()
        {
            InitializeComponent();
        }

        private void Close_Clicked(object sender, RoutedEventArgs e)
        {
            Audio.Instance.PlayCue("bes_closeButtonPress_1ch");
            var parentSVI = this.FindVisualParent<ScatterViewItem>();
            PersonVM dataContext = parentSVI.DataContext as PersonVM;
            RemoveQueryCommand.Execute(dataContext as PersonVM);
        }
        private void CloseTimer_Completed(object sender, EventArgs e)
        {
            var parentSVI = this.FindVisualParent<ScatterViewItem>();
            PersonVM dataContext = parentSVI.DataContext as PersonVM;
            RemoveQueryCommand.Execute(dataContext as PersonVM);
        }
        private void ModeCheck_Checked(object sender, RoutedEventArgs e)
        {
            Audio.Instance.PlayCue("bes_buttonPress_1ch");
            var parentSVI = this.FindVisualParent<ScatterViewItem>();
            PersonVM dataContext = parentSVI.DataContext as PersonVM;
            SetQueryGroupingStyleCommand.Execute(dataContext, ClusterGroupingStyle.Stacked);
        }

        private void ModeCheck_Unchecked(object sender, RoutedEventArgs e)
        {
            // Audio.Instance.PlayCue("bes_buttonPress_1ch");
            var parentSVI = this.FindVisualParent<ScatterViewItem>();
            PersonVM dataContext = parentSVI.DataContext as PersonVM;
            SetQueryGroupingStyleCommand.Execute(dataContext, ClusterGroupingStyle.Circular);
        }

    }
}
