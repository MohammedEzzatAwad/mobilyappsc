﻿using System;

namespace MobilyApp.ViewModels
{
    public interface ISpokeData
    {
        bool IsInCluster { get; set; }
        
        TimeSpan? ClosingIn { get; set; }
        
        int Id { get; set; }

        bool SpokeErrorToggle { get; set; }
        
        bool SpokeHighlightToggle { get; set; }

        PersonVM ParentPersonVM { get; set; }

          string LogoImage { get; set; }
    }
}