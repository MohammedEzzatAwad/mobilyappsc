﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace MobilyApp.Controls
{
   public class Plane : ContentControl
{
    // Fields
    private ModelVisual3D _ambientLights;
    private static readonly Vector3D _axisX = new Vector3D(1.0, 0.0, 0.0);
    private static readonly Vector3D _axisY = new Vector3D(0.0, 1.0, 0.0);
    private static readonly Vector3D _axisZ = new Vector3D(0.0, 0.0, 1.0);
    private Border _backContent;
    private Viewport2DVisual3D _backModel;
    private Rect _bounds;
    private PerspectiveCamera _camera;
    private ModelVisual3D _directionalLights;
    private Border _fixedContainer;
    private Border _fixedHiddenContainer;
    private RotateTransform _fixedTransform;
    private Border _frontContent;
    private Viewport2DVisual3D _frontModel;
    private bool _lastIsBackShowing;
    private bool _lastIsPaddingClippingEnabled;
    private bool _lastOnAxis;
    private QuaternionRotation3D _quaternion;
    private RotateTransform3D _rotate;
    private ScaleTransform3D _scale;
    private Viewport3D _viewport;
    public static readonly DependencyProperty BackContentProperty = DependencyProperty.Register("BackContent", typeof(object), typeof(Plane), new PropertyMetadata(null));
    public static readonly DependencyProperty BackContentTemplateProperty = DependencyProperty.Register("BackContentTemplate", typeof(DataTemplate), typeof(Plane), new PropertyMetadata(null));
    public static readonly DependencyProperty BackPaddingProperty;
    public static readonly DependencyProperty CacheInvalidationThresholdMaximumProperty;
    public static readonly DependencyProperty CacheInvalidationThresholdMinimumProperty;
    public static readonly DependencyProperty FieldOfViewProperty;
    public static readonly DependencyProperty FrontPaddingProperty;
    public static readonly DependencyProperty IsPaddingClippingEnabledProperty;
    public static readonly DependencyProperty PlaneBackContentProperty;
    public static readonly DependencyProperty PlaneBackContentTemplateProperty;
    public static readonly DependencyProperty PlaneBackPaddingProperty;
    public static readonly DependencyProperty PlaneCacheInvalidationThresholdMaximumProperty;
    public static readonly DependencyProperty PlaneCacheInvalidationThresholdMinimumProperty;
    public static readonly DependencyProperty PlaneFieldOfViewProperty;
    public static readonly DependencyProperty PlaneFrontPaddingProperty;
    public static readonly DependencyProperty PlaneRotationCenterXProperty;
    public static readonly DependencyProperty PlaneRotationCenterYProperty;
    public static readonly DependencyProperty PlaneRotationCenterZProperty;
    public static readonly DependencyProperty PlaneRotationXProperty;
    public static readonly DependencyProperty PlaneRotationYProperty;
    public static readonly DependencyProperty PlaneRotationZProperty;
    public static readonly DependencyProperty PlaneUseLightsProperty;
    public static readonly DependencyProperty RotationCenterXProperty;
    public static readonly DependencyProperty RotationCenterYProperty;
    public static readonly DependencyProperty RotationCenterZProperty;
    public static readonly DependencyProperty RotationXProperty;
    public static readonly DependencyProperty RotationYProperty;
    public static readonly DependencyProperty RotationZProperty;
    public static readonly DependencyProperty UseLightsProperty;

    // Methods
    static Plane()
    {
        RotationXProperty = DependencyProperty.Register("RotationX", typeof(double), typeof(Plane), new FrameworkPropertyMetadata(0.0, delegate (DependencyObject sender, DependencyPropertyChangedEventArgs e) {
            ((Plane) sender).UpdateRotation();
        }, (sender, baseValue) => ((double) baseValue) % 360.0));
        RotationYProperty = DependencyProperty.Register("RotationY", typeof(double), typeof(Plane), new FrameworkPropertyMetadata(0.0, delegate (DependencyObject sender, DependencyPropertyChangedEventArgs e) {
            ((Plane) sender).UpdateRotation();
        }, (sender, baseValue) => ((double) baseValue) % 360.0));
        RotationZProperty = DependencyProperty.Register("RotationZ", typeof(double), typeof(Plane), new FrameworkPropertyMetadata(0.0, delegate (DependencyObject sender, DependencyPropertyChangedEventArgs e) {
            ((Plane) sender).UpdateRotation();
        }, (sender, baseValue) => ((double) baseValue) % 360.0));
        RotationCenterXProperty = DependencyProperty.Register("RotationCenterX", typeof(double), typeof(Plane), new PropertyMetadata(0.0, delegate (DependencyObject sender, DependencyPropertyChangedEventArgs e) {
            (sender as Plane).UpdateRotationCenter();
        }));
        RotationCenterYProperty = DependencyProperty.Register("RotationCenterY", typeof(double), typeof(Plane), new PropertyMetadata(0.0, delegate (DependencyObject sender, DependencyPropertyChangedEventArgs e) {
            (sender as Plane).UpdateRotationCenter();
        }));
        RotationCenterZProperty = DependencyProperty.Register("RotationCenterZ", typeof(double), typeof(Plane), new PropertyMetadata(0.0, delegate (DependencyObject sender, DependencyPropertyChangedEventArgs e) {
            (sender as Plane).UpdateRotationCenter();
        }));
        FieldOfViewProperty = DependencyProperty.Register("FieldOfView", typeof(double), typeof(Plane), new FrameworkPropertyMetadata(45.0, delegate (DependencyObject sender, DependencyPropertyChangedEventArgs e) {
            ((Plane) sender).UpdateCamera();
        }, (sender, baseValue) => Math.Min(Math.Max((double) baseValue, 0.5), 179.9)));
        UseLightsProperty = DependencyProperty.Register("UseLights", typeof(bool), typeof(Plane), new PropertyMetadata(true, delegate (DependencyObject sender, DependencyPropertyChangedEventArgs e) {
            (sender as Plane).UpdateLights();
        }));
        CacheInvalidationThresholdMaximumProperty = DependencyProperty.Register("CacheInvalidationThresholdMaximum", typeof(double), typeof(Plane), new PropertyMetadata(2.0, delegate (DependencyObject sender, DependencyPropertyChangedEventArgs e) {
            (sender as Plane).UpdateCacheInvalidationThresholdMaximum();
        }));
        CacheInvalidationThresholdMinimumProperty = DependencyProperty.Register("CacheInvalidationThresholdMinimum", typeof(double), typeof(Plane), new PropertyMetadata(2.0, delegate (DependencyObject sender, DependencyPropertyChangedEventArgs e) {
            (sender as Plane).UpdateCacheInvalidationThresholdMinimum();
        }));
        FrontPaddingProperty = DependencyProperty.Register("FrontPadding", typeof(Thickness), typeof(Plane), new PropertyMetadata(new Thickness()));
        BackPaddingProperty = DependencyProperty.Register("BackPadding", typeof(Thickness), typeof(Plane), new PropertyMetadata(new Thickness()));
        IsPaddingClippingEnabledProperty = DependencyProperty.Register("IsPaddingClippingEnabled", typeof(bool), typeof(Plane), new PropertyMetadata(true, delegate (DependencyObject sender, DependencyPropertyChangedEventArgs e) {
            (sender as Plane).UpdateRotation();
        }));
        PlaneBackContentProperty = DependencyProperty.RegisterAttached("PlaneBackContent", typeof(FrameworkElement), typeof(Plane), new UIPropertyMetadata(null));
        PlaneBackContentTemplateProperty = DependencyProperty.RegisterAttached("PlaneBackContentTemplate", typeof(DataTemplate), typeof(Plane), new UIPropertyMetadata(null));
        PlaneRotationXProperty = DependencyProperty.RegisterAttached("PlaneRotationX", typeof(double), typeof(Plane), new UIPropertyMetadata(0.0));
        PlaneRotationYProperty = DependencyProperty.RegisterAttached("PlaneRotationY", typeof(double), typeof(Plane), new UIPropertyMetadata(0.0));
        PlaneRotationZProperty = DependencyProperty.RegisterAttached("PlaneRotationZ", typeof(double), typeof(Plane), new UIPropertyMetadata(0.0));
        PlaneRotationCenterXProperty = DependencyProperty.RegisterAttached("PlaneRotationCenterX", typeof(double), typeof(Plane), new UIPropertyMetadata(0.0));
        PlaneRotationCenterYProperty = DependencyProperty.RegisterAttached("PlaneRotationCenterY", typeof(double), typeof(Plane), new UIPropertyMetadata(0.0));
        PlaneRotationCenterZProperty = DependencyProperty.RegisterAttached("PlaneRotationCenterZ", typeof(double), typeof(Plane), new UIPropertyMetadata(0.0));
        PlaneFieldOfViewProperty = DependencyProperty.RegisterAttached("PlaneFieldOfView", typeof(double), typeof(Plane), new UIPropertyMetadata(45.0));
        PlaneUseLightsProperty = DependencyProperty.RegisterAttached("PlaneUseLights", typeof(bool), typeof(Plane), new UIPropertyMetadata(UseLightsProperty.DefaultMetadata.DefaultValue));
        PlaneCacheInvalidationThresholdMaximumProperty = DependencyProperty.RegisterAttached("PlaneCacheInvalidationThresholdMaximum", typeof(double), typeof(Plane), new UIPropertyMetadata(0.5));
        PlaneCacheInvalidationThresholdMinimumProperty = DependencyProperty.RegisterAttached("PlaneCacheInvalidationThresholdMinimum", typeof(double), typeof(Plane), new UIPropertyMetadata(2.0));
        PlaneFrontPaddingProperty = DependencyProperty.RegisterAttached("PlaneFrontPadding", typeof(Thickness), typeof(Plane), new UIPropertyMetadata(new Thickness()));
        PlaneBackPaddingProperty = DependencyProperty.RegisterAttached("PlaneBackPadding", typeof(Thickness), typeof(Plane), new UIPropertyMetadata(new Thickness()));
        FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(Plane), new FrameworkPropertyMetadata(typeof(Plane)));
    }

    public Plane()
    {
        RoutedEventHandler handler = null;
        if (handler == null)
        {
            handler = delegate (object sender, RoutedEventArgs e) {
                this.UpdateRotation();
            };
        }
        base.Loaded += handler;
    }

    public static FrameworkElement GetPlaneBackContent(DependencyObject obj)
    {
        if (obj == null)
        {
            return null;
        }
        return (FrameworkElement) obj.GetValue(PlaneBackContentProperty);
    }

    public static DataTemplate GetPlaneBackContentTemplate(DependencyObject obj)
    {
        if (obj == null)
        {
            return null;
        }
        return (DataTemplate) obj.GetValue(PlaneBackContentTemplateProperty);
    }

    public static Thickness GetPlaneBackPadding(DependencyObject obj)
    {
        if (obj == null)
        {
            return new Thickness();
        }
        return (Thickness) obj.GetValue(PlaneBackPaddingProperty);
    }

    public static double GetPlaneCacheInvalidationThresholdMaximum(DependencyObject obj)
    {
        if (obj == null)
        {
            return 0.0;
        }
        return (double) obj.GetValue(PlaneCacheInvalidationThresholdMaximumProperty);
    }

    public static double GetPlaneCacheInvalidationThresholdMinimum(DependencyObject obj)
    {
        if (obj == null)
        {
            return 0.0;
        }
        return (double) obj.GetValue(PlaneCacheInvalidationThresholdMinimumProperty);
    }

    public static double GetPlaneFieldOfView(DependencyObject obj)
    {
        if (obj == null)
        {
            return 0.0;
        }
        return (double) obj.GetValue(PlaneFieldOfViewProperty);
    }

    public static Thickness GetPlaneFrontPadding(DependencyObject obj)
    {
        if (obj == null)
        {
            return new Thickness();
        }
        return (Thickness) obj.GetValue(PlaneFrontPaddingProperty);
    }

    public static double GetPlaneRotationCenterX(DependencyObject obj)
    {
        if (obj == null)
        {
            return 0.0;
        }
        return (double) obj.GetValue(PlaneRotationCenterXProperty);
    }

    public static double GetPlaneRotationCenterY(DependencyObject obj)
    {
        if (obj == null)
        {
            return 0.0;
        }
        return (double) obj.GetValue(PlaneRotationCenterYProperty);
    }

    public static double GetPlaneRotationCenterZ(DependencyObject obj)
    {
        if (obj == null)
        {
            return 0.0;
        }
        return (double) obj.GetValue(PlaneRotationCenterZProperty);
    }

    public static double GetPlaneRotationX(DependencyObject obj)
    {
        if (obj == null)
        {
            return 0.0;
        }
        return (double) obj.GetValue(PlaneRotationXProperty);
    }

    public static double GetPlaneRotationY(DependencyObject obj)
    {
        if (obj == null)
        {
            return 0.0;
        }
        return (double) obj.GetValue(PlaneRotationYProperty);
    }

    public static double GetPlaneRotationZ(DependencyObject obj)
    {
        if (obj == null)
        {
            return 0.0;
        }
        return (double) obj.GetValue(PlaneRotationZProperty);
    }

    public static bool GetPlaneUseLights(DependencyObject obj)
    {
        if (obj == null)
        {
            return false;
        }
        return (bool) obj.GetValue(PlaneUseLightsProperty);
    }

    private static bool IsBackShowingRotation(double x, double y, double z)
    {
        Matrix3D matrixd = new Matrix3D();
        matrixd.Rotate(new Quaternion(new Vector3D(1.0, 0.0, 0.0), x));
        matrixd.Rotate(new Quaternion(new Vector3D(0.0, 1.0, 0.0) * matrixd, y));
        matrixd.Rotate(new Quaternion(new Vector3D(0.0, 0.0, 1.0) * matrixd, z));
        Vector3D vectord = matrixd.Transform(new Vector3D(0.0, 0.0, 1.0));
        return (Vector3D.DotProduct(new Vector3D(0.0, 0.0, 1.0), vectord) < 0.0);
    }

    public override void OnApplyTemplate()
    {
        this._viewport = base.GetTemplateChild("PART_Viewport") as Viewport3D;
        this._camera = base.GetTemplateChild("PART_Camera") as PerspectiveCamera;
        this._frontContent = base.GetTemplateChild("PART_FrontContent") as Border;
        this._frontModel = base.GetTemplateChild("PART_FrontModel") as Viewport2DVisual3D;
        this._backContent = base.GetTemplateChild("PART_BackContent") as Border;
        this._backModel = base.GetTemplateChild("PART_BackModel") as Viewport2DVisual3D;
        this._scale = base.GetTemplateChild("PART_Scale") as ScaleTransform3D;
        this._rotate = base.GetTemplateChild("PART_Rotate") as RotateTransform3D;
        this._quaternion = base.GetTemplateChild("PART_Quaternion") as QuaternionRotation3D;
        this._fixedContainer = base.GetTemplateChild("PART_FixedContainer") as Border;
        this._fixedHiddenContainer = base.GetTemplateChild("PART_FixedHiddenContainer") as Border;
        this._fixedTransform = base.GetTemplateChild("PART_FixedTransform") as RotateTransform;
        this._directionalLights = base.GetTemplateChild("PART_DirectionalLights") as ModelVisual3D;
        this._ambientLights = base.GetTemplateChild("PART_AmbientLights") as ModelVisual3D;
        base.SizeChanged += delegate (object sender, SizeChangedEventArgs e) {
            this.UpdateBounds();
        };
        this._frontContent.SizeChanged += delegate (object sender, SizeChangedEventArgs e) {
            this.UpdateBounds();
        };
        this._backContent.SizeChanged += delegate (object sender, SizeChangedEventArgs e) {
            this.UpdateBounds();
        };
        this.UpdateBounds();
        this.UpdateRotation();
        this.UpdateLights();
        this.UpdateCacheInvalidationThresholdMaximum();
        this.UpdateCacheInvalidationThresholdMinimum();
        base.OnApplyTemplate();
    }

    public static void SetPlaneBackContent(DependencyObject obj, FrameworkElement value)
    {
        if (obj != null)
        {
            obj.SetValue(PlaneBackContentProperty, value);
        }
    }

    public static void SetPlaneBackContentTemplate(DependencyObject obj, DataTemplate value)
    {
        if (obj != null)
        {
            obj.SetValue(PlaneBackContentTemplateProperty, value);
        }
    }

    public static void SetPlaneBackPadding(DependencyObject obj, Thickness value)
    {
        if (obj != null)
        {
            obj.SetValue(PlaneBackPaddingProperty, value);
        }
    }

    public static void SetPlaneCacheInvalidationThresholdMaximum(DependencyObject obj, double value)
    {
        if (obj != null)
        {
            obj.SetValue(PlaneCacheInvalidationThresholdMaximumProperty, value);
        }
    }

    public static void SetPlaneCacheInvalidationThresholdMinimum(DependencyObject obj, double value)
    {
        if (obj != null)
        {
            obj.SetValue(PlaneCacheInvalidationThresholdMinimumProperty, value);
        }
    }

    public static void SetPlaneFieldOfView(DependencyObject obj, double value)
    {
        if (obj != null)
        {
            obj.SetValue(PlaneFieldOfViewProperty, value);
        }
    }

    public static void SetPlaneFrontPadding(DependencyObject obj, Thickness value)
    {
        if (obj != null)
        {
            obj.SetValue(PlaneFrontPaddingProperty, value);
        }
    }

    public static void SetPlaneRotationCenterX(DependencyObject obj, double value)
    {
        if (obj != null)
        {
            obj.SetValue(PlaneRotationCenterXProperty, value);
        }
    }

    public static void SetPlaneRotationCenterY(DependencyObject obj, double value)
    {
        if (obj != null)
        {
            obj.SetValue(PlaneRotationCenterYProperty, value);
        }
    }

    public static void SetPlaneRotationCenterZ(DependencyObject obj, double value)
    {
        if (obj != null)
        {
            obj.SetValue(PlaneRotationCenterZProperty, value);
        }
    }

    public static void SetPlaneRotationX(DependencyObject obj, double value)
    {
        if (obj != null)
        {
            obj.SetValue(PlaneRotationXProperty, value);
        }
    }

    public static void SetPlaneRotationY(DependencyObject obj, double value)
    {
        if (obj != null)
        {
            obj.SetValue(PlaneRotationYProperty, value);
        }
    }

    public static void SetPlaneRotationZ(DependencyObject obj, double value)
    {
        if (obj != null)
        {
            obj.SetValue(PlaneRotationZProperty, value);
        }
    }

    public static void SetPlaneUseLights(DependencyObject obj, bool value)
    {
        if (obj != null)
        {
            obj.SetValue(PlaneUseLightsProperty, value);
        }
    }

    private void UpdateBounds()
    {
        this._bounds = VisualTreeHelper.GetDescendantBounds(this._frontContent);
        this.UpdateClip();
        this.UpdateRotation();
        this.UpdateCamera();
        this.UpdateRotationCenter();
    }

    private void UpdateCacheInvalidationThresholdMaximum()
    {
        if ((this._frontModel != null) && (this._backModel != null))
        {
            RenderOptions.SetCacheInvalidationThresholdMaximum(this._frontModel, this.CacheInvalidationThresholdMaximum);
            RenderOptions.SetCacheInvalidationThresholdMaximum(this._backModel, this.CacheInvalidationThresholdMaximum);
        }
    }

    private void UpdateCacheInvalidationThresholdMinimum()
    {
        if ((this._frontModel != null) && (this._backModel != null))
        {
            RenderOptions.SetCacheInvalidationThresholdMinimum(this._frontModel, this.CacheInvalidationThresholdMinimum);
            RenderOptions.SetCacheInvalidationThresholdMinimum(this._backModel, this.CacheInvalidationThresholdMinimum);
        }
    }

    private void UpdateCamera()
    {
        if (this._camera != null)
        {
            double num = this.FieldOfView * 0.017453292519943295;
            double z = (this._bounds.Width / Math.Tan(num / 2.0)) / 2.0;
            this._camera.Position = new Point3D(this._bounds.Width / 2.0, this._bounds.Height / 2.0, z);
            this._camera.FieldOfView = this.FieldOfView;
            this._scale.ScaleX = this._bounds.Width;
            this._scale.ScaleY = this._bounds.Height;
        }
    }

    private void UpdateClip()
    {
        bool isOnAxis = this.IsOnAxis;
        bool flag2 = IsBackShowingRotation(this.RotationX, this.RotationY, this.RotationZ);
        if (isOnAxis && this.IsPaddingClippingEnabled)
        {
            Rect rect = flag2 ? VisualTreeHelper.GetDescendantBounds(this._backContent) : this._bounds;
            FrameworkElement element = flag2 ? this._backContent : this._frontContent;
            if (rect != Rect.Empty)
            {
                Thickness thickness = flag2 ? this.BackPadding : this.FrontPadding;
                rect = new Rect(rect.X + thickness.Left, rect.Y + thickness.Top, (element.ActualWidth - thickness.Left) - thickness.Right, (element.ActualHeight - thickness.Bottom) - thickness.Top);
                RectangleGeometry geometry = new RectangleGeometry {
                    Rect = rect
                };
                base.Clip = geometry;
            }
        }
        else
        {
            base.Clip = null;
        }
    }

    private void UpdateLights()
    {
        if (this._viewport != null)
        {
            this._viewport.Children.Remove(this._ambientLights);
            this._viewport.Children.Remove(this._directionalLights);
            this._viewport.Children.Insert(0, this.UseLights ? this._directionalLights : this._ambientLights);
        }
    }

    private void UpdateQuaternion()
    {
        if (this._quaternion != null)
        {
            Quaternion quaternion = new Quaternion(_axisX, this.RotationX);
            Quaternion quaternion2 = new Quaternion(_axisY, this.RotationY);
            Quaternion quaternion3 = new Quaternion(_axisZ, this.RotationZ);
            this._quaternion.Quaternion = (quaternion * quaternion2) * quaternion3;
        }
    }

    private void UpdateRotation()
    {
        if ((this._frontModel != null) && (this._bounds != Rect.Empty))
        {
            bool isOnAxis = this.IsOnAxis;
            bool flag2 = IsBackShowingRotation(this.RotationX, this.RotationY, this.RotationZ);
            if ((isOnAxis != this._lastOnAxis) || (this.IsPaddingClippingEnabled != this._lastIsPaddingClippingEnabled))
            {
                this.UpdateClip();
            }
            if (isOnAxis)
            {
                if (!this._fixedTransform.IsFrozen)
                {
                    this._fixedTransform.Angle = flag2 ? (this.RotationZ + 180.0) : -this.RotationZ;
                    if (Math.Abs(this.RotationX) == 180.0)
                    {
                        this._fixedTransform.Angle += 180.0;
                    }
                }
            }
            else
            {
                this.UpdateQuaternion();
            }
            if ((flag2 != this._lastIsBackShowing) || (isOnAxis != this._lastOnAxis))
            {
                this._frontModel.Visual = null;
                this._backModel.Visual = null;
                this._fixedContainer.Child = null;
                this._fixedHiddenContainer.Child = null;
                if (isOnAxis)
                {
                    this._fixedContainer.Child = flag2 ? this._backContent : this._frontContent;
                    this._fixedHiddenContainer.Child = flag2 ? this._frontContent : this._backContent;
                }
                else if (flag2)
                {
                    this._backModel.Visual = this._backContent;
                }
                else
                {
                    this._frontModel.Visual = this._frontContent;
                }
                if (isOnAxis != this._lastOnAxis)
                {
                    this.UpdateRotationCenter();
                }
            }
            this._lastOnAxis = isOnAxis;
            this._lastIsBackShowing = flag2;
            this._lastIsPaddingClippingEnabled = this.IsPaddingClippingEnabled;
        }
    }

    private void UpdateRotationCenter()
    {
        if ((this._fixedTransform != null) && (this._rotate != null))
        {
            if (this.IsOnAxis)
            {
                this._fixedTransform.CenterX = (this._bounds.Width / 2.0) + this.RotationCenterX;
                this._fixedTransform.CenterY = (this._bounds.Height / 2.0) - this.RotationCenterY;
            }
            else
            {
                this._rotate.CenterX = (this._bounds.Width / 2.0) + this.RotationCenterX;
                this._rotate.CenterY = (this._bounds.Height / 2.0) - this.RotationCenterY;
                this._rotate.CenterZ = this.RotationCenterZ;
            }
        }
    }

    // Properties
    public object BackContent
    {
        get
        {
            return base.GetValue(BackContentProperty);
        }
        set
        {
            base.SetValue(BackContentProperty, value);
        }
    }

    public DataTemplate BackContentTemplate
    {
        get
        {
            return (DataTemplate) base.GetValue(BackContentTemplateProperty);
        }
        set
        {
            base.SetValue(BackContentTemplateProperty, value);
        }
    }

    public Thickness BackPadding
    {
        get
        {
            return (Thickness) base.GetValue(BackPaddingProperty);
        }
        set
        {
            base.SetValue(BackPaddingProperty, value);
        }
    }

    public double CacheInvalidationThresholdMaximum
    {
        get
        {
            return (double) base.GetValue(CacheInvalidationThresholdMaximumProperty);
        }
        set
        {
            base.SetValue(CacheInvalidationThresholdMaximumProperty, value);
        }
    }

    public double CacheInvalidationThresholdMinimum
    {
        get
        {
            return (double) base.GetValue(CacheInvalidationThresholdMinimumProperty);
        }
        set
        {
            base.SetValue(CacheInvalidationThresholdMinimumProperty, value);
        }
    }

    public double FieldOfView
    {
        get
        {
            return (double) base.GetValue(FieldOfViewProperty);
        }
        set
        {
            base.SetValue(FieldOfViewProperty, value);
        }
    }

    public Thickness FrontPadding
    {
        get
        {
            return (Thickness) base.GetValue(FrontPaddingProperty);
        }
        set
        {
            base.SetValue(FrontPaddingProperty, value);
        }
    }

    private bool IsOnAxis
    {
        get
        {
            return ((((this.RotationX % 180.0) == 0.0) && ((this.RotationY % 180.0) == 0.0)) && ((this.RotationZ % 180.0) == 0.0));
        }
    }

    public bool IsPaddingClippingEnabled
    {
        get
        {
            return (bool) base.GetValue(IsPaddingClippingEnabledProperty);
        }
        set
        {
            base.SetValue(IsPaddingClippingEnabledProperty, value);
        }
    }

    public double RotationCenterX
    {
        get
        {
            return (double) base.GetValue(RotationCenterXProperty);
        }
        set
        {
            base.SetValue(RotationCenterXProperty, value);
        }
    }

    public double RotationCenterY
    {
        get
        {
            return (double) base.GetValue(RotationCenterYProperty);
        }
        set
        {
            base.SetValue(RotationCenterYProperty, value);
        }
    }

    public double RotationCenterZ
    {
        get
        {
            return (double) base.GetValue(RotationCenterZProperty);
        }
        set
        {
            base.SetValue(RotationCenterZProperty, value);
        }
    }

    public double RotationX
    {
        get
        {
            return (double) base.GetValue(RotationXProperty);
        }
        set
        {
            base.SetValue(RotationXProperty, value);
        }
    }

    public double RotationY
    {
        get
        {
            return (double) base.GetValue(RotationYProperty);
        }
        set
        {
            base.SetValue(RotationYProperty, value);
        }
    }

    public double RotationZ
    {
        get
        {
            return (double) base.GetValue(RotationZProperty);
        }
        set
        {
            base.SetValue(RotationZProperty, value);
        }
    }

    public bool UseLights
    {
        get
        {
            return (bool) base.GetValue(UseLightsProperty);
        }
        set
        {
            base.SetValue(UseLightsProperty, value);
        }
    }
}
     
} 