﻿using System;
using MobilyApp.ViewModels;

namespace MobilyApp.Commands
{
    public class SetClosingInCommand
    {
        // Methods
        internal static void Execute(PersonVM person, TimeSpan? closingIn)
        {
            person.ClosingIn = closingIn;
            if (person.Balance != null)
            {
                person.Balance.ClosingIn = closingIn;
            }
            if (person.Bill != null)
            {
                person.Bill.ClosingIn = closingIn;
            }

            if (!closingIn.HasValue)
            {
                ResetTimeout(person);
            }
        }

        internal static void Execute(object result, TimeSpan? closingIn)
        {
            //var bill = result as BillVM;
            //if (bill != null)
            //{
            //    bill.ClosingIn = closingIn;
            //}
            //if (!closingIn.HasValue)
            //{
            //    ResetTimeout(bill.ParentPersonVM);
            //}
            //var balance = result as BalanceVM;
            //if (balance != null)
            //{
            //    balance.ClosingIn = closingIn;
            //}
            //if (!closingIn.HasValue)
            //{
            //    ResetTimeout(balance.ParentPersonVM);
            //}

        }


        private static void ResetTimeout(PersonVM query)
        {
            if (query.IdleTimer != null)
            {
                query.IdleTimer.Stop();
                query.IdleTimer.Start();
            }
            if (query.ClosingIn.HasValue)
            {
                query.ClosingIn = null;
                if (query.Bill != null)
                {
                    query.Bill.ClosingIn = null;
                }
                if (query.Balance != null)
                {
                    query.Balance.ClosingIn = null;
                }
            }
        }
    }

}