﻿using System.Windows;

namespace MobilyApp.Controls.Clusters
{
    public class IntersectionChangedEventArgs : RoutedEventArgs
    {
        // Methods
        public IntersectionChangedEventArgs(object hubData, object spokeData, bool isIntersecting)
            : base(ClustersControl.IntersectionChangedEvent)
        {
            this.HubData = hubData;
            this.SpokeData = spokeData;
            this.IsIntersecting = isIntersecting;
        }

        // Properties
        public object HubData { get; private set; }

        public bool IsIntersecting { get; private set; }

        public object SpokeData { get; private set; }
 
    }
}