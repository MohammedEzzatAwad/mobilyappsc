﻿using System;
using System.Collections.Generic;
using System.Timers;
using MobilyApp.Common;
using MobilyApp.Controls.Clusters;
using MobilyApp.Properties;
using MobilyApp.ViewModels.Requests;

namespace MobilyApp.ViewModels
{
    public class PersonVM : AbstractPersonVM
    {
        private bool _IsInCluster = true;
        private bool _IsOrphaned;
        private bool _SpokeErrorToggle;
        private bool _SpokeHighlightToggle;
        private TimeSpan? _ClosingIn;
        private int _Id;
        private ClusterGroupingStyle _GroupingStyle;
        private Timer _IdleTimer;

        public IList<ISpokeData> _results
                             ;
        public IList<ISpokeData> Results
        {
            get { return _results; }
            set 
            {
                _results = value;
                NotifyPropertyChanged("Results");
            }
        }

        public PersonVM(string hashCode)
        {
            if (hashCode == null && !(hashCode != string.Empty))
                return;
            this.HashCode = hashCode;

             this.IdleTimer = new Timer(Settings.Default.IdleTimeout.TotalMilliseconds);
        }

        public void PostPerson()
        {
            if (this.HashCode == null && !(this.HashCode != string.Empty))
                return;
            Results = new List<ISpokeData>();

            this.Balance = new BalanceVM();
            this.Balance.ParentPersonVM = this;
            this.Balance.ErrorFound += new EventHandler<GenericEventArgs<string>>(this.BalanceBill_ErrorFound);
            this.Balance.PostBalance(this.HashCode);
            this.RaiseProgress(Constants.progress_Balance);
            this.Results.Add(Balance);
            this.Balance.LogoImage = @"\Images\Balance.png";

            this.Bill = new BillVM();
            this.Bill.ParentPersonVM = this;
            this.Bill.ErrorFound += new EventHandler<GenericEventArgs<string>>(this.BalanceBill_ErrorFound);
            this.Bill.Progress += new EventHandler<GenericEventArgs<string>>(this.Bill_Progress);
            this.Bill.PostBill(this.HashCode);
            this.Results.Add(Bill);
            this.Bill.LogoImage = @"\Images\Bill.png";

            this.Neqaty = new NeqatyVM();
            this.Neqaty.ParentPersonVM = this;
            this.Neqaty.ErrorFound += new EventHandler<GenericEventArgs<string>>(this.Neqaty_ErrorFound);
            this.Neqaty.Progress += new EventHandler<GenericEventArgs<string>>(this.Neqaty_Progress);
            this.Neqaty.PostNeqaty(this.HashCode);
            this.RaiseProgress(Constants.progress_Neqaty);
            this.Results.Add(Neqaty);
            this.Neqaty.LogoImage = @"\Images\Neqaty.png";


            this.PrePaid = new PrePaidVM();
            this.PrePaid.ParentPersonVM = this;
            this.PrePaid.ErrorFound += new EventHandler<GenericEventArgs<string>>(this.PrePaid_ErrorFound);
            this.PrePaid.Progress += new EventHandler<GenericEventArgs<string>>(this.PrePaid_Progress); 
            this.PrePaid.PostPrePaid(this.HashCode);
            this.RaiseProgress(Constants.progress_PrePaid);
            this.Results.Add(PrePaid);
            this.PrePaid.LogoImage = @"\Images\PrePaid.png";

            this.PostPaid = new PostPaidVM();
            this.PostPaid.ParentPersonVM = this;
            this.PostPaid.ErrorFound += new EventHandler<GenericEventArgs<string>>(this.PostPaid_ErrorFound);
            this.PostPaid.Progress += new EventHandler<GenericEventArgs<string>>(this.PostPaid_Progress);
            this.PostPaid.PostPostPaid(this.HashCode);
            this.RaiseProgress(Constants.progress_PostPaid);
            this.Results.Add(PostPaid);
            this.PostPaid.LogoImage = @"\Images\PostPaid.png";


            this.Services = new ServicesVM();
            this.Services.ParentPersonVM = this;
            this.Services.ErrorFound += new EventHandler<GenericEventArgs<string>>(this.Services_ErrorFound);
            this.Services.Progress += new EventHandler<GenericEventArgs<string>>(this.Services_Progress);
            this.Services.PostServices(this.HashCode);
            this.RaiseProgress(Constants.progress_Services);
            this.Results.Add(Services);
            this.Services.LogoImage = @"\Images\Services.png";

        }

        private void Bill_Progress(object sender, GenericEventArgs<string> e)
        {
            this.RaiseProgress(e.Param);
        }

        private void BalanceBill_ErrorFound(object sender, GenericEventArgs<string> e)
        {
            this.RaiseErrorFound(e.Param);
        }

        private void Neqaty_Progress(object sender, GenericEventArgs<string> e)
        {

            this.RaiseProgress(e.Param);  
        }
        private void Neqaty_ErrorFound(object sender, GenericEventArgs<string> e)
        {
            RaiseErrorFound(e.Param);
        }
        private void PrePaid_Progress(object sender, GenericEventArgs<string> e)
        {

            this.RaiseProgress(e.Param);
        }
        private void PrePaid_ErrorFound(object sender, GenericEventArgs<string> e)
        {
            RaiseErrorFound(e.Param);
        }
        private void PostPaid_Progress(object sender, GenericEventArgs<string> e)
        {

            this.RaiseProgress(e.Param);
        }
        private void PostPaid_ErrorFound(object sender, GenericEventArgs<string> e)
        {
            RaiseErrorFound(e.Param);
        }
        private void Services_Progress(object sender, GenericEventArgs<string> e)
        {

            this.RaiseProgress(e.Param);
        }
        private void Services_ErrorFound(object sender, GenericEventArgs<string> e)
        {
            RaiseErrorFound(e.Param);
        }
        public TimeSpan? ClosingIn
        {
            get
            {
                return this._ClosingIn;
            }
            set
            {
                if (this._ClosingIn != value)
                {
                    this._ClosingIn = value;
                    base.NotifyPropertyChanged("ClosingIn");
                }
            }
        }

        public int Id
        {
            get
            {
                return this._Id;
            }
            set
            {
                if (this._Id != value)
                {
                    this._Id = value;
                    base.NotifyPropertyChanged("Id");
                }
            }
        }

        public bool IsInCluster
        {
            get
            {
                return this._IsInCluster;
            }
            set
            {
                if (this._IsInCluster != value)
                {
                    this._IsInCluster = value;
                    base.NotifyPropertyChanged("IsInCluster");
                }
            }
        }

        public bool IsOrphaned
        {
            get
            {
                return this._IsOrphaned;
            }
            set
            {
                if (this._IsOrphaned != value)
                {
                    this._IsOrphaned = value;
                    base.NotifyPropertyChanged("IsOrphaned");
                }
            }
        }
        public bool SpokeErrorToggle
        {
            get
            {
                return this._SpokeErrorToggle;
            }
            set
            {
                if (this._SpokeErrorToggle != value)
                {
                    this._SpokeErrorToggle = value;
                    base.NotifyPropertyChanged("SpokeErrorToggle");
                }
            }
        }

        public bool SpokeHighlightToggle
        {
            get
            {
                return this._SpokeHighlightToggle;
            }
            set
            {
                if (this._SpokeHighlightToggle != value)
                {
                    this._SpokeHighlightToggle = value;
                    base.NotifyPropertyChanged("SpokeHighlightToggle");
                }
            }
        }
        public ClusterGroupingStyle GroupingStyle
        {
            get
            {
                return this._GroupingStyle;
            }
            set
            {
                if (this._GroupingStyle != value)
                {
                    this._GroupingStyle = value;
                    base.NotifyPropertyChanged("GroupingStyle");
                }
            }
        }

        public Timer IdleTimer
        {
            get { return _IdleTimer; }
            private set
            {
                if (this._IdleTimer != null)
                {
                    this._IdleTimer.Elapsed -= new ElapsedEventHandler(IdleTimer_Elapsed);
                }
                this._IdleTimer = value;
                if (this._IdleTimer != null)
                {
                    this._IdleTimer.Elapsed += new ElapsedEventHandler(IdleTimer_Elapsed);
                }
            }
        }
        private void IdleTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.ClosingIn = new TimeSpan?(Settings.Default.IdleTimeoutAbortDelay);
            if (this.Bill != null) { Bill.ClosingIn = this.ClosingIn; }
            if (this.Balance != null) { Balance.ClosingIn = this.ClosingIn; }
            if(this.Neqaty!= null) { Neqaty.ClosingIn = this.ClosingIn; }
            if (this.PrePaid != null) { PrePaid.ClosingIn = this.ClosingIn; }
            if (this.PostPaid != null) { PostPaid.ClosingIn = this.ClosingIn; }
            if (this.Services != null) { Services.ClosingIn = this.ClosingIn; }
           
            this.IdleTimer.Stop();
        }





    }
}