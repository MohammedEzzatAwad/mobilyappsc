﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Media.Animation;
using Microsoft.Surface.Presentation.Controls;

namespace MobilyApp.Controls
{
    internal class CloseButton : SurfaceButton
    {
        #region Private fields

        private FrameworkElement _closeFill;
        private Storyboard _currentAnimation;
        private EventHandler completed;

        #endregion

        #region Constructors

        static CloseButton()
        {
        }

        public CloseButton()
        {

        }
        #endregion

        public override void OnApplyTemplate()
        {
            if (!DesignerProperties.GetIsInDesignMode(this))
            {
                this._closeFill = base.GetTemplateChild("PART_CloseFill") as FrameworkElement;
                this.UpdateClosingIn();
            }
            base.OnApplyTemplate();
        }




        internal event EventHandler Completed
        {
            add
            {
                EventHandler handler2;
                EventHandler completed = this.completed;
                do
                {
                    handler2 = completed;
                    EventHandler handler3 = (EventHandler)Delegate.Combine(handler2, value);
                    completed = Interlocked.CompareExchange<EventHandler>(ref this.completed, handler3, handler2);
                }
                while (completed != handler2);
            }
            remove
            {
                EventHandler handler2;
                EventHandler completed = this.completed;
                do
                {
                    handler2 = completed;
                    EventHandler handler3 = (EventHandler)Delegate.Remove(handler2, value);
                    completed = Interlocked.CompareExchange<EventHandler>(ref this.completed, handler3, handler2);
                }
                while (completed != handler2);
            }
        }

        public static readonly DependencyProperty ClosingInProperty =
            DependencyProperty.Register("ClosingIn", typeof (TimeSpan?), typeof (CloseButton),
                                                        new PropertyMetadata(null,delegate (DependencyObject sender, DependencyPropertyChangedEventArgs e)
                                                                                      {
                                                                                          (sender as CloseButton).
                                                                                              UpdateClosingIn();
                                                                                      }));

        public TimeSpan? ClosingIn
        {
            get { return (TimeSpan?) GetValue(ClosingInProperty); }
            set { SetValue(ClosingInProperty, value); }
        }

        private void UpdateClosingIn()
        {
            if (this._closeFill != null)
            {
                DoubleAnimationUsingKeyFrames element = new DoubleAnimationUsingKeyFrames();
                Storyboard.SetTarget(element, this._closeFill);
                Storyboard.SetTargetProperty(element, new PropertyPath(UIElement.OpacityProperty));
                element.KeyFrames.Add(new EasingDoubleKeyFrame(!this.ClosingIn.HasValue ? ((double)0) : ((double)1), TimeSpan.FromSeconds(0.2)));
                ThicknessAnimationUsingKeyFrames frames2 = new ThicknessAnimationUsingKeyFrames();
                Storyboard.SetTarget(frames2, this._closeFill);
                Storyboard.SetTargetProperty(frames2, new PropertyPath(FrameworkElement.MarginProperty));
                frames2.KeyFrames.Add(new EasingThicknessKeyFrame(new Thickness(0.0, 0.0, 0.0, !this.ClosingIn.HasValue ? 0.0 : base.ActualHeight), !this.ClosingIn.HasValue ? TimeSpan.FromSeconds(0.2) : ((KeyTime)this.ClosingIn.Value)));
                if (this._currentAnimation != null)
                {
                    this._currentAnimation.Completed -= new EventHandler(this.CurrentAnimation_Completed);
                }
                this._currentAnimation = new Storyboard();
                this._currentAnimation.Children.Add(element);
                this._currentAnimation.Children.Add(frames2);
                this._currentAnimation.Completed += new EventHandler(this.CurrentAnimation_Completed);
                try
                {
                    this._currentAnimation.Begin(this, (FrameworkTemplate)base.Template, true);
                }
                catch (InvalidOperationException)
                {
                }
            }

        }
        private void CurrentAnimation_Completed(object sender, EventArgs e)
        {
            if (this.ClosingIn.HasValue && (this.completed != null))
            {
                this.completed(this, EventArgs.Empty);
            }
        }


      
    }
}