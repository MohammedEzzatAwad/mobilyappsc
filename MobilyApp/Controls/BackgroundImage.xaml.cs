﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using MobilyApp.Properties;
using Timer = System.Timers.Timer;

namespace MobilyApp.Controls
{
    /// <summary>
    /// Interaction logic for BackgroundImage.xaml
    /// </summary>
    public partial class BackgroundImage : UserControl
    {
        private Image _CurentImage;
        private int _ImageIndex;
        private string[] _Images;
        private readonly Timer _Timer;
        private static int _TransitionMilliseconds = 0x5dc;
        private bool disposed;


        public static readonly DependencyProperty QueryCountProperty = DependencyProperty.Register("QueryCount",
                                                                                                   typeof(int),
                                                                                                   typeof(
                                                                                                       BackgroundImage),
                                                                                                   new PropertyMetadata(
                                                                                                       0,
                                                                                                       delegate(
                                                                                                           DependencyObject
                                                                                                           sender,
                                                                                                           DependencyPropertyChangedEventArgs
                                                                                                           e)
                                                                                                       {
                                                                                                           (sender
                                                                                                            as
                                                                                                            BackgroundImage)
                                                                                                               .
                                                                                                               UpdateQueryCount
                                                                                                               ((int
                                                                                                                )
                                                                                                                e.
                                                                                                                    OldValue);
                                                                                                       }));

        // Properties
        public int QueryCount
        {
            get { return (int)GetValue(QueryCountProperty); }
            set { SetValue(QueryCountProperty, value); }
        }

        private void UpdateQueryCount(int oldValue)
        {
            if ((oldValue == 0) && (this.QueryCount == 1))
            {
                (base.Resources["LogInIsActive"] as Storyboard).Begin(this, true);
            }
            else if ((oldValue == 1) && (this.QueryCount == 0))
            {
                (base.Resources["LogInIsNotActive"] as Storyboard).Begin(this, true);
            }
        }




        public BackgroundImage()
        {
            ElapsedEventHandler handler = null;
            RoutedEventHandler handler2 = null;

            InitializeComponent();

            if (!DesignerProperties.GetIsInDesignMode(this))
            {
                this._Timer = new Timer(Math.Max((double)_TransitionMilliseconds, 30000));
                if (handler == null)
                {
                    handler = (sender, args) =>
                                  {
                                      Dispatcher.BeginInvoke((ThreadStart)(() => { this.ShowImage(); }), DispatcherPriority.Normal, new object[0]);
                                  };
                }
                this._Timer.Elapsed += handler;
                this._Timer.Start();
                this._Images = Directory.GetFiles("Resources/Backgrounds");
                this._ImageIndex = new Random().Next(this._Images.Length);
                this._CurentImage = this._ImageA;
                if (handler2 == null)
                {
                    handler2 = delegate(object sender, RoutedEventArgs e)
                                   {
                                       this.LoadImage();
                                   };
                }
                base.Loaded += handler2;
            }

        }


        private void LoadImage()
        {
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.CacheOption = BitmapCacheOption.OnLoad;
            image.UriSource = new Uri(this._Images[this._ImageIndex], UriKind.Relative);
            image.EndInit();
            this._CurentImage.Source = image;
        }

        private void ShowImage()
        {
            Image lastImage;
            if (this.QueryCount <= 0)
            {
                this._Timer.Stop();
                lastImage = this._CurentImage;
                this._CurentImage = (this._CurentImage == this._ImageA) ? this._ImageB : this._ImageA;
                Panel.SetZIndex(this._CurentImage, Panel.GetZIndex(lastImage) + 1);
                this._CurentImage.Opacity = 0.0;
                this._ImageIndex++;
                if (this._ImageIndex == this._Images.Length)
                {
                    this._ImageIndex = 0;
                }
                this.LoadImage();
                Storyboard storyboard = new Storyboard();
                DoubleAnimation element = new DoubleAnimation();
                Storyboard.SetTarget(element, this._CurentImage);
                Storyboard.SetTargetProperty(element, new PropertyPath(UIElement.OpacityProperty));
                element.To = 1.0;
                element.Duration = TimeSpan.FromMilliseconds((double)_TransitionMilliseconds);
                PowerEase ease = new PowerEase
                                     {
                                         EasingMode = EasingMode.EaseOut
                                     };
                element.EasingFunction = ease;
                element.FillBehavior = FillBehavior.Stop;
                element.Completed += delegate(object sender, EventArgs e)
                                         {
                                             this._CurentImage.Opacity = 1.0;
                                             lastImage.Source = null;
                                             this._Timer.Start();
                                         };
                storyboard.Children.Add(element);
                storyboard.Begin(this, true);
            }
        }


        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }


        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this._Timer.Dispose();
                }
                this.disposed = true;
            }

        }
    }
}