﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using Microsoft.Surface.Presentation.Palettes;

namespace MobilyApp.Converters
{
    internal class AccentColorConverter : IValueConverter
    {
        // Fields
        private static LightSurfacePalette _Palette = new LightSurfacePalette();

        // Methods
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int num = ((int)value) % 4;
            if (num == 0)
            {
                num = 4;
            }
            switch (num)
            {
                case 1:
                    return new SolidColorBrush(Color.FromRgb(129, 53, 138));//A

                case 2:
                    return new SolidColorBrush(Color.FromRgb(6, 122, 190));//B

                case 3:
                    return new SolidColorBrush(Color.FromRgb(87, 187, 187));//C

                case 4:
                    return new SolidColorBrush(Color.FromRgb(32, 168, 225));//D
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }

 

}