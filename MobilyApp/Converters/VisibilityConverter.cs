﻿using System;
using System.Collections;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace MobilyApp.Converters
{
    public class VisibilityConverter : IValueConverter
    {
        // Methods
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool flag = (((((value is string) && !string.IsNullOrEmpty((string)value)) || ((value is int) && (((int)value) > 0))) || (((value is uint) && (((uint)value) > 0)) || ((value is double) && (((double)value) > 0.0)))) || (((value is bool) && ((bool)value)) || ((value is ICollection) && ((value as ICollection).Count > 0)))) || ((((!(value is double) && !(value is int)) && (!(value is uint) && !(value is double))) && (!(value is bool) && !(value is ICollection))) && (value != null));
            if (parameter != null)
            {
                flag = !flag;
            }
            return (flag ? Visibility.Visible : Visibility.Collapsed);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }


}