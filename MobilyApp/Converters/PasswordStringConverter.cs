﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace MobilyApp.Converters
{
    public class PasswordStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                string str1 = value as string;
                if (str1 == null)
                    return (object)null;
                string str2 = "";
                for (int index = 0; index < str1.Length; ++index)
                    str2 = str2 + "*";
                return (object)str2;
            }
            catch (Exception ex)
            {
                Console.WriteLine("PasswordStringConverter : " + ex.Message);
                return (object)null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
