﻿using System;
using System.ComponentModel;
using System.Threading;

namespace MobilyApp.Common
{
    public class ViewModelBase : INotifyPropertyChanged, IDisposable
    {
        private PropertyChangedEventHandler propertyChanged;

        public event PropertyChangedEventHandler PropertyChanged
        {
            add
            {
                PropertyChangedEventHandler changedEventHandler = this.propertyChanged;
                PropertyChangedEventHandler comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler>(ref this.propertyChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler changedEventHandler = this.propertyChanged;
                PropertyChangedEventHandler comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler>(ref this.propertyChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }

        protected void NotifyPropertyChanged(string propertyName)
        {
            if (this.propertyChanged == null)
                return;
            this.propertyChanged((object)this, new PropertyChangedEventArgs(propertyName));
        }

        public virtual void Dispose()
        {
            Console.WriteLine("Warning : Dispose() method isn't implemented for " + this.GetType().FullName);
        }
    }
}