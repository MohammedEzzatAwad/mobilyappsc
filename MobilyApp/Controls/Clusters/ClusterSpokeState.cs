﻿using System.Windows;
using Microsoft.Surface.Presentation.Controls;

namespace MobilyApp.Controls.Clusters
{
    public class ClusterSpokeState
    {
        // Properties
        public double Angle { get; set; }

        public ClusterSpokeAnimationState AnimationState { get; set; }

        public double CurrentDistance { get; set; }

        public double CurrentFlickDistance { get; set; }

        public Point DesiredCenter { get; set; }

        public double Distance { get; set; }

        public double Easing { get; set; }

        public bool EverRemovedFromCluster { get; set; }

        public double FlickAngle { get; set; }

        public ClusterHubState Hub { get; set; }

        public bool IsIntersectingCluster { get; set; }

        public bool IsOrphaned { get; set; }

        public bool IsRemovedFromCluster { get; set; }

        public Point OriginalLocation { get; set; }

        public double RandomOrientation { get; set; }

        public ScatterViewItem ScatterViewItem { get; set; }

        public Point? SpawningTo { get; set; }

    }
}