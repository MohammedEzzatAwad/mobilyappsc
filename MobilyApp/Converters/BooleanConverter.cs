﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace MobilyApp.Converters
{
    public class BooleanConverter : IValueConverter
    {
        // Methods
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (((Visibility)new VisibilityConverter().Convert(value, targetType, parameter, culture)) == Visibility.Visible);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

    }
}