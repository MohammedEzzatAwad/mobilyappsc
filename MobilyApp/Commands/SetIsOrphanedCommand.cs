﻿using MobilyApp.ViewModels;

namespace MobilyApp.Commands
{
    internal static class SetIsOrphanedCommand
    {
        // Methods
        internal static void Execute(PersonVM result, bool isOrphaned)
        {
            result.IsOrphaned = isOrphaned;
        }
    }

 

}