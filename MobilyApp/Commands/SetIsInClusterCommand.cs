﻿using System.Collections.Generic;
using System.Linq;
using MobilyApp.Helpers;
using MobilyApp.Properties;
using MobilyApp.ViewModels;

namespace MobilyApp.Commands
{
    internal static class SetIsInClusterCommand
    {
        // Methods
        internal static bool Execute(ISpokeData hubData, bool isInCluster)
        {
            if (!isInCluster)
            {

                List<ISpokeData> list = (from r in hubData.ParentPersonVM.Results
                                     where ((r is ISpokeData && !((ISpokeData)r).IsInCluster) || (r is ISpokeData && !((ISpokeData)r).IsInCluster))
                                     select r).ToList();
                if (list.Count >= Settings.Default.MaxRemovedResults)
                {
                    Audio.Instance.PlayCue("bes_error_2ch");
                    list.ForEach(delegate(ISpokeData r)
                    {
                        r.SpokeErrorToggle = !r.SpokeErrorToggle;
                    });
                    return false;
                }
            }
            hubData.IsInCluster = isInCluster;
         return true;
    }
}

    }


 