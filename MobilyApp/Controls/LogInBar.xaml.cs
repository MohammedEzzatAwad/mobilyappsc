﻿using System.Windows;
using System.Windows.Controls;

namespace MobilyApp.Controls
{
    /// <summary>
    /// Interaction logic for LogInBar.xaml
    /// </summary>
    public partial class LogInBar : UserControl
    {
        public static readonly DependencyProperty IsLoggingProperty =
            DependencyProperty.Register("IsLogging", typeof (double), typeof (LogInBar), new PropertyMetadata(default(double)));

        public double IsLogging
        {
            get { return (double) GetValue(IsLoggingProperty); }
            set { SetValue(IsLoggingProperty, value); }
        }

        public LogInBar()
        {
            InitializeComponent(); 
       
        
        } 
    }
}
