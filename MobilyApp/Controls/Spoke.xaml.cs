﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using MobilyApp.Controls.Spokes;
using MobilyApp.Core.Utils;
using Microsoft.Surface.Presentation.Controls;
using MobilyApp.Helpers;
using MobilyApp.ViewModels;

namespace MobilyApp.Controls
{
    /// <summary>
    /// Interaction logic for Spoke.xaml
    /// </summary>
    public partial class Spoke : UserControl
    {
        private UserControl _selectedState;
        private double width;
        private double height;

        public override void OnApplyTemplate()
        {
             
            base.OnApplyTemplate();
        }

        public static readonly RoutedEvent CloseRequestedEvent;
        public static readonly RoutedEvent ReturnRequestedEvent;
        public static readonly DependencyProperty SpokeErrorToggleProperty;
        public static readonly DependencyProperty BackSideProperty = DependencyProperty.Register("BackSide", typeof(FrameworkElement), typeof(Spoke), new PropertyMetadata(null));
        public static readonly DependencyProperty IsInClusterProperty = DependencyProperty.Register("IsInCluster", typeof(bool), typeof(Spoke), new PropertyMetadata(true, new PropertyChangedCallback(Spoke.IsInClusterPropertyChanged)));
        public static readonly DependencyProperty IsOrphanedProperty = DependencyProperty.Register("IsOrphaned", typeof(bool), typeof(Spoke), new PropertyMetadata(false));

        #region Constructors
        static Spoke()
        {
            SpokeErrorToggleProperty = DependencyProperty.Register("SpokeErrorToggle", typeof(bool?), typeof(Spoke), new PropertyMetadata(null, delegate(DependencyObject sender, DependencyPropertyChangedEventArgs e)
            {
                (sender as Spoke).UpdateSpokeErrorToggle(e.OldValue as bool?);
            }));
            CloseRequestedEvent = EventManager.RegisterRoutedEvent("CloseRequested", RoutingStrategy.Bubble, typeof(EventHandler<RoutedEventArgs>), typeof(Spoke));
            ReturnRequestedEvent = EventManager.RegisterRoutedEvent("ReturnRequested", RoutingStrategy.Bubble, typeof(EventHandler<RoutedEventArgs>), typeof(Spoke));
        }

        public Spoke()
        {
            InitializeComponent();

            base.SetBinding(IsInClusterProperty, "IsInCluster");
            base.SetBinding(IsOrphanedProperty, "IsOrphaned");
            base.Loaded += new RoutedEventHandler(this.Spoke_Loaded);

        }


        private void Spoke_Loaded(object sender, RoutedEventArgs e)
        {
            base.Loaded -= new RoutedEventHandler(this.Spoke_Loaded);
            if(DataContext != null)
            {
                if (DataContext is BillVM)
                {
                    _selectedState = _billSpoke;
                    
                }
                else if (DataContext is BalanceVM)
                {
                   _selectedState =  _balanceSpoke;
                }
                else if (DataContext is NeqatyVM)
                {
                    _selectedState = _neqatySpoke;
                }
                else if (DataContext is PrePaidVM)
                {
                    _selectedState = _prePaidSpoke;
                }
                else if (DataContext is PostPaidVM)
                {
                    _selectedState = _postPaidSpoke;
                }
                else if (DataContext is ServicesVM)
                {
                    _selectedState = _servicesSpoke;
                }
                GoToState(_selectedState);
            }
            this.UpdateIsInCluster();
        }

        #endregion

        private static void IsInClusterPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            (sender as Spoke).UpdateIsInCluster();
        } 
        private void Close_Click(object sender, RoutedEventArgs e)
        {
           // Audio.Instance.PlayCue("bes_closeButtonPress_1ch");
            if (this.IsOrphaned)
            {
                base.RaiseEvent(new RoutedEventArgs(CloseRequestedEvent));
            }
            else
            {
                base.RaiseEvent(new RoutedEventArgs(ReturnRequestedEvent));
            }
        }
        private void Flip_Click(object sender, RoutedEventArgs e)
        {
            Audio.Instance.PlayCue("bes_cardFlip_1ch");
            ScatterFlip.SetIsFlipped(this.FindVisualParent<ScatterViewItem>(), true);
            //this.BackSide.FindVisualChild<SpokeBack>().TagLink = new Uri(((base.DataContext as ISpokeData).BingResult as ImageResult).MediaUrl, UriKind.Absolute);
        }

 

        private void CloseTimer_Completed(object sender, EventArgs e)
        {
            if (!(base.DataContext as PersonVM).ClosingIn.HasValue)
            {
                base.RaiseEvent(new RoutedEventArgs(CloseRequestedEvent));
            }
        }


        private void UpdateIsInCluster()
        {
            ScatterViewItem svi;
            
            if (IsLoaded)
            {
                svi = this.FindVisualParent<ScatterViewItem>();
                TimeSpan duration = TimeSpan.FromSeconds(0.05);
                Storyboard storyboard = new Storyboard();
                width = svi.MinWidth;
                height = svi.MinHeight;
                if (!this.IsInCluster)
                {

                    if (DataContext != null)
                    {
                        if (DataContext is BillVM)
                        {
                            
                            width = _billSpoke.ActualWidth +30;
                            height = _billSpoke.ActualHeight+30;
                        }
                        else if (DataContext is BalanceVM)
                        {

                            width = _balanceSpoke.ActualWidth + 30;
                            height = _balanceSpoke.ActualHeight + 30; 
                        }
                        else if (DataContext is NeqatyVM)
                        {
                            width = _neqatySpoke.ActualWidth + 30;
                            height = _neqatySpoke.ActualHeight + 30; 
                        }
                        else if (DataContext is PrePaidVM)
                        {
                            width = _prePaidSpoke.ActualWidth + 60; 
                            height = _prePaidSpoke.ActualHeight + 60; 
                        }
                        else if (DataContext is PostPaidVM)
                        {
                            width = _postPaidSpoke.ActualWidth + 60; 
                            height = _postPaidSpoke.ActualHeight + 60; 
                        }
                        else if (DataContext is ServicesVM)
                        {
                            width = _servicesSpoke.ActualWidth + 60;
                            height = _servicesSpoke.ActualHeight + 60; 
                        }
                    }
                   
                    //double num = svi.MinWidth * 3;
                    //double num2 = svi.MinHeight * 3;
                    //width = num;
                    //height = num2;
                }
                else
                {
                    ScatterFlip.SetIsFlipped(svi, false);
                }
                DoubleAnimation element = new DoubleAnimation(width, duration)
                {
                    FillBehavior = FillBehavior.Stop
                };
                SineEase ease = new SineEase
                {
                    EasingMode = EasingMode.EaseOut
                };
                element.EasingFunction = ease;
                Storyboard.SetTarget(element, svi);
                Storyboard.SetTargetProperty(element, new PropertyPath(FrameworkElement.WidthProperty));
                storyboard.Children.Add(element);
                DoubleAnimation animation2 = new DoubleAnimation(height, duration)
                {
                    FillBehavior = FillBehavior.Stop
                };
                SineEase ease2 = new SineEase
                {
                    EasingMode = EasingMode.EaseOut
                };
                animation2.EasingFunction = ease2;
                Storyboard.SetTarget(animation2, svi);
                Storyboard.SetTargetProperty(animation2, new PropertyPath(FrameworkElement.HeightProperty));
                storyboard.Children.Add(animation2);
                storyboard.Completed += delegate(object sender, EventArgs e)
                {
                    svi.Width = width;
                    svi.Height = height;
                };
                storyboard.Begin(svi);
                VisualStateManager.GoToState(this, this.IsInCluster ? this.IsInClusterState.Name : this.NotInClusterState.Name, true);
                if (!this.IsInCluster)
                {
                  Audio.Instance.PlayCue("bes_pullIingFromStack_1ch");
                }
                GoToState(_selectedState);
            }
        }


        private void UpdateSpokeErrorToggle(bool? oldValue)
        {
            if ((base.IsLoaded && oldValue.HasValue) && this.SpokeErrorToggle.HasValue)
            {
               // (this._CloseBtn.Template.Resources["Highlight"] as Storyboard).Clone().Begin(this._CloseBtn, this._CloseBtn.Template);
            }
        }

        // Properties
        public FrameworkElement BackSide
        {
            get
            {
                return (FrameworkElement)base.GetValue(BackSideProperty);
            }
            set
            {
                base.SetValue(BackSideProperty, value);
            }
        }

        public bool IsInCluster
        {
            get
            {
                return (bool)base.GetValue(IsInClusterProperty);
            }
            set
            {
                base.SetValue(IsInClusterProperty, value);
            }
        }

        public bool IsOrphaned
        {
            get
            {
                return (bool)base.GetValue(IsOrphanedProperty);
            }
            set
            {
                base.SetValue(IsOrphanedProperty, value);
            }
        }

        public bool? SpokeErrorToggle
        {
            get
            {
                return (bool?)base.GetValue(SpokeErrorToggleProperty);
            }
            set
            {
                base.SetValue(SpokeErrorToggleProperty, value);
            }
        }
        private void GoToState(UserControl state)
        {
            _billSpoke.Visibility = state == _billSpoke ? Visibility.Visible : Visibility.Collapsed;
            _balanceSpoke.Visibility = state == _balanceSpoke ? Visibility.Visible : Visibility.Collapsed;
            _neqatySpoke.Visibility = state == _neqatySpoke ? Visibility.Visible : Visibility.Collapsed;
            _prePaidSpoke.Visibility = state == _prePaidSpoke ? Visibility.Visible : Visibility.Collapsed;
            _postPaidSpoke.Visibility = state == _postPaidSpoke ? Visibility.Visible : Visibility.Collapsed;
            _servicesSpoke.Visibility = state == _servicesSpoke ? Visibility.Visible : Visibility.Collapsed;
        }
    }
}
