﻿namespace MobilyApp.ViewModels.Requests
{
    public static class Constants
    {
        public static string request_url = "https://www.mobily.com.sa/IPhoneService/mobilyIphoneService.do";
        public static string login_MOBILY_IPHONE_REQUEST = "MOBILY_IPHONE_REQUEST";
        public static string login_FunctionId = "FunctionId";
        public static string login_device = "device";
        public static string login_UserName = "UserName";
        public static string login_Password = "Password";
        public static string login_RequestorLanguage = "RequestorLanguage";
        public static string login_device_number = "5";
        public static string login_LOGIN = "LOGIN";
        public static string login_LOGOUT = "LOGOUT";
        public static string login_EA = "E/A";
        public static string login_MOBILY_IPHONE_REPLY = "MOBILY_IPHONE_REPLY";
        public static string login_HashCode = "HashCode";
        public static string login_root = "<root>";
        public static string login_rootEnd = "</root>";
        public static string login_ERROR = "ERROR";
        public static string login_ErrorCode = "ErrorCode";
        public static string balancebill_GET_BALANCE = "GET_BALANCE";
        public static string balancebill_GET_BILL_INFO = "GET_BILL_INFO";
        public static string balancebill_GET_BILL_SUMMARY = "GET_BILL_SUMMARY";
        public static string balancebill_GET_BILL_PERSONAL_INFO = "GET_BILL_PERSONAL_INFO";
        public static string balance_E = "E";
        public static string balancebill_LINENUMBER = "LINE_NUMBER";
        public static string balancebill_UnbilledAmount = "UnbilledAmount";
        public static string balancebill_DueAmount = "DueAmount";
        public static string balancebill_FreeMinutes = "FreeMinutes";
        public static string balancebill_FreeOnNetMinutes = "FreeOnNetMinutes";
        public static string balancebill_FreeSMS = "FreeSMS";
        public static string balancebill_FreeOnNetSMS = "FreeOnNetSMS";
        public static string balancebill_FreeMMS = "FreeMMS";
        public static string balancebill_FreeOnNetMMS = "FreeOnNetMMS";
        public static string balancebill_FreeGPRS = "FreeGPRS";
        public static string balancebill_NationalFavoriteNumber = "NationalFavoriteNumber";
        public static string balancebill_InternationalFavoriteNumber = "InternationalFavoriteNumber";
        public static string balancebill_EXPIRATION_DATE = "EXPIRATION_DATE";
        public static string balancebill_BALANCE = "BALANCE";
        public static string balancebill_BILL_PERSONAL_INFO = "BILL_PERSONAL_INFO";
        public static string balancebill_Name = "Name";
        public static string balancebill_BillNumber = "BillingNumber";
        public static string balancebill_POBox = "P.O.Box";
        public static string balancebill_Address = "Address";
        public static string balancebill_City = "City";
        public static string balancebill_Country = "Country";
        public static string balancebill_StartDate = "StartDate";
        public static string balancebill_EndDate = "EndDate";
        public static string balancebill_Tarrif_Plan = "Tarrif_Plan";
        public static string balancebill_Due_Date = "Due_Date";
        public static string balancebill_Previous_Amount = "Previous_Amount";
        public static string balancebill_MonthlyFee = "MonthlyFee";
        public static string balancebill_AdditionalFee = "AdditionalFee";
        public static string balancebill_UsageAmount = "UsageAmount";
        public static string balancebill_Discount = "Discount";
        public static string balancebill_PaidAmount = "PaidAmount";
        public static string balancebill_AmountDue = "AmountDue";
        public static string balancebill_BILL_SUMMARY = "BILL_SUMMARY";
        public static string balancebill_BILL_INFO = "BILL_INFO";

        public static string neqaty_LOYALTY_VIEW_SUMMARY = "LOYALTY_VIEW_SUMMARY";
        public static string neqaty_LOYALTY_INQUIRY_ITEMS = "LOYALTY_INQUIRY_ITEMS";
        public static string neqaty_CurrentBalance = "CurrentBalance";
        public static string neqaty_EarnedPoints = "EarnedPoints";
        public static string neqaty_RedeemedPoints = "RedeemedPoints";
        public static string neqaty_ExpirySchedulerPoints = "ExpirySchedulerPoints";
        public static string neqaty_LostPoints = "LostPoints";

        public static string error_0 = "The operation was completed successfully.";
        public static string error_10000 = "Sorry, You are not authorized to view this page.";
        public static string error_10001 = "Sorry, an unknown error has occurred. Please try again later.";
        public static string error_10002 = "Sorry, you cannot login to this iPhone application with the provided username and password. This application is intended for consumer accounts and not corporate accounts.";
        public static string error_10005 = "Sorry, you cannot login to this iPhone application with the provided username and password. This application is intended for GSM mobile accounts and not Broadband@Home accounts.";
        public static string error_10003 = "Sorry, either the username or the password you provided is not valid.";
        public static string error_10004 = "Sorry, you are not login in yet. Please login first.";
        public static string error_5001 = "Sorry, but prepaid customers do not have bills.";
        public static string error_5002 = "Currently, there is no bill under your account in the system. Please check back after the next billing cycle to view your bills.";
        public static string progress_null = string.Empty;
        public static string progress_Starting = "Starting ...";
        public static string progress_Login = "Login : done ...";
        public static string progress_Balance = "Balance : done ...";
        public static string progress_BillInfo = "Bill informations : done ...";
        public static string progress_BillSummary = "Bill summary : done ...";
        public static string progress_BillPersonalInfo = "Bill personal information : done ...";
        public static string progress_Neqaty = "Neqaty : done ...";
        public static string progress_PrePaid = "Pre-Paid : done ...";
        public static string progress_PostPaid = "Post-Paid : done ...";
        public static string progress_Services = "Services : done ...";


        static Constants()
        {
        }
    }
}