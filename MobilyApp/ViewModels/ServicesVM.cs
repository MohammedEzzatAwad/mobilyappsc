﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Serialization;
using MobilyApp.Model;

namespace MobilyApp.ViewModels
{
    public class ServicesVM : AbstractServicesVM, ISpokeData
    {
        // Fields
        private TimeSpan? _ClosingIn;
        private int _Id;
        private bool _IsInCluster = true;

        private bool _SpokeErrorToggle;
        private bool _SpokeHighlightToggle;
        private string _logoImage;
        private PersonVM _personVm;

        public ServicesVM()
        {
            this.CollItem = new ObservableCollection<Item>();
        }
        public PersonVM ParentPersonVM
        {
            get { return this._personVm; }
            set
            {
                if (_personVm != value)
                {
                    this._personVm = value;
                    base.NotifyPropertyChanged("ParentPersonVM");
                }
            }
        }

        public string LogoImage
        {
            get { return this._logoImage; }
            set
            {
                if (_logoImage != value)
                {
                    this._logoImage = value;
                    NotifyPropertyChanged("LogoImage");
                }
            }
        }

        public TimeSpan? ClosingIn
        {
            get
            {
                return this._ClosingIn;
            }
            set
            {
                if (this._ClosingIn != value)
                {
                    this._ClosingIn = value;
                    base.NotifyPropertyChanged("ClosingIn");
                }
            }
        }

        public int Id
        {
            get
            {
                return this._Id;
            }
            set
            {
                if (this._Id != value)
                {
                    this._Id = value;
                    base.NotifyPropertyChanged("Id");
                }
            }
        }

        public bool IsInCluster
        {
            get
            {
                return this._IsInCluster;
            }
            set
            {
                if (this._IsInCluster != value)
                {
                    this._IsInCluster = value;
                    base.NotifyPropertyChanged("IsInCluster");
                }
            }
        }
        public bool SpokeErrorToggle
        {
            get
            {
                return this._SpokeErrorToggle;
            }
            set
            {
                if (this._SpokeErrorToggle != value)
                {
                    this._SpokeErrorToggle = value;
                    base.NotifyPropertyChanged("SpokeErrorToggle");
                }
            }
        }

        public bool SpokeHighlightToggle
        {
            get
            {
                return this._SpokeHighlightToggle;
            }
            set
            {
                if (this._SpokeHighlightToggle != value)
                {
                    this._SpokeHighlightToggle = value;
                    base.NotifyPropertyChanged("SpokeHighlightToggle");
                }
            }
        }

        public void PostServices(string hashcode)
        {
            XmlSerializer deserializer = new XmlSerializer(typeof (List<Item>));

            TextReader textReader = new StreamReader("Model\\Items.xml");

            var AllItems = (List<Item>) deserializer.Deserialize(textReader);

            foreach (Item item in AllItems)
            {
                if (item.Category == "Services")
                {
                    CollItem.Add(item);
                }
            }

        }
    }
}