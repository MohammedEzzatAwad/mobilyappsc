﻿using System;

namespace MobilyApp.Common
{
    public class DelegateCommand : CommandBase
    {
        private readonly Predicate<object> _canExecute;
        private readonly Action<object> _execute;

        public DelegateCommand()
        {
        }

        public DelegateCommand(Action<object> execute)
            : this(execute, (Predicate<object>)null)
        {
        }

        public DelegateCommand(Action<object> execute, Predicate<object> canExecute)
        {
            this._execute = execute;
            this._canExecute = canExecute;
        }

        public override bool CanExecute(object parameter)
        {
            if (this._canExecute == null)
                return true;
            else
                return this._canExecute(parameter);
        }

        public override void Execute(object parameter)
        {
            this._execute(parameter);
        }
    }
}