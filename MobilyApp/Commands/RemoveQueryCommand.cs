﻿using MobilyApp.ViewModels;

namespace MobilyApp.Commands
{
    internal class RemoveQueryCommand
    {
        // Methods
        internal static void Execute(PersonVM query)
        {
            ApplicationVM.Instance.CollPerson.Remove(query);
        }

    }
}