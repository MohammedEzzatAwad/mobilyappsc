﻿using System;
using System.Diagnostics;
using System.IO;

namespace MobilyApp.Helpers
{
   public class Audio : IDisposable
{
    // Fields
    private static Audio audio;
    //private AudioEngine b;
    //private WaveBank c;
    public const string CueButtonPress = "bes_buttonPress_1ch";
    public const string CueCardFlip = "bes_cardFlip_1ch";
    public const string CueClose = "bes_closeButtonPress_1ch";
    public const string CueError = "bes_error_2ch";
    public const string CueFullScreenMap = "bes_mapToFullScreen_2ch";
    public const string CuePushpin = "bes_pushpinPress_1ch";
    public const string CueRemoveFromCluster = "bes_pullIingFromStack_1ch";
    public const string CueRestoreMap = "bes_mapToWindow_2ch";
    public const string CueSearchSpawned = "bes_barSpawn_2ch";
    //private SoundBank d;

    // Methods
    private Audio()
    {
        this.a();
    }

    private void a()
    {
        string str2 = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + @"\Resources\Audio\";
        try
        {
            //this.b = new AudioEngine(str2 + "Audio.xgs");
            //this.c = new WaveBank(this.b, str2 + "Audio.xwb");
            //this.d = new SoundBank(this.b, str2 + "Audio.xsb");
        }
        catch
        {
            //this.b = null;
            //this.c = null;
            //this.d = null;
            throw;
        }
    }

    public void Dispose()
    {
        this.Dispose(true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (disposing)
        {
            //this.b.Dispose();
            //this.c.Dispose();
            //this.d.Dispose();
        }
    }

    public static void Initialize()
    {
       // a = new Audio();
    }

    public void PlayCue(string soundCue)
    { 

    }

    // Properties
    public static Audio Instance
    {
        get
        {
            if (audio == null)
            {
                audio = new Audio();
            }
            return audio;
        }
    }
}

  

}