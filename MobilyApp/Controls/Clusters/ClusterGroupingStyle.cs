﻿namespace MobilyApp.Controls.Clusters
{
    public enum ClusterGroupingStyle
    { 
        Circular,
        Stacked
    }
}