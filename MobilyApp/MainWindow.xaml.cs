using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation.Input;
using MobilyApp.Helpers;
using MobilyApp.ViewModels;

namespace MobilyApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : SurfaceWindow, IComponentConnector
    {
        private bool _isLogInBarBeingManipulated;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            // Add handlers for window availability events
            AddWindowAvailabilityHandlers();

            SurfaceKeyboard.Shown += new EventHandler(this.SurfaceKeyboardShown);

            Activated += (sender, args) =>
                              {
                                  if (!SurfaceEnvironment.IsSurfaceEnvironmentAvailable)
                                  {
                                      this.Startup();
                                  }
                              };


            SizeChanged += MainWindowSizeChanged;
        }

        private void MainWindowSizeChanged(object sender, SizeChangedEventArgs e)
        {
            if ((e.NewSize.Width != 0.0) && (e.NewSize.Height != 0.0))
            {
                base.SizeChanged -= new SizeChangedEventHandler(this.MainWindowSizeChanged);
                this._LogInBar.Center = new Point(base.ActualWidth / 2.0, base.ActualHeight / 2.0);
            }
        }



        private void SurfaceKeyboardShown(object sender, EventArgs e)
        {
            Matrix matrix = new Matrix();
            matrix.Translate(_LogInBar.ActualCenter.X, _LogInBar.ActualCenter.Y);
            matrix.Translate(0.0, ((_LogInBar.ActualHeight / 2.0) + (SurfaceKeyboard.Height / 2f)) + 20.0);
            matrix.RotateAt(_LogInBar.ActualOrientation, _LogInBar.ActualCenter.X, this._LogInBar.ActualCenter.Y);
            SurfaceKeyboard.CenterX = (float)Math.Max(0.0, Math.Min(ActualWidth, matrix.OffsetX));
            SurfaceKeyboard.CenterY = (float)Math.Max(0.0, Math.Min(ActualHeight, matrix.OffsetY));
            SurfaceKeyboard.Rotation = (float)(this._LogInBar.ActualOrientation * 0.017453292519943295);
        }


        /// <summary>
        /// Occurs when the window is about to close. 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            // Remove handlers for window availability events
            RemoveWindowAvailabilityHandlers();
        }

        /// <summary>
        /// Adds handlers for window availability events.
        /// </summary>
        private void AddWindowAvailabilityHandlers()
        {
            // Subscribe to surface window availability events
            ApplicationServices.WindowInteractive += OnWindowInteractive;
            ApplicationServices.WindowNoninteractive += OnWindowNoninteractive;
            ApplicationServices.WindowUnavailable += OnWindowUnavailable;
        }

        /// <summary>
        /// Removes handlers for window availability events.
        /// </summary>
        private void RemoveWindowAvailabilityHandlers()
        {
            // Unsubscribe from surface window availability events
            ApplicationServices.WindowInteractive -= OnWindowInteractive;
            ApplicationServices.WindowNoninteractive -= OnWindowNoninteractive;
            ApplicationServices.WindowUnavailable -= OnWindowUnavailable;
        }

        /// <summary>
        /// This is called when the user can interact with the application's window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnWindowInteractive(object sender, EventArgs e)
        {
            this.Startup();
            if (SurfaceKeyboard.IsVisible)
            {
                SurfaceTextBox box = this._LogInBar.FindVisualChild<SurfaceTextBox>();
                if (box != null &&
                    (box.Name.ToUpper(CultureInfo.InvariantCulture).Contains("LOGIN") ||
                     box.Name.ToUpper(CultureInfo.InvariantCulture).Contains("PASSWORD")))
                {
                    box.Focus();
                }
            }

        }

        /// <summary>
        /// This is called when the user can see but not interact with the application's window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnWindowNoninteractive(object sender, EventArgs e)
        {
            //TODO: Disable audio here if it is enabled

            //TODO: optionally enable animations here
        }

        /// <summary>
        /// This is called when the application's window is not visible or interactive.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnWindowUnavailable(object sender, EventArgs e)
        {
            //TODO: disable audio, animations here
        }


        private void Startup()
        {
            if (base.DataContext == null)
            {
                base.DataContext = ApplicationVM.Instance;
                //Audio.Initialize();
                _LogInBar.ContainerManipulationStarted += (sender, args) =>
                                                              {
                                                                  this._isLogInBarBeingManipulated = true;
                                                              };
                _LogInBar.ContainerManipulationCompleted += (sender, args) =>
                                                                {
                                                                    this._isLogInBarBeingManipulated = false;
                                                                };
                CompositionTarget.Rendering += (sender, args) =>
                                                   {
                                                       this.UpdateLogInBarPosition();
                                                       this.UpdateKeyboardPosition();
                                                   };

            }
        }



        private void UpdateLogInBarPosition()
        {
            bool flag = false;
            TouchDevice device = (this._LogInBar.TouchesCaptured.Count<TouchDevice>() != 1)
                                     ? null
                                     : this._LogInBar.TouchesCaptured.FirstOrDefault<TouchDevice>();
            if ((device != null) && !TouchExtensions.GetIsFingerRecognized(device))
            {
                device = null;
            }
            if (SurfaceKeyboard.IsVisible && !this._isLogInBarBeingManipulated)
            {
                flag = true;
            }
            if (flag)
            {
                var matrix = new Matrix();
                matrix.Translate((double)SurfaceKeyboard.CenterX, (double)SurfaceKeyboard.CenterY);
                matrix.Translate(0.0, ((-this._LogInBar.ActualHeight / 2.0) + (-SurfaceKeyboard.Height / 2f)) + -20.0);
                matrix.RotateAt(SurfaceKeyboard.Rotation * 57.295779513082323, (double)SurfaceKeyboard.CenterX,
                                (double)SurfaceKeyboard.CenterY);
                double x = this._LogInBar.ActualCenter.X;
                double num2 = matrix.OffsetX - x;
                bool flag2 = Math.Abs(num2) > 1.0;
                if (flag2)
                {
                    x += num2 * 0.2;
                    x = Math.Min(this._LogInBarContainer.ActualWidth, Math.Max(0.0, x));
                }
                double y = this._LogInBar.ActualCenter.Y;
                double num4 = matrix.OffsetY - y;
                bool flag3 = Math.Abs(num4) > 1.0;
                if (flag3)
                {
                    y += num4 * 0.2;
                    y = Math.Min(this._LogInBarContainer.ActualHeight, Math.Max(0.0, y));
                }
                if (flag2 || flag3)
                {
                    this._LogInBar.Center = new Point(x, y);
                }
            }
        }

        private void UpdateKeyboardPosition()
        {
            if (SurfaceKeyboard.IsVisible && this._isLogInBarBeingManipulated)
            {
                double num = this._LogInBar.ActualOrientation - (SurfaceKeyboard.Rotation * 57.295779513082323);
                num = num % 360.0;
                if (num > 180.0)
                {
                    num -= 360.0;
                }
                else if (num < -180.0)
                {
                    num += 360.0;
                }
                if (Math.Abs(num) > 1.0)
                {
                    SurfaceKeyboard.Rotation += (float)((num * 0.2) * 0.017453292519943295);
                }
                Matrix matrix = new Matrix();
                matrix.Translate(this._LogInBar.ActualCenter.X, this._LogInBar.ActualCenter.Y);
                matrix.Translate(0.0, ((this._LogInBar.ActualHeight / 2.0) + (SurfaceKeyboard.Height / 2f)) + 20.0);
                matrix.RotateAt(this._LogInBar.ActualOrientation, this._LogInBar.ActualCenter.X,
                                this._LogInBar.ActualCenter.Y);
                double centerX = SurfaceKeyboard.CenterX;
                double num3 = matrix.OffsetX - centerX;
                bool flag = Math.Abs(num3) > 1.0;
                if (flag)
                {
                    centerX += num3 * 0.2;
                }
                double centerY = SurfaceKeyboard.CenterY;
                double num5 = matrix.OffsetY - centerY;
                bool flag2 = Math.Abs(num5) > 1.0;
                if (flag2)
                {
                    centerY += num5 * 0.2;
                }
                if (flag || flag2)
                {
                    SurfaceKeyboard.CenterX = (float)Math.Max(0.0, Math.Min(base.ActualWidth, centerX));
                    SurfaceKeyboard.CenterY = (float)Math.Max(0.0, Math.Min(base.ActualHeight, centerY));
                }
            }
        }

        protected void TagVisualizer_PreviewVisualizationAdded(object sender, TagVisualizationEventArgs e)
        {
            //var evo = (from route in ApplicationVM.Instance.SearchRoutes
            //                     where route.Series == e.Visualization.VisualizedTag.Series
            //                     where route.Value == e.Visualization.VisualizedTag.Value
            //                     select route).FirstOrDefault<SearchRouteVO>();
            //e.Visualization.DataContext = evo;

        }
    }
}