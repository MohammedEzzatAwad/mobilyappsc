﻿using System;
using System.Xml.Linq;
using MobilyApp.Helpers;

namespace MobilyApp.ViewModels.Requests
{
    public static class LoginRequest
    {
        public static string LoginToEPortal(string msg)
        {
            return new PostSubmitter()
            {
                Url = Constants.request_url,
                PostItems = {
          {
            (string) null,
            msg
          }
        },
                Type = PostSubmitter.PostTypeEnum.Post
            }.Post();
        }

        public static string CreateXmlForLogin(string login, string pwd)
        {
            XElement xelement = new XElement((XName)Constants.login_MOBILY_IPHONE_REQUEST);
            xelement.Add(new XElement((XName)Constants.login_FunctionId, (object)Constants.login_LOGIN));
            xelement.Add(new XElement((XName)Constants.login_device, (object)Constants.login_device_number));
            xelement.Add(new XElement((XName)Constants.login_UserName,  login));
            xelement.Add(new XElement((XName)Constants.login_Password,  pwd));
            xelement.Add(new XElement((XName)Constants.login_RequestorLanguage, (object)Constants.login_EA));
            Console.WriteLine(((object)xelement).ToString());
            return ((object)xelement).ToString();
        }

        internal static string CreateXmlForLogout(string hashcode)
        {
            XElement xelement = new XElement(Constants.login_MOBILY_IPHONE_REQUEST);
            xelement.Add((object)new XElement(Constants.login_FunctionId, Constants.login_LOGOUT));
            xelement.Add((object)new XElement(Constants.login_device, Constants.login_device_number));
            xelement.Add((object)new XElement(Constants.login_HashCode, hashcode));
            xelement.Add((object)new XElement(Constants.login_RequestorLanguage, Constants.login_EA));
            return ((object)xelement).ToString();
        }

        public static PersonVM GetPersonVM(string msg)
        {
            string str = string.Empty;
            msg = msg.Replace("---!>", "-->");
            foreach (XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants())
            {
                if (xelement1.Name == (XName)Constants.login_MOBILY_IPHONE_REPLY)
                {
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (xelement2.Name == (XName)Constants.login_HashCode)
                            return new PersonVM(xelement2.Value);
                    }
                }
            }
            return (PersonVM)null;
        }
    }
}