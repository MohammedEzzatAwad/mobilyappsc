﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using MobilyApp.Common;
using MobilyApp.Properties;
using MobilyApp.ViewModels.Requests;

namespace MobilyApp.ViewModels
{
    public class ApplicationVM : AbstractApplicationVM
    {
        private static ApplicationVM _instance;

        public static ApplicationVM Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new ApplicationVM();
                }
                return _instance;
            }
        }

        public ApplicationVM()
        {
            this.CollPerson = new ObservableCollection<PersonVM>();
            PersonVM personVm = new PersonVM("testPerson");
            BalanceVM balanceVm = new BalanceVM();
            //personVm.Balance = balanceVm;
            //balanceVm.Balance = "testBalance";
            //balanceVm.DueAmount = "testDueAmount";
            //balanceVm.ExpirationDate = "testExpirationDate";
            //balanceVm.FreeGPRS = 1000.0;
            //balanceVm.FreeMinutes = 1000.0;
            //balanceVm.FreeMMS = 1000.0;
            //balanceVm.FreeOnNetMinutes = 1000.0;
            //balanceVm.FreeOnNetMMS = 1000.0;
            //balanceVm.FreeOnNetSMS = 1000.0;
            //balanceVm.FreeSMS = 1000.0;
            //balanceVm.InternationalFavoriteNumber = "testInternationalFavoriteNumber";
            //balanceVm.LineNumber = "testLineNumber";
            //balanceVm.NationalFavoriteNumber = "testNationalFavoriteNumber";
            //balanceVm.UnbilledAmount = "testUnbilledAmount";
            BillVM billVm = new BillVM();
            //personVm.Bill = billVm;
            //billVm.AdditionalFee = 1000.0;
            //billVm.Address = "testAddress";
            //billVm.AmountDue = 1000.0;
            //billVm.BillNumber = "testBillNumber";
            //billVm.City = "testCity";
            //billVm.Country = "testCountry";
            //billVm.Discount = 1000.0;
            //billVm.DueDate = "testDueDate";
            //billVm.EndDate = "testEndDate";
            //billVm.LineNumber = "testLineNumber";
            //billVm.MonthlyFee = 1000.0;
            //billVm.Name = "testName";
            //billVm.PaidAmount = 1000.0;
            //billVm.POBox = "testPOBox";
            //billVm.PreviousAmount = 1000.0;
            //billVm.StartDate = "testStartDate";
            //billVm.TarrifPlan = "testTarrifPlan";
            //billVm.UsageAmount = 1000.0;


             

        }

        protected override void ExecuteValidate(object objArguments)
        {
            base.ExecuteValidate(objArguments);
            this.ProgressText = Constants.progress_Starting;
            this.ErrorText = string.Empty;
            BackgroundWorker backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += OnSaveThreadDoWork;
            backgroundWorker.RunWorkerCompleted += OnSaveThreadCompleted;
            backgroundWorker.RunWorkerAsync();
        }

        private PersonVM PostApplication()
        {
            string xmlForLogin = LoginRequest.CreateXmlForLogin(this.Login, this.Password);
            string msg = LoginRequest.LoginToEPortal(xmlForLogin);
            string error1 = ConstantsRequest.GetError(msg);
            if (error1 == null || error1 == string.Empty)
            {
                ApplicationVM applicationVm = this;
                string str = Constants.progress_Login;  //applicationVm.ProgressText + "\n" + Constants.progress_Login;
                applicationVm.ProgressText = str;
                PersonVM personVm = LoginRequest.GetPersonVM(msg);
                personVm.ErrorFound += personVM_ErrorFound;
                personVm.Progress += personVM_Progress;
                personVm.PostPerson();
                if (personVm != null)
                {
                    LoginRequest.CreateXmlForLogout(personVm.HashCode);
                    LoginRequest.LoginToEPortal(xmlForLogin);
                    personVm.Id = CollPerson.Count;
                    string error2 = ConstantsRequest.GetError(msg);
                    if (error2 == null || error2 == string.Empty)
                        return personVm;
                    this.ErrorText = "Logout : " + error1;
                   
                }
            }
            else
                this.ErrorText = "Login : " + error1;
            return (PersonVM)null;
        }

        private void personVM_Progress(object sender, GenericEventArgs<string> e)
        {
            ApplicationVM applicationVm = this;
            string str = e.Param; //applicationVm.ProgressText + "\n" + e.Param;
            applicationVm.ProgressText = str;
        }

        private void personVM_ErrorFound(object sender, GenericEventArgs<string> e)
        {
            this.ErrorText = e.Param;
        }

        private void OnSaveThreadDoWork(object sender, DoWorkEventArgs e)
        {
            e.Result = (object)this.PostApplication();
        }

        private void OnSaveThreadCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
                this.ProgressText = "Cancelled";
            else if (e.Error != null)
            {
                this.ProgressText = "Exception Thrown";
            }
            else
            {
                if (e.Result != null && e.Result is PersonVM)
                    this.CollPerson.Add(e.Result as PersonVM);
                this.ProgressText = Constants.progress_null;
            }
            this.Login = string.Empty;
            this.Password = string.Empty;
        }
     

    }
}