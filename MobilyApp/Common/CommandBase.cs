﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace MobilyApp.Common
{
    public abstract class CommandBase : ICommand
    {
        private readonly Dispatcher _dispatcher;

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        protected CommandBase()
        {
            this._dispatcher = Application.Current == null ? Dispatcher.CurrentDispatcher : Application.Current.Dispatcher;
            Debug.Assert(this._dispatcher != null);
        }

        public abstract bool CanExecute(object parameter);

        public abstract void Execute(object parameter);

        protected virtual void OnCanExecuteChanged()
        {
            if (!this._dispatcher.CheckAccess())
                this._dispatcher.Invoke((Delegate)new ThreadStart(this.OnCanExecuteChanged), DispatcherPriority.Normal, new object[0]);
            else
                CommandManager.InvalidateRequerySuggested();
        }
    }
}