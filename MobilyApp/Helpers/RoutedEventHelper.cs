﻿using System;
using System.Windows;

namespace MobilyApp.Helpers
{
    public static class RoutedEventHelper
    {
        public static void RaiseEvent(DependencyObject target, RoutedEventArgs args)
        {
            if (target is UIElement)
            {
                (target as UIElement).RaiseEvent(args);
            }
            else
            {
                if (!(target is ContentElement))
                    return;
                (target as ContentElement).RaiseEvent(args);
            }
        }

        public static void AddHandler(DependencyObject element, RoutedEvent routedEvent, Delegate handler)
        {
            UIElement uiElement = element as UIElement;
            if (uiElement != null)
            {
                uiElement.AddHandler(routedEvent, handler);
            }
            else
            {
                ContentElement contentElement = element as ContentElement;
                if (contentElement == null)
                    return;
                contentElement.AddHandler(routedEvent, handler);
            }
        }

        public static void RemoveHandler(DependencyObject element, RoutedEvent routedEvent, Delegate handler)
        {
            UIElement uiElement = element as UIElement;
            if (uiElement != null)
            {
                uiElement.RemoveHandler(routedEvent, handler);
            }
            else
            {
                ContentElement contentElement = element as ContentElement;
                if (contentElement == null)
                    return;
                contentElement.RemoveHandler(routedEvent, handler);
            }
        }
    }
}