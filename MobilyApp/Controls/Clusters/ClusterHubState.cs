﻿using System.Collections.Generic;
using System.Windows;
using Microsoft.Surface.Presentation.Controls;

namespace MobilyApp.Controls.Clusters
{
    public class ClusterHubState 
    {
        public ClusterGroupingStyle GroupingStyle { get; set; }
        public bool IsRemoving { get; set; }
        public bool IsUpdating { get; set; }
        public ScatterViewItem ScatterViewItem { get; set; }
        public Point? SpawningTo { get; set; }

        public IList<ClusterSpokeState> Spokes { get; set; }

        public double StackDistance { get; set; }

    }
}