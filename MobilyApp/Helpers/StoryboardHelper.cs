﻿using System.Windows.Markup;
using System.Windows.Media.Animation;

namespace MobilyApp.Helpers
{
    public static class StoryboardHelper
    {
        public static Storyboard CloneStoryboard(Storyboard stbModel)
        {
            if (stbModel != null)
                return (Storyboard)XamlReader.Parse(XamlWriter.Save((object)stbModel));
            else
                return (Storyboard)null;
        }
    }
}