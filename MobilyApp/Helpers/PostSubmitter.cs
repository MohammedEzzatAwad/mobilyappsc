﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

namespace MobilyApp.Helpers
{
    public class PostSubmitter
    {
        private string m_url = string.Empty;
        private NameValueCollection m_values = new NameValueCollection();
        private PostSubmitter.PostTypeEnum m_type = PostSubmitter.PostTypeEnum.Get;

        public string Url
        {
            get
            {
                return this.m_url;
            }
            set
            {
                this.m_url = value;
            }
        }

        public NameValueCollection PostItems
        {
            get
            {
                return this.m_values;
            }
            set
            {
                this.m_values = value;
            }
        }

        public PostSubmitter.PostTypeEnum Type
        {
            get
            {
                return this.m_type;
            }
            set
            {
                this.m_type = value;
            }
        }

        public PostSubmitter()
        {
        }

        public PostSubmitter(string url)
            : this()
        {
            this.m_url = url;
        }

        public PostSubmitter(string url, NameValueCollection values)
            : this(url)
        {
            this.m_values = values;
        }

        public string Post()
        {
            StringBuilder baseRequest = new StringBuilder();
            for (int index = 0; index < this.m_values.Count; ++index)
                this.EncodeAndAddItem(ref baseRequest, this.m_values.GetKey(index), this.m_values[index]);
            bool flag = false;
            string str = string.Empty;
            while (!flag)
            {
                str = this.PostData(this.m_url, ((object)baseRequest).ToString());
                if (str.Substring(0, 3).Equals("###"))
                {
                    flag = false;
                    Thread.Sleep(5000);
                }
                else
                    flag = true;
            }
            return str;
        }

        public string Post(string url)
        {
            this.m_url = url;
            return this.Post();
        }

        public string Post(string url, NameValueCollection values)
        {
            this.m_values = values;
            return this.Post(url);
        }

        private string PostData(string url, string postData)
        {
            HttpWebRequest httpWebRequest;
            if (this.m_type == PostSubmitter.PostTypeEnum.Post)
            {
                httpWebRequest = (HttpWebRequest)WebRequest.Create(new Uri(url));
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                httpWebRequest.ContentLength = (long)postData.Length;
                httpWebRequest.Timeout = 200000;
                
                using (Stream requestStream = ((WebRequest)httpWebRequest).GetRequestStream())
                {
                    byte[] bytes = new UTF8Encoding().GetBytes(postData);
                    requestStream.Write(bytes, 0, bytes.Length);
                }
            }
            else
            {
                httpWebRequest = (HttpWebRequest)WebRequest.Create(new Uri(url + "?" + postData));
                httpWebRequest.Method = "GET";
            }
            string str = string.Empty;
            try
            {
                using (HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    using (Stream responseStream = httpWebResponse.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(responseStream, Encoding.UTF8))
                            str = streamReader.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ATTENTION ! " + ex.Message);
                str = "###" + ex.Message;
            }
            return str;
        }

        private void EncodeAndAddItem(ref StringBuilder baseRequest, string key, string dataItem)
        {
            if (baseRequest == null)
                baseRequest = new StringBuilder();
            if (baseRequest.Length != 0)
                baseRequest.Append("&");
            if (key != null && key != string.Empty)
            {
                baseRequest.Append(key);
                baseRequest.Append("=");
            }
            baseRequest.Append(dataItem);
        }

        public enum PostTypeEnum
        {
            Get,
            Post,
        }
    }
}