﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using MobilyApp.Common;
using MobilyApp.Model;

namespace MobilyApp.ViewModels
{
    public class AbstractPrePaidVM : ViewModelBase
    {
        protected ObservableCollection<Item> m_refCollItem = (ObservableCollection<Item>)null;
        private PropertyChangedEventHandler<ObservableCollection<Item>> collItemChanged;

        private EventHandler<GenericEventArgs<string>> errorFound;
        private EventHandler<GenericEventArgs<string>> progress;
        public virtual ObservableCollection<Item> CollItem
        {
            get
            {
                return this.m_refCollItem;
            }
            set
            {
                ObservableCollection<Item> refOldValue = this.m_refCollItem;
                if (!this.ValidateCollItemChange(refOldValue, value))
                    return;
                this.m_refCollItem = value;
                this.HandleCollItemChanged(refOldValue, this.m_refCollItem);
                this.NotifyPropertyChanged("CollItem");
                this.RaiseCollItemChanged(refOldValue, this.m_refCollItem);
            }
        }
        public event PropertyChangedEventHandler<ObservableCollection<Item>> CollItemChanged
        {
            add
            {
                PropertyChangedEventHandler<ObservableCollection<Item>> changedEventHandler = this.collItemChanged;
                PropertyChangedEventHandler<ObservableCollection<Item>> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<ObservableCollection<Item>>>(ref this.collItemChanged, comparand + value, comparand);
                }
                while (changedEventHandler != comparand);
            }
            remove
            {
                PropertyChangedEventHandler<ObservableCollection<Item>> changedEventHandler = this.collItemChanged;
                PropertyChangedEventHandler<ObservableCollection<Item>> comparand;
                do
                {
                    comparand = changedEventHandler;
                    changedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler<ObservableCollection<Item>>>(ref this.collItemChanged, comparand - value, comparand);
                }
                while (changedEventHandler != comparand);
            }
        }
       
        public event EventHandler<GenericEventArgs<string>> ErrorFound
        {
            add
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.errorFound;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.errorFound, comparand + value, comparand);
                }
                while (eventHandler != comparand);
            }
            remove
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.errorFound;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.errorFound, comparand - value, comparand);
                }
                while (eventHandler != comparand);
            }
        }
        public event EventHandler<GenericEventArgs<string>> Progress
        {
            add
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.progress;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.progress, comparand + value, comparand);
                }
                while (eventHandler != comparand);
            }
            remove
            {
                EventHandler<GenericEventArgs<string>> eventHandler = this.progress;
                EventHandler<GenericEventArgs<string>> comparand;
                do
                {
                    comparand = eventHandler;
                    eventHandler = Interlocked.CompareExchange<EventHandler<GenericEventArgs<string>>>(ref this.progress, comparand - value, comparand);
                }
                while (eventHandler != comparand);
            }
        }

      
        protected void RaiseErrorFound(string ErrorName)
        {
            if (this.errorFound == null)
                return;
            this.errorFound((object)this, new GenericEventArgs<string>(ErrorName));
        }
        protected virtual bool ValidateCollItemChange(ObservableCollection<Item> refOldValue, ObservableCollection<Item> refRequestedValue)
        {
            return refOldValue != refRequestedValue;
        }
        protected virtual void HandleCollItemChanged(ObservableCollection<Item> refOldValue, ObservableCollection<Item> refNewValue)
        {
        }
        protected void RaiseCollItemChanged(ObservableCollection<Item> refOldValue, ObservableCollection<Item> refNewValue)
        {
            if (this.collItemChanged == null)
                return;
            this.collItemChanged((object)this, new PropertyChangedEventArgs<ObservableCollection<Item>>(refOldValue, refNewValue));
        }
    }
}