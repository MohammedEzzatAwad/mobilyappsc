﻿using MobilyApp.Controls.Clusters;
using MobilyApp.ViewModels;

namespace MobilyApp.Commands
{
   internal static class SetQueryGroupingStyleCommand
{
    // Methods
    internal static void Execute(PersonVM query, ClusterGroupingStyle groupingStyle)
    {
        query.GroupingStyle = groupingStyle;
    }
}

 

}