﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using Microsoft.Surface;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation.Input;
using MobilyApp.Helpers;

namespace MobilyApp.Controls.Clusters
{
    public class ClustersControl : Control
    {
        // Fields
        private DispatcherTimer _FrameTimer;
        private Random _Random = new Random();
        private ScatterView _ScatterView;

        public static readonly DependencyProperty AllowReturnToClusterProperty =
            DependencyProperty.Register("AllowReturnToCluster", typeof (bool), typeof (ClustersControl),
                                        new PropertyMetadata(false));

        private static readonly DependencyProperty ClusterHubStateProperty =
            DependencyProperty.RegisterAttached("ClusterHubState", typeof (ClusterHubState), typeof (ClustersControl),
                                                new UIPropertyMetadata(null));

        private static readonly DependencyProperty ClusterSpokeStateProperty =
            DependencyProperty.RegisterAttached("ClusterSpokeState", typeof (ClusterSpokeState),
                                                typeof (ClustersControl), new UIPropertyMetadata(null));

        public static readonly DependencyProperty HubStyleProperty = DependencyProperty.Register("HubStyle",
                                                                                                 typeof (Style),
                                                                                                 typeof (ClustersControl
                                                                                                     ),
                                                                                                 new PropertyMetadata(
                                                                                                     null));
 

        public static readonly RoutedEvent IntersectionChangedEvent =
            EventManager.RegisterRoutedEvent("IntersectionChanged", RoutingStrategy.Bubble,
                                             typeof (EventHandler<IntersectionChangedEventArgs>),
                                             typeof (ClustersControl));

        public static readonly DependencyProperty IsMagnetPreviewShowingProperty =
            DependencyProperty.RegisterAttached("IsMagnetPreviewShowing", typeof (bool), typeof (ClustersControl),
                                                new UIPropertyMetadata(false));

        public static readonly DependencyProperty MaxEasingProperty = DependencyProperty.Register("MaxEasing",
                                                                                                  typeof (double),
                                                                                                  typeof (
                                                                                                      ClustersControl),
                                                                                                  new PropertyMetadata(
                                                                                                      0.4));

        public static readonly DependencyProperty MaxSpokeDistanceProperty =
            DependencyProperty.Register("MaxSpokeDistance", typeof (double), typeof (ClustersControl),
                                        new PropertyMetadata(75.0));

        public static readonly DependencyProperty MinEasingProperty = DependencyProperty.Register("MinEasing",
                                                                                                  typeof (double),
                                                                                                  typeof (
                                                                                                      ClustersControl),
                                                                                                  new PropertyMetadata(
                                                                                                      0.12));

        public static readonly DependencyProperty MinSpokeDistanceProperty =
            DependencyProperty.Register("MinSpokeDistance", typeof (double), typeof (ClustersControl),
                                        new PropertyMetadata(50.0));

        public static readonly DependencyProperty OrientationRandomnessProperty =
            DependencyProperty.Register("OrientationRandomness", typeof (double), typeof (ClustersControl),
                                        new PropertyMetadata(15.0));

        public static readonly RoutedEvent OrphanedChangedEvent = EventManager.RegisterRoutedEvent("OrphanedChanged",
                                                                                                   RoutingStrategy.
                                                                                                       Bubble,
                                                                                                   typeof (
                                                                                                       EventHandler
                                                                                                       <
                                                                                                       OrphanedChangedEventArgs
                                                                                                       >),
                                                                                                   typeof (
                                                                                                       ClustersControl));

        public static readonly DependencyProperty SpokeStyleProperty = DependencyProperty.Register("SpokeStyle",
                                                                                                   typeof (Style),
                                                                                                   typeof (
                                                                                                       ClustersControl),
                                                                                                   new PropertyMetadata(
                                                                                                       null));

        public static readonly DependencyProperty SpokeStyleSelectorProperty =
            DependencyProperty.Register("SpokeStyleSelector", typeof (StyleSelector), typeof (ClustersControl),
                                        new PropertyMetadata(null));

        // Methods
        public void AddCluster(object hubData, IList<object> spokesData, Point? spawnFrom, double? orientTo,
                               IList<Point> avoid)
        {
            RoutedEventHandler handler = null;
            if (this.Clusters.ContainsKey(hubData))
            {
                throw new ArgumentException("Can't have two clusters with the same hub data.", "hubData");
            }
            Dictionary<object, IList<object>> writableClusterData = this.GetWritableClusterData();
            writableClusterData[hubData] = spokesData;
            this.Clusters = new ReadOnlyDictionary<object, IList<object>>(writableClusterData);
            ScatterViewItem item = new ScatterViewItem
                                       {
                                           IsTopmostOnActivation = false
                                       };
            ScatterViewItem hubSvi = item;
            hubSvi.DataContext = hubData;
            ClusterHubState state = new ClusterHubState();
            SetClusterHubState(hubSvi, state);
            state.ScatterViewItem = hubSvi;
            hubSvi.ContainerActivated += new RoutedEventHandler(this.Hub_ContainerActivated);
            hubSvi.PreviewMouseDown += new MouseButtonEventHandler(this.HubSvi_PreviewMouseDown);
            hubSvi.PreviewTouchDown += new EventHandler<TouchEventArgs>(this.HubSvi_PreviewTouchDown);
            hubSvi.Unloaded += delegate(object sender, RoutedEventArgs e)
                                   {
                                       hubSvi.ContainerActivated -= new RoutedEventHandler(this.Hub_ContainerActivated);
                                       hubSvi.PreviewMouseDown -=
                                           new MouseButtonEventHandler(this.HubSvi_PreviewMouseDown);
                                       hubSvi.PreviewTouchDown -=
                                           new EventHandler<TouchEventArgs>(this.HubSvi_PreviewTouchDown);
                                   };
            Style hubStyle = null;
            if ((hubStyle == null) && (this.HubStyle != null))
            {
                hubStyle = this.HubStyle;
            }
            if (hubStyle != null)
            {
                hubSvi.Style = hubStyle;
            }
            if (orientTo.HasValue)
            {
                hubSvi.Orientation = orientTo.Value +
                                     this._Random.Next((int) -this.OrientationRandomness,
                                                       (int) this.OrientationRandomness);
            }
            if (spawnFrom.HasValue)
            {
                hubSvi.Center = spawnFrom.Value;
                state.SpawningTo = new Point?(this.FindNewClusterLocation(spawnFrom, avoid));
            }
            else
            {
                hubSvi.Center = this.FindNewClusterLocation(null, avoid);
            }
            this._ScatterView.Items.Add(hubSvi);
            if (spokesData != null)
            {
                if (handler == null)
                {
                    handler = delegate(object sender, RoutedEventArgs e)
                                  {
                                      this.BuildSpokes(hubSvi, spokesData);
                                  };
                }
                hubSvi.Loaded += handler;
            }
        }

        public void AddOrphanedSpoke(object hubData, object spokeData, Point? spawnFrom, double? orientTo,
                                     IList<Point> avoid)
        {
            Dictionary<object, IList<object>> writableClusterData = this.GetWritableClusterData();
            if (writableClusterData[hubData] == null)
            {
                writableClusterData[hubData] = new List<object>();
            }
            writableClusterData[hubData].Add(spokeData);
            this.Clusters = new ReadOnlyDictionary<object, IList<object>>(writableClusterData);
            ScatterViewItem item2 = new ScatterViewItem
                                        {
                                            IsTopmostOnActivation = false
                                        };
            ScatterViewItem item = item2;
            item.DataContext = spokeData;
            ClusterSpokeState state2 = new ClusterSpokeState
                                           {
                                               ScatterViewItem = item,
                                               IsOrphaned = true,
                                               IsRemovedFromCluster = true,
                                               Easing =
                                                   this.MinEasing +
                                                   (this._Random.NextDouble()*(this.MaxEasing - this.MinEasing)),
                                               Hub = GetClusterHubState(this.GetHubFromData(hubData)),
                                               AnimationState = ClusterSpokeAnimationState.Neutral
                                           };
            ClusterSpokeState state = state2;
            SetClusterSpokeState(item, state);
            if (state.Hub.Spokes == null)
            {
                state.Hub.Spokes = new List<ClusterSpokeState>();
            }
            this.HookSpokeEvents(item);
            state.Hub.Spokes.Add(state);
            Style spokeStyle = null;
            if (this.SpokeStyleSelector != null)
            {
                spokeStyle = this.SpokeStyleSelector.SelectStyle(spokeData, this);
            }
            if ((spokeStyle == null) && (this.SpokeStyle != null))
            {
                spokeStyle = this.SpokeStyle;
            }
            if (spokeStyle != null)
            {
                item.Style = spokeStyle;
            }
            if (orientTo.HasValue)
            {
                item.Orientation = orientTo.Value +
                                   this._Random.Next((int) -this.OrientationRandomness, (int) this.OrientationRandomness);
            }
            if (spawnFrom.HasValue)
            {
                item.Center = spawnFrom.Value;
                state.SpawningTo = new Point?(this.FindNewClusterLocation(spawnFrom, avoid));
            }
            else
            {
                item.Center = this.FindNewClusterLocation(null, avoid);
            }
            this._ScatterView.Items.Add(item);
        }

        private static RectangleGeometry BuildRectangleGeometry(Size size, Point center, double orientation)
        {
            RectangleGeometry geometry =
                new RectangleGeometry(new Rect(new Point(0.0, 0.0), new Point(size.Width, size.Height)));
            Matrix identity = Matrix.Identity;
            identity.Translate(-size.Width/2.0, -size.Height/2.0);
            identity.Rotate(orientation);
            identity.Translate(center.X, center.Y);
            geometry.Transform = new MatrixTransform(identity);
            return geometry;
        }

        private void BuildSpokes(ScatterViewItem hubSvi, IList<object> spokesData)
        {
            if (spokesData != null)
            {
                ClusterHubState clusterHubState = GetClusterHubState(hubSvi);
                List<ClusterSpokeState> list = new List<ClusterSpokeState>();
                clusterHubState.Spokes = list;
                PathGeometry flattenedPathGeometry = new RectangleGeometry(new Rect(new Point(hubSvi.ActualCenter.X - (hubSvi.ActualWidth / 2.0), hubSvi.ActualCenter.Y - (hubSvi.ActualHeight / 2.0)), new Size(hubSvi.ActualWidth, hubSvi.ActualHeight))).GetFlattenedPathGeometry();
                double num = (hubSvi.ActualWidth * 2.0) + (hubSvi.ActualHeight * 2.0);
                double num2 = num / ((double)spokesData.Count);
                Point point = new Point();
                Point tangent = new Point();
                for (int i = 0; i < spokesData.Count; i++)
                {
                    object obj2 = spokesData[i];
                    double progress = (num2 * i) / num;
                    progress += 0.125;
                    progress = progress % 1.0;
                    flattenedPathGeometry.GetPointAtFractionLength(progress, out point, out tangent);
                    double num5 = Math.Atan2(point.Y - hubSvi.ActualCenter.Y, point.X - hubSvi.ActualCenter.X);
                    ClusterSpokeState state2 = null;
                    ScatterViewItem spokeSvi = null;
                    foreach (ScatterViewItem item2 in (IEnumerable)this._ScatterView.Items)
                    {
                        ClusterSpokeState clusterSpokeState = GetClusterSpokeState(item2);
                        if (((clusterSpokeState != null) && (item2.DataContext == obj2)) && clusterSpokeState.IsOrphaned)
                        {
                            spokeSvi = item2;
                            state2 = GetClusterSpokeState(item2);
                            state2.IsOrphaned = false;
                            base.RaiseEvent(new OrphanedChangedEventArgs(state2.Hub.ScatterViewItem.DataContext, state2.ScatterViewItem.DataContext, state2.IsOrphaned));
                            break;
                        }
                    }
                    if (state2 == null)
                    {
                        ClusterSpokeState state4 = new ClusterSpokeState();
                        Vector vector = (Vector)(point - hubSvi.ActualCenter);
                        state4.Distance = this._Random.Next((int) this.MinSpokeDistance,
                                                            ((int) this.MaxSpokeDistance) + 1) +vector.Length;
                        state4.Angle = num5;
                        state4.RandomOrientation = this._Random.Next((int)-this.OrientationRandomness, ((int)this.OrientationRandomness) + 1);
                        state4.Easing = this.MinEasing + (this._Random.NextDouble() * (this.MaxEasing - this.MinEasing));
                        state4.Hub = clusterHubState;
                        state4.AnimationState = ClusterSpokeAnimationState.Expanding;
                        state4.IsIntersectingCluster = true;
                        state2 = state4;
                        ScatterViewItem item3 = new ScatterViewItem
                        {
                            DataContext = obj2,
                            Center = hubSvi.ActualCenter,
                            Orientation = hubSvi.ActualOrientation,
                            IsTopmostOnActivation = false,
                            Tag = state2
                        };
                        spokeSvi = item3;
                        if (this.SpokeStyle != null)
                        {
                            spokeSvi.Style = this.SpokeStyle;
                        }
                        //Style spokeStyle = null;
                        //if (this.SpokeStyleSelector != null)
                        //{
                        //    spokeStyle = this.SpokeStyleSelector.SelectStyle(obj2, this);
                        //}
                        //if ((spokeStyle == null) && (this.SpokeStyle != null))
                        //{
                        //    spokeStyle = this.SpokeStyle;
                        //}
                        //if (spokeStyle != null)
                        //{
                        //    spokeSvi.Style = spokeStyle;
                        //}
                        this.HookSpokeEvents(spokeSvi);
                        state2.ScatterViewItem = spokeSvi;
                        SetClusterSpokeState(state2.ScatterViewItem, state2);
                        this._ScatterView.Items.Add(spokeSvi);
                    }
                    list.Add(state2);
                }
                this.Update();
                UpdateZIndexOnActivation(hubSvi);
            }

        }

        private Point FindNewClusterLocation(Point? spawnPosition, IList<Point> avoid)
        {
            if ((this._ScatterView.Items.Count == 0) && !spawnPosition.HasValue)
            {
                return new Point(base.ActualWidth/2.0, base.ActualHeight/2.0);
            }
            double actualHeight = base.ActualHeight;
            double actualWidth = base.ActualWidth;
            double num3 = 20.0;
            int num4 = (int) Math.Ceiling((double) (actualHeight/num3));
            int num5 = (int) Math.Ceiling((double) (actualWidth/num3));
            double num6 = 0.0;
            Point point = new Point();
            double num7 = 1.2;
            double num8 = 0.8;
            for (int i = 0; i < num4; i++)
            {
                for (int j = 0; j < num5; j++)
                {
                    double y = (i*num3) + (num3/2.0);
                    double x = (j*num3) + (num3/2.0);
                    Point point2 = new Point(x, y);
                    double positiveInfinity = double.PositiveInfinity;
                    positiveInfinity =
                        Math.Min(
                            Math.Min(Math.Min(Math.Min(positiveInfinity, y*num7), (actualWidth - x)*num7),
                                     (actualHeight - y)*num7), x*num7);
                    if (spawnPosition.HasValue)
                    {
                        Vector vector = (Vector) (point2 - spawnPosition.Value);
                        positiveInfinity = Math.Min(positiveInfinity, vector.Length);
                    }
                    foreach (ScatterViewItem item in (IEnumerable) this._ScatterView.Items)
                    {
                        ClusterSpokeState clusterSpokeState = GetClusterSpokeState(item);
                        if ((clusterSpokeState == null) || clusterSpokeState.IsRemovedFromCluster)
                        {
                            Vector vector2 = (Vector) (item.ActualCenter - point2);
                            positiveInfinity = Math.Min(positiveInfinity, vector2.Length*num8);
                        }
                    }
                    if (SurfaceKeyboard.IsVisible)
                    {
                        Vector vector3 =
                            (Vector)
                            (new Point((double) SurfaceKeyboard.CenterX, (double) SurfaceKeyboard.CenterY) - point2);
                        positiveInfinity = Math.Min(positiveInfinity, vector3.Length*num8);
                    }
                    foreach (TouchDevice device in from touch in Application.Current.MainWindow.TouchesOver
                                                   where touch.GetTagData() != TagData.None
                                                   select touch)
                    {
                        Point position = device.GetPosition(this);
                        Vector vector4 = (Vector) (position - point2);
                        positiveInfinity = Math.Min(positiveInfinity, vector4.Length*num8);
                    }
                    if (avoid != null)
                    {
                        foreach (Point point4 in avoid)
                        {
                            Vector vector5 = (Vector) (point4 - point2);
                            positiveInfinity = Math.Min(positiveInfinity, vector5.Length*num8);
                        }
                    }
                    if (positiveInfinity > num6)
                    {
                        num6 = positiveInfinity;
                        point = point2;
                    }
                }
            }
            return point;
        }

        private static ClusterHubState GetClusterHubState(DependencyObject obj)
        {
            return (ClusterHubState) obj.GetValue(ClusterHubStateProperty);
        }

        private static ClusterSpokeState GetClusterSpokeState(DependencyObject obj)
        {
            return (ClusterSpokeState) obj.GetValue(ClusterSpokeStateProperty);
        }

        private static double GetDistanceFromCluster(ClusterHubState hubState, ClusterSpokeState testSpokeState)
        {
            Vector vector =
                (Vector) (testSpokeState.ScatterViewItem.ActualCenter - hubState.ScatterViewItem.ActualCenter);
            double length = vector.Length;
            foreach (ClusterSpokeState state in hubState.Spokes)
            {
                if ((state != testSpokeState) && !state.IsRemovedFromCluster)
                {
                    Vector vector2 = (Vector) (testSpokeState.ScatterViewItem.ActualCenter - state.DesiredCenter);
                    length = Math.Min(vector2.Length, length);
                }
            }
            return length;
        }

        private ScatterViewItem GetHubFromData(object hubData)
        {
            ScatterViewItem item = null;
            foreach (ScatterViewItem item2 in (IEnumerable) this._ScatterView.Items)
            {
                if ((GetClusterHubState(item2) != null) && (item2.DataContext == hubData))
                {
                    item = item2;
                    break;
                }
            }
            if (item == null)
            {
                throw new InvalidOperationException("Couldn't find hub.");
            }
            return item;
        }

        public static bool GetIsMagnetPreviewShowing(DependencyObject obj)
        {
            if (obj == null)
            {
                return false;
            }
            return (bool) obj.GetValue(IsMagnetPreviewShowingProperty);
        }

        private ScatterViewItem GetSpokeFromData(object spokeData)
        {
            ScatterViewItem item = null;
            foreach (ScatterViewItem item2 in (IEnumerable) this._ScatterView.Items)
            {
                if ((GetClusterSpokeState(item2) != null) && (item2.DataContext == spokeData))
                {
                    item = item2;
                    break;
                }
            }
            if (item == null)
            {
                throw new InvalidOperationException("Couldn't find spoke.");
            }
            return item;
        }

        private Point GetSpokeLocation(ClusterSpokeState spokeState)
        {
            ScatterViewItem scatterViewItem = spokeState.ScatterViewItem;
            Matrix identity = Matrix.Identity;
            identity.Translate(-scatterViewItem.ActualWidth/2.0, -scatterViewItem.ActualHeight/2.0);
            double d = (spokeState.Hub.GroupingStyle == ClusterGroupingStyle.Circular)
                           ? spokeState.Angle
                           : -1.5707963267948966;
            identity.Translate(spokeState.CurrentDistance*Math.Cos(d), spokeState.CurrentDistance*Math.Sin(d));
            identity.Translate(spokeState.Hub.ScatterViewItem.ActualCenter.X,
                               spokeState.Hub.ScatterViewItem.ActualCenter.Y);
            identity.RotateAt(spokeState.Hub.ScatterViewItem.ActualOrientation,
                              spokeState.Hub.ScatterViewItem.ActualCenter.X - (scatterViewItem.ActualWidth / 2.0),
                              spokeState.Hub.ScatterViewItem.ActualCenter.Y - (scatterViewItem.ActualHeight / 2.0));
            double num2 = Math.Min(scatterViewItem.ActualWidth, scatterViewItem.ActualHeight)*0.2;
            if (identity.OffsetX < -num2)
            {
                identity.Translate(-identity.OffsetX - num2, 0.0);
            }
            else if (identity.OffsetX > (base.ActualWidth - num2))
            {
                identity.Translate((base.ActualWidth - identity.OffsetX) - num2, 0.0);
            }
            if (identity.OffsetY < -num2)
            {
                identity.Translate(0.0, -identity.OffsetY - num2);
            }
            else if (identity.OffsetY > (base.ActualHeight - num2))
            {
                identity.Translate(0.0, (base.ActualHeight - identity.OffsetY) - num2);
            }
            identity.Translate(spokeState.CurrentFlickDistance*Math.Cos(spokeState.FlickAngle),
                               spokeState.CurrentFlickDistance*Math.Sin(spokeState.FlickAngle));
            return new Point(identity.OffsetX, identity.OffsetY);
        }

        private Dictionary<object, IList<object>> GetWritableClusterData()
        {
            Dictionary<object, IList<object>> dictionary = new Dictionary<object, IList<object>>();
            foreach (KeyValuePair<object, IList<object>> pair in this.Clusters)
            {
                dictionary[pair.Key] = pair.Value;
            }
            return dictionary;
        }

        private void HookSpokeEvents(ScatterViewItem spokeSvi)
        {
            spokeSvi.ContainerActivated += new RoutedEventHandler(this.SpokeSvi_ContainerActivated);
            spokeSvi.ContainerManipulationStarted +=
                new ContainerManipulationStartedEventHandler(this.SpokeSvi_ContainerManipulationStarted);
            spokeSvi.ContainerManipulationDelta +=
                new ContainerManipulationDeltaEventHandler(this.SpokeSvi_ContainerManipulationDelta);
            spokeSvi.ContainerManipulationCompleted +=
                new ContainerManipulationCompletedEventHandler(this.SpokeSvi_ContainerManipulationCompleted);
            spokeSvi.ContainerDeactivated += new RoutedEventHandler(this.SpokeSvi_ContainerDeactivated);
            spokeSvi.Loaded += new RoutedEventHandler(this.SpokeSvi_Loaded);
            spokeSvi.PreviewMouseDown += new MouseButtonEventHandler(this.SpokeSvi_PreviewMouseDown);
            spokeSvi.PreviewTouchDown += new EventHandler<TouchEventArgs>(this.SpokeSvi_PreviewTouchDown);
            TouchExtensions.AddTapGestureHandler(spokeSvi, new EventHandler<TouchEventArgs>(this.SpokeSvi_TapGesture));
            spokeSvi.Unloaded += delegate(object sender, RoutedEventArgs e)
                                     {
                                         spokeSvi.ContainerActivated -=
                                             new RoutedEventHandler(this.SpokeSvi_ContainerActivated);
                                         spokeSvi.ContainerManipulationStarted -=
                                             new ContainerManipulationStartedEventHandler(
                                                 this.SpokeSvi_ContainerManipulationStarted);
                                         spokeSvi.ContainerManipulationDelta -=
                                             new ContainerManipulationDeltaEventHandler(
                                                 this.SpokeSvi_ContainerManipulationDelta);
                                         spokeSvi.ContainerManipulationCompleted -=
                                             new ContainerManipulationCompletedEventHandler(
                                                 this.SpokeSvi_ContainerManipulationCompleted);
                                         spokeSvi.ContainerDeactivated -=
                                             new RoutedEventHandler(this.SpokeSvi_ContainerDeactivated);
                                         spokeSvi.PreviewMouseDown -=
                                             new MouseButtonEventHandler(this.SpokeSvi_PreviewMouseDown);
                                         spokeSvi.PreviewTouchDown -=
                                             new EventHandler<TouchEventArgs>(this.SpokeSvi_PreviewTouchDown);
                                         spokeSvi.Loaded -= new RoutedEventHandler(this.SpokeSvi_Loaded);
                                         TouchExtensions.RemoveTapGestureHandler(spokeSvi,
                                                                                 new EventHandler<TouchEventArgs>(
                                                                                     this.SpokeSvi_TapGesture));
                                     };
        }

        private void Hub_ContainerActivated(object sender, RoutedEventArgs e)
        {
            UpdateZIndexOnActivation(e.OriginalSource);
        }

        private void HubSvi_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            UpdateZIndexOnActivation(e.OriginalSource);
        }

        private void HubSvi_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            UpdateZIndexOnActivation(e.OriginalSource);
        }

        private static bool IsSpokeIntersectingCluster(ClusterSpokeState spokeState)
        {
            ClusterHubState hub = spokeState.Hub;
            ScatterViewItem scatterViewItem = spokeState.ScatterViewItem;
            RectangleGeometry geometry =
                BuildRectangleGeometry(new Size(scatterViewItem.MinWidth, scatterViewItem.MinHeight),
                                       scatterViewItem.ActualCenter, scatterViewItem.ActualOrientation);
            foreach (ClusterSpokeState state2 in hub.Spokes)
            {
                if (
                    BuildRectangleGeometry(new Size(state2.ScatterViewItem.MinWidth, state2.ScatterViewItem.MinHeight),
                                           state2.DesiredCenter,
                                           state2.Hub.ScatterViewItem.ActualOrientation + state2.RandomOrientation).
                        FillContainsWithDetail(geometry) == IntersectionDetail.Intersects)
                {
                    return true;
                }
            }
            return
                (BuildRectangleGeometry(new Size(hub.ScatterViewItem.MinWidth, hub.ScatterViewItem.MinHeight),
                                        hub.ScatterViewItem.ActualCenter, hub.ScatterViewItem.ActualOrientation).
                     FillContainsWithDetail(geometry) == IntersectionDetail.Intersects);
        }

        private static void MoveSpoke(ClusterSpokeState spokeState, Point location)
        {
            ScatterViewItem scatterViewItem = spokeState.ScatterViewItem;
            double x = scatterViewItem.ActualCenter.X;
            double num2 = location.X + (scatterViewItem.ActualWidth/2.0);
            double num3 = num2 - x;
            bool flag = Math.Abs(num3) > 1.0;
            if (flag)
            {
                x += num3*spokeState.Easing;
            }
            double y = scatterViewItem.ActualCenter.Y;
            double num5 = location.Y + (scatterViewItem.ActualHeight/2.0);
            double num6 = num5 - y;
            bool flag2 = Math.Abs(num6) > 1.0;
            if (flag2)
            {
                y += num6*spokeState.Easing;
            }
            spokeState.DesiredCenter = new Point(num2, num5);
            if (!spokeState.IsRemovedFromCluster && (flag || flag2))
            {
                scatterViewItem.Center = new Point(x, y);
            }
            if (!spokeState.IsRemovedFromCluster)
            {
                double num7 = spokeState.Hub.ScatterViewItem.ActualOrientation + spokeState.RandomOrientation;
                double num8 = num7 - scatterViewItem.ActualOrientation;
                num8 = num8%360.0;
                if (num8 > 180.0)
                {
                    num8 -= 360.0;
                }
                else if (num8 < -180.0)
                {
                    num8 += 360.0;
                }
                if (Math.Abs(num8) > 1.0)
                {
                    scatterViewItem.Orientation = scatterViewItem.ActualOrientation + (num8*spokeState.Easing);
                }
            }
        }

        public override void OnApplyTemplate()
        {
            EventHandler handler = null;
            if (!DesignerProperties.GetIsInDesignMode(this))
            {
                this.Clusters = new ReadOnlyDictionary<object, IList<object>>();
                this._ScatterView = base.GetTemplateChild("PART_ScatterView") as ScatterView;
                DispatcherTimer timer = new DispatcherTimer(DispatcherPriority.Send)
                                            {
                                                Interval = TimeSpan.FromSeconds(0.016666666666666666)
                                            };
                this._FrameTimer = timer;
                if (handler == null)
                {
                    handler = delegate(object sender, EventArgs e)
                                  {
                                      this.Update();
                                  };
                }
                this._FrameTimer.Tick += handler;
                this._FrameTimer.Start();
            }
            base.OnApplyTemplate();
        }

        public void RemoveCluster(object hubData)
        {
            ScatterViewItem hubFromData = this.GetHubFromData(hubData);
            ClusterHubState clusterHubState = GetClusterHubState(hubFromData);
            Dictionary<object, IList<object>> writableClusterData = this.GetWritableClusterData();
            writableClusterData.Remove(hubData);
            this.Clusters = new ReadOnlyDictionary<object, IList<object>>(writableClusterData);
            if (clusterHubState.Spokes != null)
            {
                foreach (ClusterSpokeState state2 in clusterHubState.Spokes)
                {
                    state2.AnimationState = ClusterSpokeAnimationState.Collapsing;
                    state2.ScatterViewItem.IsEnabled = false;
                }
            }
            clusterHubState.IsRemoving = true;
            hubFromData.IsEnabled = false;
        }

        public void RemoveOrphanedSpoke(object spokeData)
        {
            ScatterViewItem spokeFromData = this.GetSpokeFromData(spokeData);
            ClusterSpokeState clusterSpokeState = GetClusterSpokeState(spokeFromData);
            if (!clusterSpokeState.IsOrphaned)
            {
                throw new InvalidOperationException("Couldn't find orphan to remove.");
            }
            clusterSpokeState.AnimationState = ClusterSpokeAnimationState.Destroying;
            clusterSpokeState.EverRemovedFromCluster = false;
            spokeFromData.IsEnabled = false;
            if (clusterSpokeState.Hub == null)
            {
                Dictionary<object, IList<object>> writableClusterData = this.GetWritableClusterData();
                writableClusterData[false].Remove(spokeFromData.DataContext);
                this.Clusters = new ReadOnlyDictionary<object, IList<object>>(writableClusterData);
            }
        }

        public void ReturnRemovedSpoke(object spokeData)
        {
            ScatterViewItem spokeFromData = this.GetSpokeFromData(spokeData);
            ClusterSpokeState clusterSpokeState = GetClusterSpokeState(spokeFromData);
            spokeFromData.CancelManipulation();
            spokeFromData.ReleaseAllTouchCaptures();
            spokeFromData.ReleaseMouseCapture();
            clusterSpokeState.IsRemovedFromCluster = false;
            clusterSpokeState.IsOrphaned = false;
            clusterSpokeState.EverRemovedFromCluster = false;
            clusterSpokeState.IsIntersectingCluster = true;
            UpdateZIndexOnActivation(spokeFromData);
        }

        public void SetClusterGroupingStyle(object hubData, ClusterGroupingStyle groupingStyle)
        {
            ClusterHubState clusterHubState = GetClusterHubState(this.GetHubFromData(hubData));
            clusterHubState.GroupingStyle = groupingStyle;
            if ((clusterHubState.Spokes.Count != 0) && (clusterHubState.GroupingStyle == ClusterGroupingStyle.Stacked))
            {
                clusterHubState.StackDistance =
                    ((IEnumerable<double>) (from spoke in clusterHubState.Spokes select spoke.ScatterViewItem.MinHeight))
                        .Max();
            }
        }

        private static void SetClusterHubState(DependencyObject obj, ClusterHubState value)
        {
            obj.SetValue(ClusterHubStateProperty, value);
        }

        private static void SetClusterSpokeState(DependencyObject obj, ClusterSpokeState value)
        {
            obj.SetValue(ClusterSpokeStateProperty, value);
        }

        public static void SetIsMagnetPreviewShowing(DependencyObject obj, bool value)
        {
            if (obj != null)
            {
                obj.SetValue(IsMagnetPreviewShowingProperty, value);
            }
        }

        private void SetSpokeDistance(ClusterSpokeState spokeState)
        {
            Func<ClusterSpokeState, bool> predicate = null;
            ScatterViewItem scatterViewItem = spokeState.ScatterViewItem;
            double num = (spokeState.Hub.GroupingStyle == ClusterGroupingStyle.Circular)
                             ? spokeState.Distance
                             : spokeState.Hub.StackDistance;
            if (spokeState.AnimationState == ClusterSpokeAnimationState.Expanding)
            {
                double num2 = num - spokeState.CurrentDistance;
                if (Math.Abs(num2) > 1.0)
                {
                    spokeState.CurrentDistance += num2*(spokeState.Easing*3.0);
                }
                else
                {
                    spokeState.CurrentDistance = num;
                    spokeState.AnimationState = ClusterSpokeAnimationState.Neutral;
                    scatterViewItem.IsEnabled = true;
                }
                scatterViewItem.Opacity = spokeState.CurrentDistance/num;
            }
            else if (spokeState.AnimationState == ClusterSpokeAnimationState.Collapsing)
            {
                if (Math.Abs(spokeState.CurrentDistance) > 1.0)
                {
                    spokeState.CurrentDistance -= spokeState.CurrentDistance*(spokeState.Easing*3.0);
                }
                else
                {
                    spokeState.CurrentDistance = 0.0;
                }
                scatterViewItem.Opacity = spokeState.CurrentDistance/num;
            }
            else if (spokeState.AnimationState == ClusterSpokeAnimationState.Flicking)
            {
                Vector vector =
                    (Vector) (new Point() - new Point(scatterViewItem.ActualWidth, scatterViewItem.ActualHeight));
                double num3 = vector.Length - spokeState.CurrentFlickDistance;
                if (num3 > 1.0)
                {
                    spokeState.CurrentFlickDistance += num3*(this.MinEasing*6.0);
                }
                else
                {
                    int num4 =
                        ((IEnumerable<int>)
                         (from siblingSpokeState in spokeState.Hub.Spokes
                          select Panel.GetZIndex(siblingSpokeState.ScatterViewItem))).Min();
                    Panel.SetZIndex(spokeState.ScatterViewItem, num4);
                    if (predicate == null)
                    {
                        predicate = siblingSpokeState => siblingSpokeState.ScatterViewItem != spokeState.ScatterViewItem;
                    }
                    foreach (
                        ScatterViewItem item2 in
                            from siblingSpokeState in spokeState.Hub.Spokes.Where<ClusterSpokeState>(predicate)
                            select siblingSpokeState.ScatterViewItem
                            into svi
                            orderby Panel.GetZIndex(svi)
                            select svi)
                    {
                        Panel.SetZIndex(item2, ++num4);
                    }
                    spokeState.CurrentFlickDistance = 0.0;
                    spokeState.AnimationState = ClusterSpokeAnimationState.Neutral;
                }
            }
            else
            {
                double num5 = num - spokeState.CurrentDistance;
                if (Math.Abs(num5) > 1.0)
                {
                    spokeState.CurrentDistance += num5*(spokeState.Easing*3.0);
                }
                else
                {
                    spokeState.CurrentDistance = num;
                }
            }
        }

        private void SpokeSvi_ContainerActivated(object sender, RoutedEventArgs e)
        {
            UpdateZIndexOnActivation(e.OriginalSource);
        }

        private void SpokeSvi_ContainerDeactivated(object sender, RoutedEventArgs e)
        {
            ScatterViewItem item = sender as ScatterViewItem;
            ClusterSpokeState spokeState = GetClusterSpokeState(item);
            if (((spokeState.Hub.GroupingStyle == ClusterGroupingStyle.Stacked) && !spokeState.EverRemovedFromCluster) &&
                (spokeState.IsIntersectingCluster && ((from spoke in spokeState.Hub.Spokes
                                                       where !spoke.IsRemovedFromCluster && (spoke != spokeState)
                                                       select spoke).Count<ClusterSpokeState>() > 0)))
            {
                item.CancelManipulation();
                Vector vector = (Vector) (spokeState.ScatterViewItem.ActualCenter - spokeState.OriginalLocation);
                spokeState.FlickAngle = Math.Atan2(vector.Y, vector.X);
                spokeState.AnimationState = ClusterSpokeAnimationState.Flicking;
                spokeState.CurrentFlickDistance = 0.0;
            }
        }

        private void SpokeSvi_ContainerManipulationCompleted(object sender, ContainerManipulationCompletedEventArgs e)
        {
            ScatterViewItem item = sender as ScatterViewItem;
            ClusterSpokeState clusterSpokeState = GetClusterSpokeState(item);
            if (!clusterSpokeState.IsOrphaned &&
                (!clusterSpokeState.EverRemovedFromCluster || this.AllowReturnToCluster))
            {
                this.UpdateIsIntersectingCluster(clusterSpokeState, null);
                clusterSpokeState.IsRemovedFromCluster = !clusterSpokeState.IsIntersectingCluster;
                UpdateIsMagnetPreviewShowing(clusterSpokeState.Hub);
            }
        }

        private void SpokeSvi_ContainerManipulationDelta(object sender, ContainerManipulationDeltaEventArgs e)
        {
            ScatterViewItem item = sender as ScatterViewItem;
            ClusterSpokeState clusterSpokeState = GetClusterSpokeState(item);
            if (!clusterSpokeState.IsOrphaned &&
                (!clusterSpokeState.EverRemovedFromCluster || this.AllowReturnToCluster))
            {
                bool? overrideIntersection = null;
                if (e.ScaleFactor > 1.0)
                {
                    overrideIntersection = false;
                }
                this.UpdateIsIntersectingCluster(clusterSpokeState, overrideIntersection);
                UpdateIsMagnetPreviewShowing(clusterSpokeState.Hub);
                if (!clusterSpokeState.IsIntersectingCluster)
                {
                    clusterSpokeState.EverRemovedFromCluster = true;
                }
            }
        }

        private void SpokeSvi_ContainerManipulationStarted(object sender, ContainerManipulationStartedEventArgs e)
        {
            ScatterViewItem item = sender as ScatterViewItem;
            ClusterSpokeState clusterSpokeState = GetClusterSpokeState(item);
            if (!clusterSpokeState.IsOrphaned &&
                (!clusterSpokeState.EverRemovedFromCluster || this.AllowReturnToCluster))
            {
                clusterSpokeState.IsRemovedFromCluster = true;
                clusterSpokeState.EverRemovedFromCluster = false;
                this.UpdateIsIntersectingCluster(clusterSpokeState, null);
                UpdateIsMagnetPreviewShowing(clusterSpokeState.Hub);
                clusterSpokeState.OriginalLocation = item.ActualCenter;
            }
        }

        private void SpokeSvi_Loaded(object sender, RoutedEventArgs e)
        {
            ClusterSpokeState clusterSpokeState = GetClusterSpokeState(sender as ScatterViewItem);
            if ((from spoke in clusterSpokeState.Hub.Spokes select spoke.ScatterViewItem.IsLoaded).Count<bool>() ==
                clusterSpokeState.Hub.Spokes.Count)
            {
                this.SetClusterGroupingStyle(clusterSpokeState.Hub.ScatterViewItem.DataContext,
                                             clusterSpokeState.Hub.GroupingStyle);
            }
        }

        private void SpokeSvi_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            UpdateZIndexOnActivation(e.OriginalSource);
        }

        private void SpokeSvi_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            UpdateZIndexOnActivation(e.OriginalSource);
        }

        private void SpokeSvi_TapGesture(object sender, TouchEventArgs e)
        {
            ScatterViewItem item = sender as ScatterViewItem;
            ClusterSpokeState clusterSpokeState = GetClusterSpokeState(item);
            if ((((clusterSpokeState.Hub.GroupingStyle == ClusterGroupingStyle.Stacked) &&
                  !clusterSpokeState.EverRemovedFromCluster) && clusterSpokeState.IsIntersectingCluster) &&
                ((from spoke in clusterSpokeState.Hub.Spokes
                  where !spoke.IsRemovedFromCluster
                  select spoke).Count<ClusterSpokeState>() > 1))
            {
                item.CancelManipulation();
                clusterSpokeState.FlickAngle = -90.0;
                clusterSpokeState.AnimationState = ClusterSpokeAnimationState.Flicking;
                clusterSpokeState.CurrentFlickDistance = 0.0;
            }
        }

        private void Update()
        {
            this.UpdateSpawningHubsAndSpokes();
            this.UpdateRemovingClusters();
            this.UpdateUpdatingClusters();
            this.UpdateSpokeLocations();
            this.UpdateOrientations();
        }

        public void UpdateCluster(object hubData, IList<object> spokesData)
        {
            ClusterHubState clusterHubState = GetClusterHubState(this.GetHubFromData(hubData));
            Dictionary<object, IList<object>> writableClusterData = this.GetWritableClusterData();
            writableClusterData[hubData] = spokesData;
            this.Clusters = new ReadOnlyDictionary<object, IList<object>>(writableClusterData);
            if (clusterHubState.Spokes != null)
            {
                foreach (ClusterSpokeState state2 in from spoke in clusterHubState.Spokes
                                                     where spoke.IsIntersectingCluster
                                                     select spoke)
                {
                    state2.AnimationState = ClusterSpokeAnimationState.Collapsing;
                    state2.ScatterViewItem.IsEnabled = false;
                    state2.ScatterViewItem.ReleaseAllCaptures();
                }
                foreach (ClusterSpokeState state3 in from spoke in clusterHubState.Spokes
                                                     where spoke.IsRemovedFromCluster
                                                     select spoke)
                {
                    state3.IsOrphaned = true;
                    base.RaiseEvent(new OrphanedChangedEventArgs(state3.Hub.ScatterViewItem.DataContext,
                                                                 state3.ScatterViewItem.DataContext, state3.IsOrphaned));
                }
            }
            clusterHubState.IsUpdating = true;
        }

        private void UpdateIsIntersectingCluster(ClusterSpokeState spokeState, bool? overrideIntersection)
        {
            bool flag = !overrideIntersection.HasValue
                            ? IsSpokeIntersectingCluster(spokeState)
                            : overrideIntersection.Value;
            if (flag != spokeState.IsIntersectingCluster)
            {
                spokeState.IsIntersectingCluster = flag;
                base.RaiseEvent(new IntersectionChangedEventArgs(spokeState.Hub.ScatterViewItem.DataContext,
                                                                 spokeState.ScatterViewItem.DataContext,
                                                                 spokeState.IsIntersectingCluster));
            }
        }

        private static void UpdateIsMagnetPreviewShowing(ClusterHubState hubState)
        {
            bool flag = (from spoke in hubState.Spokes
                         where spoke.IsIntersectingCluster && spoke.IsRemovedFromCluster
                         select spoke).FirstOrDefault<ClusterSpokeState>() != null;
            SetIsMagnetPreviewShowing(hubState.ScatterViewItem, flag);
        }

        private void UpdateOrientations()
        {
            if ((InteractiveSurface.PrimarySurfaceDevice.IsFingerRecognitionSupported &&
                 InteractiveSurface.PrimarySurfaceDevice.IsTouchOrientationSupported) &&
                ((InteractiveSurface.PrimarySurfaceDevice.Tilt == Tilt.Horizontal) ||
                 (InteractiveSurface.PrimarySurfaceDevice.Tilt == Tilt.Tilted)))
            {
                List<ScatterViewItem> list = new List<ScatterViewItem>();
                foreach (ScatterViewItem item in (IEnumerable) this._ScatterView.Items)
                {
                    ClusterSpokeState clusterSpokeState = GetClusterSpokeState(item);
                    if (((clusterSpokeState != null) && (item.TouchesCaptured.Count<TouchDevice>() == 1)) &&
                        (item.TouchesCapturedWithin.First<TouchDevice>().GetIsFingerRecognized() &&
                         !clusterSpokeState.EverRemovedFromCluster))
                    {
                        list.Add(item);
                    }
                }
                foreach (ScatterViewItem item2 in list)
                {
                    double num = item2.TouchesCapturedWithin.First<TouchDevice>().GetOrientation(this._ScatterView) +
                                 90.0;
                    double num2 = num - item2.ActualOrientation;
                    num2 = num2%360.0;
                    if (num2 > 180.0)
                    {
                        num2 -= 360.0;
                    }
                    else if (num2 < -180.0)
                    {
                        num2 += 360.0;
                    }
                    if (Math.Abs(num2) > 1.0)
                    {
                        item2.Orientation = item2.ActualOrientation + (num2*this.MaxEasing);
                    }
                }
            }
        }

        private void UpdateRemovingClusters()
        {
            for (int i = 0; i < this._ScatterView.Items.Count; i++)
            {
                ScatterViewItem item = this._ScatterView.Items[i] as ScatterViewItem;
                ClusterHubState clusterHubState = GetClusterHubState(item);
                if ((clusterHubState != null) && clusterHubState.IsRemoving)
                {
                    List<ClusterSpokeState> spokes = clusterHubState.Spokes as List<ClusterSpokeState>;
                    item.Opacity -= item.Opacity*(this.MinEasing*3.0);
                    foreach (ScatterViewItem item2 in (IEnumerable) this._ScatterView.Items)
                    {
                        ClusterSpokeState clusterSpokeState = GetClusterSpokeState(item2);
                        if (((clusterSpokeState != null) && (clusterSpokeState.Hub == clusterHubState)) &&
                            clusterSpokeState.IsRemovedFromCluster)
                        {
                            clusterSpokeState.ScatterViewItem.Opacity = item.Opacity;
                        }
                    }
                    if (spokes == null)
                    {
                        if (item.Opacity < 0.1)
                        {
                            clusterHubState.IsRemoving = false;
                            clusterHubState.ScatterViewItem = null;
                            clusterHubState.Spokes = null;
                            this._ScatterView.Items.Remove(item);
                            i--;
                        }
                    }
                    else
                    {
                        IEnumerable<ClusterSpokeState> source = from spoke in spokes
                                                                where
                                                                    spoke.AnimationState ==
                                                                    ClusterSpokeAnimationState.Collapsing
                                                                select spoke;
                        IEnumerable<ClusterSpokeState> enumerable2 = from spoke in source
                                                                     where spoke.CurrentDistance == 0.0
                                                                     select spoke;
                        if ((source.Count<ClusterSpokeState>() == enumerable2.Count<ClusterSpokeState>()) &&
                            (item.Opacity < 0.1))
                        {
                            for (int j = 0; j < this._ScatterView.Items.Count; j++)
                            {
                                ScatterViewItem item3 = this._ScatterView.Items[j] as ScatterViewItem;
                                ClusterSpokeState state3 = GetClusterSpokeState(item3);
                                if ((state3 != null) && (state3.Hub == clusterHubState))
                                {
                                    SetClusterSpokeState(item3, null);
                                    this._ScatterView.Items.Remove(item3);
                                    state3.ScatterViewItem = null;
                                    state3.Hub = null;
                                    j--;
                                }
                            }
                            clusterHubState.IsRemoving = false;
                            clusterHubState.ScatterViewItem = null;
                            clusterHubState.Spokes = null;
                            SetClusterHubState(item, null);
                            this._ScatterView.Items.Remove(item);
                            i--;
                        }
                    }
                }
            }
        }

        private void UpdateSpawningHubsAndSpokes()
        {
            List<ScatterViewItem> list = new List<ScatterViewItem>();
            foreach (ScatterViewItem item in (IEnumerable) this._ScatterView.Items)
            {
                ClusterHubState clusterHubState = GetClusterHubState(item);
                if (((clusterHubState != null) && item.IsLoaded) && clusterHubState.SpawningTo.HasValue)
                {
                    list.Add(item);
                }
            }
            foreach (ScatterViewItem item2 in (IEnumerable) this._ScatterView.Items)
            {
                ClusterSpokeState clusterSpokeState = GetClusterSpokeState(item2);
                if (((clusterSpokeState != null) && item2.IsLoaded) && clusterSpokeState.SpawningTo.HasValue)
                {
                    list.Add(item2);
                }
            }
            foreach (ScatterViewItem item3 in list)
            {
                Point? spawningTo = null;
                ClusterHubState state3 = GetClusterHubState(item3);
                if ((state3 != null) && state3.SpawningTo.HasValue)
                {
                    spawningTo = state3.SpawningTo;
                }
                ClusterSpokeState state4 = GetClusterSpokeState(item3);
                if ((state4 != null) && state4.SpawningTo.HasValue)
                {
                    spawningTo = state4.SpawningTo;
                }
                if (item3.IsLoaded && spawningTo.HasValue)
                {
                    Point point = spawningTo.Value;
                    double x = item3.ActualCenter.X;
                    double num2 = point.X - x;
                    bool flag = Math.Abs(num2) > 1.0;
                    if (flag)
                    {
                        x += num2*(this.MinEasing + ((this.MaxEasing - this.MinEasing)/2.0));
                    }
                    double y = item3.ActualCenter.Y;
                    double num4 = point.Y - y;
                    bool flag2 = Math.Abs(num4) > 1.0;
                    if (flag2)
                    {
                        y += num4*(this.MinEasing + ((this.MaxEasing - this.MinEasing)/2.0));
                    }
                    if (flag || flag2)
                    {
                        item3.Center = new Point(x, y);
                        continue;
                    }
                    if (state3 != null)
                    {
                        state3.SpawningTo = null;
                    }
                    if (state4 != null)
                    {
                        state4.SpawningTo = null;
                    }
                }
            }
        }

        private void UpdateSpokeLocations()
        {
            for (int i = 0; i < this._ScatterView.Items.Count; i++)
            {
                ScatterViewItem item = this._ScatterView.Items[i] as ScatterViewItem;
                ClusterSpokeState clusterSpokeState = GetClusterSpokeState(item);
                if (clusterSpokeState != null)
                {
                    if (clusterSpokeState.IsOrphaned)
                    {
                        if (clusterSpokeState.AnimationState == ClusterSpokeAnimationState.Destroying)
                        {
                            if (item.Opacity > 0.01)
                            {
                                item.Opacity -= item.Opacity*(this.MinEasing*3.0);
                            }
                            else
                            {
                                clusterSpokeState.Hub.Spokes.Remove(clusterSpokeState);
                                SetClusterSpokeState(item, null);
                                this._ScatterView.Items.Remove(item);
                                i--;
                            }
                        }
                    }
                    else
                    {
                        this.SetSpokeDistance(clusterSpokeState);
                        Point spokeLocation = this.GetSpokeLocation(clusterSpokeState);
                        MoveSpoke(clusterSpokeState, spokeLocation);
                    }
                }
            }
        }

        private void UpdateUpdatingClusters()
        {
            for (int i = 0; i < this._ScatterView.Items.Count; i++)
            {
                ScatterViewItem item = this._ScatterView.Items[i] as ScatterViewItem;
                ClusterHubState clusterHubState = GetClusterHubState(item);
                if ((clusterHubState != null) && clusterHubState.IsUpdating)
                {
                    List<ClusterSpokeState> spokes = clusterHubState.Spokes as List<ClusterSpokeState>;
                    if (spokes == null)
                    {
                        clusterHubState.IsUpdating = false;
                        this.BuildSpokes(item, this.Clusters[item.DataContext]);
                    }
                    else
                    {
                        IEnumerable<ClusterSpokeState> source = from spoke in spokes
                                                                where
                                                                    spoke.AnimationState ==
                                                                    ClusterSpokeAnimationState.Collapsing
                                                                select spoke;
                        IEnumerable<ClusterSpokeState> enumerable2 = from spoke in source
                                                                     where spoke.CurrentDistance == 0.0
                                                                     select spoke;
                        if (source.Count<ClusterSpokeState>() == enumerable2.Count<ClusterSpokeState>())
                        {
                            foreach (ClusterSpokeState state2 in enumerable2)
                            {
                                SetClusterSpokeState(state2.ScatterViewItem, null);
                                this._ScatterView.Items.Remove(state2.ScatterViewItem);
                                state2.ScatterViewItem = null;
                            }
                            clusterHubState.IsUpdating = false;
                            clusterHubState.Spokes = null;
                            this.BuildSpokes(item, this.Clusters[item.DataContext]);
                            i += this.Clusters[item.DataContext].Count;
                        }
                    }
                }
            }
        }

        private static void UpdateZIndexOnActivation(object source)
        {
            FrameworkElement element = source as FrameworkElement;
            if (element != null)
            {
                ScatterViewItem item = element as ScatterViewItem;
                if (item == null)
                {
                    item = element.FindVisualParent<ScatterViewItem>();
                }
                if (item != null)
                {
                    ClusterHubState hub = null;
                    ClusterSpokeState clusterSpokeState = GetClusterSpokeState(item);
                    if (clusterSpokeState != null)
                    {
                        clusterSpokeState.ScatterViewItem.SetRelativeZIndex(RelativeScatterViewZIndex.Topmost);
                        if (clusterSpokeState.IsOrphaned || clusterSpokeState.IsRemovedFromCluster)
                        {
                            return;
                        }
                        hub = clusterSpokeState.Hub;
                    }
                    if (hub == null)
                    {
                        hub = GetClusterHubState(item);
                    }
                    if (hub != null)
                    {
                        if (hub.Spokes != null)
                        {
                            (from spoke in hub.Spokes
                             where !spoke.IsOrphaned && !spoke.IsRemovedFromCluster
                             select spoke.ScatterViewItem
                             into svi
                             orderby svi.ZIndex
                             select svi).ToList<ScatterViewItem>().ForEach(delegate(ScatterViewItem svi)
                                                                               {
                                                                                   svi.SetRelativeZIndex(
                                                                                       RelativeScatterViewZIndex.Topmost);
                                                                               });
                        }
                        hub.ScatterViewItem.SetRelativeZIndex(RelativeScatterViewZIndex.Topmost);
                    }
                }
            }
        }

        // Properties
        public bool AllowReturnToCluster
        {
            get { return (bool) base.GetValue(AllowReturnToClusterProperty); }
            set { base.SetValue(AllowReturnToClusterProperty, value); }
        }

        public ReadOnlyDictionary<object, IList<object>> Clusters { get; private set; }

        public Style HubStyle
        {
            get { return (Style) base.GetValue(HubStyleProperty); }
            set { base.SetValue(HubStyleProperty, value); }
        }

      

        public double MaxEasing
        {
            get { return (double) base.GetValue(MaxEasingProperty); }
            set { base.SetValue(MaxEasingProperty, value); }
        }

        public double MaxSpokeDistance
        {
            get { return (double) base.GetValue(MaxSpokeDistanceProperty); }
            set { base.SetValue(MaxSpokeDistanceProperty, value); }
        }

        public double MinEasing
        {
            get { return (double) base.GetValue(MinEasingProperty); }
            set { base.SetValue(MinEasingProperty, value); }
        }

        public double MinSpokeDistance
        {
            get { return (double) base.GetValue(MinSpokeDistanceProperty); }
            set { base.SetValue(MinSpokeDistanceProperty, value); }
        }

        public double OrientationRandomness
        {
            get { return (double) base.GetValue(OrientationRandomnessProperty); }
            set { base.SetValue(OrientationRandomnessProperty, value); }
        }

        public Style SpokeStyle
        {
            get { return (Style) base.GetValue(SpokeStyleProperty); }
            set { base.SetValue(SpokeStyleProperty, value); }
        }

        public StyleSelector SpokeStyleSelector
        {
            get { return (StyleSelector) base.GetValue(SpokeStyleSelectorProperty); }
            set { base.SetValue(SpokeStyleSelectorProperty, value); }
        }
    }
}