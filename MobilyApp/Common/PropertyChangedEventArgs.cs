﻿using System;

namespace MobilyApp.Common
{
    public class PropertyChangedEventArgs<T> : EventArgs
    {
        public T OldValue { get; set; }

        public T NewValue { get; set; }

        public PropertyChangedEventArgs(T refOldValue, T refNewValue)
        {
            this.OldValue = refOldValue;
            this.NewValue = refNewValue;
        }
    }
}