﻿using System.Xml.Linq;

namespace MobilyApp.ViewModels.Requests
{
    public static class ConstantsRequest
    {
        public static string GetError(string msg)
        {
            bool flag = false;
            string str = string.Empty;
            msg = msg.Replace("---!>", "-->");
            foreach (XElement xelement1 in XElement.Parse(Constants.login_root + msg + Constants.login_rootEnd).Descendants())
            {
                if (xelement1.Name == (XName)Constants.login_MOBILY_IPHONE_REPLY)
                {
                    flag = true;
                    foreach (XElement xelement2 in xelement1.Descendants())
                    {
                        if (xelement2.Name == (XName)Constants.login_ERROR)
                        {
                            foreach (XElement xelement3 in xelement2.Descendants())
                            {
                                if (xelement3.Name == (XName)Constants.login_ErrorCode)
                                {
                                    switch (int.Parse(xelement3.Value))
                                    {
                                        case 5001:
                                            str = Constants.error_5001;
                                            break;
                                        case 5002:
                                            str = Constants.error_5002;
                                            break;
                                        case 10000:
                                            str = Constants.error_10000;
                                            break;
                                        case 10001:
                                            str = Constants.error_10001;
                                            break;
                                        case 10002:
                                            str = Constants.error_10002;
                                            break;
                                        case 10003:
                                            str = Constants.error_10003;
                                            break;
                                        case 10004:
                                            str = Constants.error_10004;
                                            break;
                                        case 10005:
                                            str = Constants.error_10005;
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (flag)
                return str;
            else
                return msg;
        }

        public static string ParseString(XElement xChild, string cstLabel)
        {
            if (xChild.Name == (XName)cstLabel)
                return xChild.Value;
            else
                return string.Empty;
        }

        public static double ParseDouble(XElement xChild, string cstLabel)
        {
            if (xChild.Name == (XName)cstLabel)
                return double.Parse(xChild.Value);
            else
                return 0.0;
        }
    }
}